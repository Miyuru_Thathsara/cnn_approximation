localparam      DATA_WIDTH          = 8;
localparam      APPROX_LEVEL_COUNT  = 2;    //for lenet, max abs(approx_weight) = 2
localparam      SUM_WIDTH           = 27;
localparam      APPROX_SUM_WIDTH    = SUM_WIDTH - 8 + APPROX_LEVEL_COUNT;
//*********************** L1 Params *************************
localparam      CONV_IN_WIDTH 	 	   = DATA_WIDTH * 1;

localparam      L1_CONV_DIM            = 5;
localparam      L1_PRE_CONV_DIM        = L1_CONV_DIM + 1;
localparam      L1_MAX_POOL_DIM        = 2;
localparam		L1_IMAGE_WIDTH         = 28;
localparam      L1_CONV_IMAGE_WIDTH    = L1_IMAGE_WIDTH - (L1_CONV_DIM-1);
localparam		L1_POOL_IMAGE_WIDTH    = L1_CONV_IMAGE_WIDTH/2;
localparam		L1_NUM_IFM             = 1;
localparam		L1_NUM_OFM             = 20;
localparam      L1_CONV_NEIGH_DIM      = L1_CONV_DIM * L1_CONV_DIM;
localparam      L1_PRE_CONV_NEIGH_DIM  = L1_PRE_CONV_DIM * L1_PRE_CONV_DIM;
localparam      L1_MAX_POOL_NEIGH_DIM  = L1_MAX_POOL_DIM * L1_MAX_POOL_DIM;
localparam      L1_MAX_POOL_NEIGH_DIM_WIDTH = 2;
localparam      L1_MAX_POOL_NEIGH_DIM_BUS_WIDTH = L1_MAX_POOL_NEIGH_DIM_WIDTH * L1_NUM_OFM;
localparam      L1_WINDOW_WIDTH        = DATA_WIDTH * L1_CONV_NEIGH_DIM * L1_NUM_IFM;
localparam      L1_PRE_WINDOW_WIDTH    = DATA_WIDTH * L1_PRE_CONV_NEIGH_DIM * L1_NUM_IFM;
localparam      L1_LINE_WIDTH          = DATA_WIDTH * L1_PRE_CONV_DIM * L1_NUM_IFM;
localparam      L1_INTER_ROW_WINDOW_GAP= 8;
localparam      L1_APPROX_CONTROLLER_IDX_WIDTH = L1_MAX_POOL_NEIGH_DIM_WIDTH + 1; //+1 for activation
localparam      L1_APPROX_CONTROLLER_IDX_BUS_WIDTH = L1_NUM_OFM * L1_APPROX_CONTROLLER_IDX_WIDTH;
localparam      L1_NO_OP_CMD_WIDTH      = 2; //0 = operation valid, 1 = operation invalid, 2 = operation zero
localparam      L1_NO_OP_CMD_BUS_WIDTH  = L1_NO_OP_CMD_WIDTH * L1_NUM_OFM; 

// localparam      L1_CONV_OUT_DATA_WIDTH = (DATA_WIDTH + count2width(L1_NUM_IFM + L1_CONV_NEIGH_DIM)) * L1_NUM_OFM;
localparam      L1_CONV_OUT_DATA_WIDTH = SUM_WIDTH * L1_NUM_OFM;
localparam      L1_APPROX_CONV_OUT_DATA_WIDTH = APPROX_SUM_WIDTH * L1_NUM_OFM;
localparam      L1_OUT_DATA_WIDTH      = DATA_WIDTH * L1_NUM_OFM;
//*********************** L2 Params *************************

localparam      L2_CONV_DIM            = 5;
localparam      L2_PRE_CONV_DIM        = L2_CONV_DIM + 1;
localparam      L2_MAX_POOL_DIM        = 2;
localparam      L2_PRE_CONV_NEIGH_DIM  = L2_PRE_CONV_DIM * L2_PRE_CONV_DIM;
localparam      L2_CONV_NEIGH_DIM      = L2_CONV_DIM * L2_CONV_DIM;
localparam		L2_IMAGE_WIDTH         = L1_POOL_IMAGE_WIDTH;
localparam      L2_CONV_IMAGE_WIDTH    = L2_IMAGE_WIDTH - (L2_CONV_DIM-1);
localparam		L2_POOL_IMAGE_WIDTH    = L2_CONV_IMAGE_WIDTH/2;
localparam		L2_NUM_IFM             = L1_NUM_OFM;
localparam		L2_NUM_OFM             = 50;
localparam      L2_WINDOW_WIDTH        = DATA_WIDTH * L2_CONV_NEIGH_DIM * L2_NUM_IFM;
localparam      L2_PRE_WINDOW_WIDTH    = DATA_WIDTH * L2_PRE_CONV_NEIGH_DIM * L2_NUM_IFM;
localparam      L2_CONV_OUT_DATA_WIDTH = SUM_WIDTH * L2_NUM_OFM;
localparam      L2_APPROX_CONV_OUT_DATA_WIDTH = APPROX_SUM_WIDTH * L2_NUM_OFM;
localparam      L2_OUT_DATA_WIDTH      = DATA_WIDTH * L2_NUM_OFM;
localparam      L2_LINE_WIDTH          = DATA_WIDTH * L2_PRE_CONV_DIM * L2_NUM_IFM;
localparam      L2_INTER_ROW_WINDOW_GAP= 49;
localparam      L2_MAX_POOL_NEIGH_DIM       = L2_MAX_POOL_DIM * L2_MAX_POOL_DIM;
localparam      L2_MAX_POOL_NEIGH_DIM_WIDTH = 2;
localparam      L2_MAX_POOL_NEIGH_DIM_BUS_WIDTH = L2_MAX_POOL_NEIGH_DIM_WIDTH * L2_NUM_OFM;
localparam      L2_APPROX_CONTROLLER_IDX_WIDTH = L2_MAX_POOL_NEIGH_DIM_WIDTH + 1; //+1 for activation
localparam      L2_APPROX_CONTROLLER_IDX_BUS_WIDTH = L2_NUM_OFM * L2_APPROX_CONTROLLER_IDX_WIDTH;
localparam      L2_NO_OP_CMD_WIDTH      = 2; //0 = operation valid, 1 = operation invalid, 2 = operation zero
localparam      L2_NO_OP_CMD_BUS_WIDTH  = L2_NO_OP_CMD_WIDTH * L2_NUM_OFM; 

localparam      CONV_OUT_WIDTH = L2_NUM_OFM * DATA_WIDTH;

/* 40 */
/*
localparam      L1_SCALE_FACTOR_A = 10;
localparam      L1_SCALE_FACTOR_B = 11;
//localparam      L1_SCALE_FACTOR_A = 9;
//ocalparam      L1_SCALE_FACTOR_B = 11;
    
localparam      L2_SCALE_FACTOR_A = 12;
localparam      L2_SCALE_FACTOR_B = 12;
//localparam      L2_SCALE_FACTOR_B = 15;
*/

/* 50 */
localparam      L1_SCALE_FACTOR_A = 10;
localparam      L1_SCALE_FACTOR_B = 12;
    
localparam      L2_SCALE_FACTOR_A = 11;
localparam      L2_SCALE_FACTOR_B = 13;

