`timescale 1ns / 1ps

module feature_map_state_handler
    (
        clk,
        reset,

        cfg_row_count_in,
        cfg_col_count_in,
        cfg_valid_in,

        valid_in,

        row_count_out,
        col_count_out,
        max_pool_neigh_idx_out,

        is_row_odd_out,
        is_col_odd_out,
        row_last_act_out,
        im_last_act_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                         "params/global_params.v"
    `include                                                        "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       CONV_IM_DIM              = 24;
    parameter                                                       MAX_POOL_DIM             = 2;
    parameter                                                       OFM_ITER_COUNT           = 1;     
    parameter                                                       DIM_WIDTH                = 8; 
    parameter                                                       DIM_PREDEFINED           = 1;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                      MAX_POOL_NEIGH_DIM_WIDTH = count2width(MAX_POOL_DIM * MAX_POOL_DIM);
    localparam                                                      OFM_ITER_COUNT_WIDTH     = count2width(OFM_ITER_COUNT);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                           clk;
    input                                                           reset;

    input        [DIM_WIDTH-1 : 0]                                  cfg_row_count_in;
    input        [DIM_WIDTH-1 : 0]                                  cfg_col_count_in;
    input                                                           cfg_valid_in;

    input                                                           valid_in;

    output reg   [DIM_WIDTH-1 : 0]                                  row_count_out;
    output reg   [DIM_WIDTH-1 : 0]                                  col_count_out;
    output       [MAX_POOL_NEIGH_DIM_WIDTH- 1: 0]                   max_pool_neigh_idx_out;

    output                                                          is_row_odd_out;
    output                                                          is_col_odd_out;
    output                                                          row_last_act_out;
    output                                                          im_last_act_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg          [DIM_WIDTH-1 : 0]                                  cfg_col_count;
    reg          [DIM_WIDTH-1 : 0]                                  cfg_row_count;
    reg          [OFM_ITER_COUNT_WIDTH-1 : 0]                       ofm_iter_count;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    always@(posedge clk) begin : cfg_blk
        if(reset) begin
            cfg_col_count                   <= {DIM_WIDTH{1'b0}};
            cfg_row_count                   <= {DIM_WIDTH{1'b0}};
        end
        else begin
            if(DIM_PREDEFINED) begin
                cfg_col_count               <= CONV_IM_DIM;
                cfg_row_count               <= CONV_IM_DIM;
            end
            else begin
                if(cfg_valid_in) begin
                    cfg_col_count           <= cfg_col_count_in;
                    cfg_row_count           <= cfg_row_count_in;
                end 
            end
        end 
    end

    always@(posedge clk) begin : pix_count_blk
        if(reset) begin
            ofm_iter_count                  <= {OFM_ITER_COUNT_WIDTH{1'b0}};
            col_count_out                   <= {DIM_WIDTH{1'b0}};
            row_count_out                   <= {DIM_WIDTH{1'b0}};
        end
        else begin
            if(valid_in) begin
                if(ofm_iter_count == OFM_ITER_COUNT - 1) begin //todo : make_cfg
                    ofm_iter_count          <= {OFM_ITER_COUNT_WIDTH{1'b0}};
                    if(col_count_out == cfg_col_count-1) begin
                        col_count_out       <= {DIM_WIDTH{1'b0}};
                        if(row_count_out == cfg_row_count-1)begin
                            row_count_out   <= {DIM_WIDTH{1'b0}};
                        end
                        else begin
                            row_count_out   <= row_count_out + 1'b1;
                        end
                    end
                    else begin
                        col_count_out       <= col_count_out + 1'b1;
                    end
                end
                else begin
                    ofm_iter_count          <= ofm_iter_count + 1'b1;
                end 
            end
        end
    end

    assign max_pool_neigh_idx_out = (col_count_out%MAX_POOL_DIM) * MAX_POOL_DIM + (row_count_out % MAX_POOL_DIM);
    assign is_row_odd_out         = row_count_out[0];
    assign is_col_odd_out         = col_count_out[0];
    assign row_last_act_out       = (col_count_out == cfg_col_count-1) ? 1'b1 : 1'b0;
    assign im_last_act_out        = (row_count_out == cfg_row_count-1 && col_count_out == cfg_col_count-1) ? 1'b1 : 1'b0;

endmodule
