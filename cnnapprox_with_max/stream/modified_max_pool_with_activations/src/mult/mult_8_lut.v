`timescale 1ns / 1ps
module mult8_lut
    (
        clk,
        reset,

        pixel_in,
        weight_in,
        valid_in,
    
        result_out,
        result_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                           DATA_WIDTH        = 8;
    parameter                                                           WEIGHT_WIDTH      = 8;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam								                            RESULT_WIDTH      = DATA_WIDTH + WEIGHT_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                               clk;
    input                                                               reset;

    input signed  [DATA_WIDTH-1 : 0]                        		    pixel_in;
    input signed  [WEIGHT_WIDTH-1:0]              			            weight_in;
    input                                                               valid_in;

    output reg signed [RESULT_WIDTH-1 : 0]                              result_out;
    output reg                                                          result_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    always@(posedge clk) begin
        if(reset) begin
           result_out	        <= {RESULT_WIDTH{1'b0}};
           result_valid_out 	<= 1'b0;
        end
        else begin
            result_valid_out	<= valid_in;
            if(valid_in) begin
                result_out	    <= pixel_in * weight_in;
            end
        end
    end
    
endmodule

