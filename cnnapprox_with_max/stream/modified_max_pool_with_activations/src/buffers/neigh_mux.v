`timescale 1ns / 1ps

module neigh_mux
    (
        clk,
        reset,

        neigh_in,
        neigh_idx_in,
        neigh_idx_valid_in,
        

        neigh_out,
        neigh_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
   parameter                                                   DATA_WIDTH   = 8;
   parameter                                                   CONV_KERNEL_DIM = 5;
   parameter                                                   APPROX_KERNEL_DIM = CONV_KERNEL_DIM + 1;
   parameter                                                   MAX_POOL_NEIGH_DIM_WIDTH = 2; 
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [DATA_WIDTH * APPROX_KERNEL_DIM * APPROX_KERNEL_DIM -1 : 0]   
                                                                neigh_in;
    input       [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                neigh_idx_in;
    input                                                       neigh_idx_valid_in;

    output reg  [DATA_WIDTH * CONV_KERNEL_DIM * CONV_KERNEL_DIM  -1 : 0]
                                                                neigh_out;
    output reg                                                  neigh_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg    [APPROX_KERNEL_DIM * CONV_KERNEL_DIM * DATA_WIDTH -1 : 0]   v_neigh;
    // reg    [CONV_KERNEL_DIM * CONV_KERNEL_DIM * DATA_WIDTH -1 : 0]     h_neigh;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    
    integer j;
    always@(*) begin
        v_neigh = {(APPROX_KERNEL_DIM * CONV_KERNEL_DIM * DATA_WIDTH){1'b0}};
        for(j=0;j<APPROX_KERNEL_DIM;j=j+1) begin
            if(neigh_idx_valid_in) begin
                v_neigh[j * CONV_KERNEL_DIM * DATA_WIDTH +: CONV_KERNEL_DIM * DATA_WIDTH] 
                    // = neigh_in[j*APPROX_KERNEL_DIM*DATA_WIDTH + (DATA_WIDTH * (1 - (neigh_idx_in%2))) +: CONV_KERNEL_DIM * DATA_WIDTH]; 
                    = neigh_in[j*APPROX_KERNEL_DIM*DATA_WIDTH + (DATA_WIDTH * (neigh_idx_in%2)) +: CONV_KERNEL_DIM * DATA_WIDTH]; 
            end 
        end
        // h_neigh = 
    end


//    always @(posedge clk) begin
//        if(reset) begin
//            neigh_out           <= {(CONV_KERNEL_DIM * CONV_KERNEL_DIM * DATA_WIDTH){1'b0}};
//            neigh_valid_out     <= 1'b0;
//        end
//        else begin
//            neigh_valid_out     <= neigh_idx_valid_in;
//            if(neigh_idx_valid_in) begin
//                neigh_out       <= v_neigh[(1- (neigh_idx_in/2)) * CONV_KERNEL_DIM * DATA_WIDTH +: CONV_KERNEL_DIM * CONV_KERNEL_DIM * DATA_WIDTH];
//            end
//        end
//    end
    always @(*) begin
        neigh_out           = {(DATA_WIDTH * CONV_KERNEL_DIM * CONV_KERNEL_DIM){1'b0}};
        neigh_valid_out     = neigh_idx_valid_in;
        if(neigh_idx_valid_in) begin
            // neigh_out       = v_neigh[(1- (neigh_idx_in/2)) * CONV_KERNEL_DIM * DATA_WIDTH +: CONV_KERNEL_DIM * CONV_KERNEL_DIM * DATA_WIDTH];
            neigh_out       = v_neigh[(neigh_idx_in/2) * CONV_KERNEL_DIM * DATA_WIDTH +: CONV_KERNEL_DIM * CONV_KERNEL_DIM * DATA_WIDTH];
        end
    end

            // assign neigh_out = h_neigh_reg;
endmodule
