
`timescale 1ns / 1ps

module pseudo_max_pool_mux
    (
        clk,
        reset,

        //RX port
        rx_data_in,
        rx_data_reg_in,
        rx_cache_rd_data_in,
        rx_is_data_valid_in,
        rx_is_data_valid_reg_in,
        rx_valid_in,

        //TX port
        tx_data_out,
        tx_data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                               DATA_WIDTH  = 8;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                   clk;
    input                                   reset;

    //RX port
    input       [DATA_WIDTH-1 : 0]          rx_data_in;
    input       [DATA_WIDTH-1 : 0]          rx_data_reg_in;
    input       [DATA_WIDTH-1 : 0]          rx_cache_rd_data_in;
    input                                   rx_is_data_valid_in;
    input                                   rx_is_data_valid_reg_in;
    input                                   rx_valid_in;

    //TX port
    output reg  [DATA_WIDTH-1 : 0]          tx_data_out;
    output reg                              tx_data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    always@(posedge clk) begin
        if(reset) begin
            tx_data_out         <= {DATA_WIDTH{1'b0}};
            tx_data_valid_out   <= 1'b0;
        end
        else begin
            tx_data_valid_out   <= rx_valid_in;
            if(rx_valid_in) begin
                if(rx_is_data_valid_reg_in) begin
                    tx_data_out <= rx_data_reg_in;
                end
                else if(rx_is_data_valid_in) begin
                    tx_data_out <= rx_data_in;
                end
                else begin
                    tx_data_out <= rx_cache_rd_data_in;
                end
            end 
        end
    end

endmodule
