`timescale 1ns / 1ps

module quant_dequant_activation_int
    (
        clk,
        reset,

        scale_factor_a_in,
        scale_factor_b_in,

        pixel_in,
        pixel_valid_in,

        pixel_out,
        pixel_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
   `include "params/weight_params.sv"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   IN_DATA_WIDTH        = 32;
    parameter                                                   LAYER_IDX            = 0;
    parameter                                                   FM_IDX               = 0;
    parameter                                                   ACTIVATION_TYPE      = "none";
    parameter                                                   OUT_DATA_WIDTH       = 8;
    parameter                                                   SCALE_FACTOR_A       = 1;
    parameter                                                   SCALE_FACTOR_B       = 2;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  PARTIAL_RESULT_WIDTH = IN_DATA_WIDTH * 2;
    localparam                                                  PARTIAL_RESULT_ROUNDED_WIDTH = PARTIAL_RESULT_WIDTH-IN_DATA_WIDTH;
    localparam                                                  OUT_MAX              = 2**(OUT_DATA_WIDTH-1) - 1;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [7:0]                                           scale_factor_a_in;
    input       [7:0]                                           scale_factor_b_in;

    input       [IN_DATA_WIDTH-1 : 0]                           pixel_in;
    input                                                       pixel_valid_in;

    output reg  [OUT_DATA_WIDTH-1 : 0]                          pixel_out;
    output reg                                                  pixel_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire signed [IN_DATA_WIDTH-1 : 0]                           shifted_result_a;
    wire signed [IN_DATA_WIDTH-1 : 0]                           shifted_result_b;

    // wire signed [IN_DATA_WIDTH-1 : 0]                           shifted_result_a_se;
    // wire signed [IN_DATA_WIDTH-1 : 0]                           shifted_result_b_se;
    wire signed [PARTIAL_RESULT_WIDTH-1 : 0]                    shifted_result_a_se;
    wire signed [PARTIAL_RESULT_WIDTH-1 : 0]                    shifted_result_b_se;

    reg  signed [PARTIAL_RESULT_WIDTH-1 : 0]                    partial_result_sum;
    reg                                                         partial_result_sum_valid;
    wire signed [PARTIAL_RESULT_ROUNDED_WIDTH-1 : 0]            partial_result_sum_int;
    wire signed [PARTIAL_RESULT_ROUNDED_WIDTH-1 : 0]            partial_result_sum_rounded;
    wire signed [PARTIAL_RESULT_ROUNDED_WIDTH-1 : 0]            partial_result_sum_abs;
    wire signed [IN_DATA_WIDTH-1 : 0]                           partial_result_sum_shifted;
    wire signed [IN_DATA_WIDTH-1 : 0]                           partial_result_sum_shifted_se;
    wire                                                        sum_out_of_range;
    
    //params
    wire        [31 : 0]                                        dequant_factor;
    wire        [31 : 0]                                        quant_factor;
    wire        [31 : 0]                                        dequant_quant_factor;
    // wire        [31 : 0]                                        bias;

    wire        [31 : 0]                                        pixel_float;
    wire                                                        pixel_float_valid; 

    // wire        [31 : 0]                                        dequant_with_bias_result;
    // wire                                                        dequant_with_bias_result_valid;    

    wire        [31 : 0]                                        fquant_result;
    wire                                                        fquant_result_valid;    
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    // assign dequant_factor       = dequant_factors[LAYER_IDX];
    // assign quant_factor         = quant_factors[LAYER_IDX];
    // assign dequant_quant_factor = quant_dequant_factors[LAYER_IDX];
    // assign bias           = (LAYER_IDX == 0) ? l1_bias_terms[FM_IDX] : 
    //                         (LAYER_IDX == 1) ? l2_bias_terms[FM_IDX] : 
    //                         {32'h0};
    // assign shifted_result = pixel_in * 0.375;

    /*
    Basic Shift Operation

    Shift by power of two values and add up : slight loss in accuracy
    */
    /*
    assign shifted_result_a     = pixel_in >> 9;
    assign shifted_result_b     = pixel_in >> 10;

    sign_extend
    #(
        .INPUT_WIDTH    ((IN_DATA_WIDTH-9)),
        .OUTPUT_WIDTH   (IN_DATA_WIDTH)
    )
    u_shift_a_se
    (
        .data_in        (shifted_result_a [0 +: (IN_DATA_WIDTH-9)]),
        .data_out       (shifted_result_a_se)
    );

    sign_extend
    #(
        .INPUT_WIDTH    ((IN_DATA_WIDTH-10)),
        .OUTPUT_WIDTH   (IN_DATA_WIDTH)
    )
    u_shift_b_se
    (
        .data_in        (shifted_result_b[0 +: (IN_DATA_WIDTH-10)]),
        .data_out       (shifted_result_b_se)
    );

    assign shifted_result_sum   = shifted_result_a_se + shifted_result_b_se;
    assign pixel_out            = shifted_result_sum[OUT_DATA_WIDTH-1 : 0];
    */


    /* 
    optimized shift operation
    */
    left_shift_fixed_se 
    #(
        .INPUT_WIDTH    (IN_DATA_WIDTH),
        .SHIFT_VAL      (SCALE_FACTOR_A)
    )
    u_left_shift_fixed_se_a
    (
        .data_in        (pixel_in),
        .data_out       (shifted_result_a_se)
    );

    left_shift_fixed_se
    #(
        .INPUT_WIDTH    (IN_DATA_WIDTH),
        .SHIFT_VAL      (SCALE_FACTOR_B)
    )
    u_left_shift_fixed_se_b
    (
        .data_in        (pixel_in),
        .data_out       (shifted_result_b_se)
    );

    always@(posedge clk) begin
        if(reset) begin
            partial_result_sum          <= {(PARTIAL_RESULT_WIDTH){1'b0}};
            partial_result_sum_valid    <= 1'b0;
        end
        else begin
            partial_result_sum_valid    <= pixel_valid_in;
            if(pixel_valid_in) begin
                partial_result_sum      <= shifted_result_a_se + shifted_result_b_se;
            end
        end
    end

    // assign 
    assign partial_result_sum_int     = partial_result_sum[IN_DATA_WIDTH +: IN_DATA_WIDTH];
    assign partial_result_sum_rounded = (partial_result_sum[IN_DATA_WIDTH-1]) ? 
                                                ( (partial_result_sum[PARTIAL_RESULT_WIDTH-1]) ?  (partial_result_sum[IN_DATA_WIDTH +: IN_DATA_WIDTH] - 1'b1): 
                                                                                                  (partial_result_sum[IN_DATA_WIDTH +: IN_DATA_WIDTH] + 1'b1) ): 
                                        partial_result_sum[IN_DATA_WIDTH +: IN_DATA_WIDTH];  
    assign partial_result_sum_abs     = partial_result_sum_rounded[PARTIAL_RESULT_ROUNDED_WIDTH-1] ? (-partial_result_sum_rounded) : partial_result_sum_rounded;
    assign sum_out_of_range           = (partial_result_sum_abs>OUT_MAX) ? 1'b1 : 1'b0;

    always@(posedge clk) begin
        if(reset) begin
            pixel_out             <= {OUT_DATA_WIDTH{1'b0}};
            pixel_valid_out       <= 1'b0;
        end
        else begin
            pixel_valid_out       <= partial_result_sum_valid;
            if(partial_result_sum_valid) begin
                if(sum_out_of_range)   begin   //check if any out of range bits are set
                    if(partial_result_sum_rounded[PARTIAL_RESULT_ROUNDED_WIDTH-1]) begin
                        pixel_out <= -OUT_MAX;
                    end
                    else begin
                        pixel_out <= OUT_MAX;
                    end
                end
                else begin
                    pixel_out     <= partial_result_sum_rounded[0 +: OUT_DATA_WIDTH] ;
                end
            end 
        end
    end

endmodule