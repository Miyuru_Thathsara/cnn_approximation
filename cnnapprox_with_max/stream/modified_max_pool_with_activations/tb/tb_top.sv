`timescale 1ns / 1ps

module tb_top;

	`include "image.sv"
	`include "tb_defs.sv" 

	localparam                      DATA_WIDTH    = 8;
	localparam						L1_NUM_OFM	  = 20;
	localparam						L2_NUM_OFM	  = 50;
	localparam						INPUT_NUM_FM  = 1;
	localparam                      INPUT_DATA_WIDTH  = DATA_WIDTH * INPUT_NUM_FM;
    localparam                      OUTPUT_DATA_WIDTH = DATA_WIDTH * L2_NUM_OFM;
	localparam						IMAGE_DIM	  = 28;
	localparam						L1_CONV_DIM   = 24;
	localparam						L2_CONV_DIM   = 8;
	localparam						OUT_IMAGE_DIM = 4;

	localparam      				APPROX_LEVEL_COUNT  = 2;    //for lenet, max abs(approx_weight) = 2
	localparam      				SUM_WIDTH           = 27;
	localparam      				APPROX_SUM_WIDTH    = SUM_WIDTH - 8 + APPROX_LEVEL_COUNT;
    
	reg clk;
	reg reset;

	reg  [INPUT_DATA_WIDTH-1 : 0]	data_in;
	reg 							data_valid_in;

	wire [OUTPUT_DATA_WIDTH-1 : 0]	out_data;
	wire 							out_data_valid;

	integer 						out_file;

	integer 						counter;
	integer 						row_counter;
	integer 						col_counter;

	top
    u_conv_top
    (
        .clk			(clk),
        .reset			(reset),

		.pixel_bus_in	(data_in),
		.pixel_valid_in	(data_valid_in),

        .data_out 		(out_data),
        .data_valid_out (out_data_valid)
    );

	always #5 clk = ~clk;

	task write_image();
		integer i;

		// repeat(3) begin
		// 	@(posedge clk);
		// 	#1;
		// 	data_valid_in = 1'b1;
		// end

		repeat(IMAGE_DIM) begin
//			data_in = 1 + counter%28;
//			 data_in = $urandom_range(0, 255);
			repeat(IMAGE_DIM) begin
				// data_in = counter/IMAGE_DIM + 1;
				@(posedge clk);
				//#1;
				//fork
				data_in = pixel_array_1[(IMAGE_DIM * IMAGE_DIM) - 1 - counter];
				//  for(i=0; i<INPUT_NUM_FM; i++) begin
				//  	data_in[i*DATA_WIDTH +: DATA_WIDTH] = counter%IMAGE_DIM + 1 + i + ((counter/IMAGE_DIM)%2);
				//  end
					data_valid_in = 1'b1;
				//join
				counter = counter + 1'b1;
				col_counter = col_counter + 1'b1;
			end	
			@(posedge clk);
			//#1;
			data_valid_in = 1'b0;
			repeat(3) begin
				@(posedge clk);
			end
			col_counter = 0;
			row_counter = row_counter + 1'b1;
		end
	endtask

	task read_out();
		repeat(OUT_IMAGE_DIM * OUT_IMAGE_DIM) begin
		// repeat(12 * 12) begin
			while(!out_data_valid) begin
				@(posedge clk);
			end
			$display("Out pixel bus : %x", out_data);
			$fwrite(out_file,"%h\n",out_data);
			@(posedge clk);
		end
	endtask
	
	// task monitor_errors_l1();
	// 	bit	conv_positive	[L1_NUM_OFM];
	// 	bit	approx_positive	[L1_NUM_OFM];

	// 	integer false_positives[L1_NUM_OFM], false_negatives[L1_NUM_OFM], negatives[L1_NUM_OFM], no_op[L1_NUM_OFM];
	// 	integer total_false_positives, total_false_negatives, mismatch_count, total_negatives, total_count, total_no_op;
		
	// 	for(int i = 0; i<L1_NUM_OFM; i++) begin
	// 		false_negatives[i] = 0;
	// 		false_positives[i] = 0;
	// 		negatives[i] 	   = 0;
	// 		no_op[i]		   = 0;
	// 	end
	// 	total_false_negatives = 0;
	// 	total_false_positives = 0;
	// 	total_negatives		  = 0;
	// 	total_no_op			  = 0;

	// 	repeat(L1_CONV_DIM * L1_CONV_DIM) begin
	// 		@(posedge clk);
	// 		while(!(u_conv_top.u_conv_layer_l1.data_valid_out)) begin
	// 			@(posedge clk);
	// 		end
	// 		// for(int i=0; i<L1_NUM_OFM; i=i+1) begin
	// 			// conv_positive   = u_conv_top.u_conv_layer_l1.genblk1[i].u_stream_conv_out_ch[i].conv_data_valid[0];
	// 			/*
	// 			conv_positive[0]   = (u_conv_top.u_conv_layer_l1.genblk1[0].u_stream_conv_out_ch.conv_ofm_with_bias_dout_se > 0) ? 1'b1 : 1'b0;
	// 			approx_positive[0] = (u_conv_top.u_conv_layer_l1.genblk1[0].u_stream_conv_out_ch.is_result_zero_delayed) ? 1'b0 : 1'b1;
	// 			$display("Conv Positive : %d Approx Positive : %d", conv_positive[0], approx_positive[0]);
	// 			if(conv_positive[0] == 1 && approx_positive[0] == 0) begin
	// 				false_negatives[0]++;
	// 			end
	// 			else if(conv_positive[0] == 0 && approx_positive[0] == 1) begin
	// 				false_positives[0]++;
	// 			end
	// 			*/
	// 		fork
	// 			`SNOOP_APPROX_STATS_L1(0)
	// 			`SNOOP_APPROX_STATS_L1(1)
	// 			`SNOOP_APPROX_STATS_L1(2)
	// 			`SNOOP_APPROX_STATS_L1(3)
	// 			`SNOOP_APPROX_STATS_L1(4)
	// 			`SNOOP_APPROX_STATS_L1(5)
	// 			`SNOOP_APPROX_STATS_L1(6)
	// 			`SNOOP_APPROX_STATS_L1(7)
	// 			`SNOOP_APPROX_STATS_L1(8)
	// 			`SNOOP_APPROX_STATS_L1(9)
	// 			`SNOOP_APPROX_STATS_L1(10)
	// 			`SNOOP_APPROX_STATS_L1(11)
	// 			`SNOOP_APPROX_STATS_L1(12)
	// 			`SNOOP_APPROX_STATS_L1(13)
	// 			`SNOOP_APPROX_STATS_L1(14)
	// 			`SNOOP_APPROX_STATS_L1(15)
	// 			`SNOOP_APPROX_STATS_L1(16)
	// 			`SNOOP_APPROX_STATS_L1(17)
	// 			`SNOOP_APPROX_STATS_L1(18)
	// 			`SNOOP_APPROX_STATS_L1(19)
	// 			// `L1_SNOOP_CALLS()
	// 		join
	// 		// end
	// 	end

	// 	for(int i = 0; i<L1_NUM_OFM; i++) begin
	// 		total_false_negatives += false_negatives[i];
	// 		total_false_positives += false_positives[i];
	// 		total_negatives		  += negatives[i];
	// 		total_no_op			  += no_op[i];
	// 	end

	// 	mismatch_count = total_false_negatives + total_false_positives;
	// 	total_count = L1_CONV_DIM * L1_CONV_DIM * L1_NUM_OFM;

	// 	$display("L2 False Positive : %d, False Negative : %d, Mismatch Count : %d, Total Negative : %d Total No OP : %d Total Count : %d", total_false_positives, total_false_negatives, mismatch_count, total_negatives, total_no_op,total_count);
	// endtask

	// task monitor_errors_l2();
	// 	bit	conv_positive	[L2_NUM_OFM];
	// 	bit	approx_positive	[L2_NUM_OFM];

	// 	integer false_positives[L2_NUM_OFM], false_negatives[L2_NUM_OFM], negatives[L2_NUM_OFM], no_op[L2_NUM_OFM];
	// 	integer total_false_positives, total_false_negatives, mismatch_count, total_negatives, total_count, total_no_op;
		
	// 	for(int i = 0; i<L2_NUM_OFM; i++) begin
	// 		false_negatives[i] = 0;
	// 		false_positives[i] = 0;
	// 		negatives[i] 	   = 0;
	// 		no_op[i]		   = 0;
	// 	end
	// 	total_false_negatives = 0;
	// 	total_false_positives = 0;
	// 	total_negatives		  = 0;
	// 	total_no_op			  = 0;

	// 	repeat(L2_CONV_DIM * L2_CONV_DIM) begin
	// 		@(posedge clk);
	// 		while(!(u_conv_top.u_conv_layer_l2.data_valid_out)) begin
	// 			@(posedge clk);
	// 		end
	// 		// for(int i=0; i<L2_NUM_OFM; i=i+1) begin
	// 			// conv_positive   = u_conv_top.u_conv_layer_l2.genblk1[i].u_stream_conv_out_ch[i].conv_data_valid[0];
	// 			/*
	// 			conv_positive[0]   = (u_conv_top.u_conv_layer_l2.genblk1[0].u_stream_conv_out_ch.conv_ofm_with_bias_dout_se > 0) ? 1'b1 : 1'b0;
	// 			approx_positive[0] = (u_conv_top.u_conv_layer_l2.genblk1[0].u_stream_conv_out_ch.is_result_zero_delayed) ? 1'b0 : 1'b1;
	// 			$display("Conv Positive : %d Approx Positive : %d", conv_positive[0], approx_positive[0]);
	// 			if(conv_positive[0] == 1 && approx_positive[0] == 0) begin
	// 				false_negatives[0]++;
	// 			end
	// 			else if(conv_positive[0] == 0 && approx_positive[0] == 1) begin
	// 				false_positives[0]++;
	// 			end
	// 			*/
	// 		fork
	// 			`SNOOP_APPROX_STATS_L2(0)                                               
	// 			`SNOOP_APPROX_STATS_L2(1)                                       
	// 			`SNOOP_APPROX_STATS_L2(2)                                       
	// 			`SNOOP_APPROX_STATS_L2(3)                                       
	// 			`SNOOP_APPROX_STATS_L2(4)                                       
	// 			`SNOOP_APPROX_STATS_L2(5)                                       
	// 			`SNOOP_APPROX_STATS_L2(6)                                       
	// 			`SNOOP_APPROX_STATS_L2(7)                                       
	// 			`SNOOP_APPROX_STATS_L2(8)                                       
	// 			`SNOOP_APPROX_STATS_L2(9)                                       
	// 			`SNOOP_APPROX_STATS_L2(10)      
	// 			`SNOOP_APPROX_STATS_L2(11)                      
	// 			`SNOOP_APPROX_STATS_L2(12)                      
	// 			`SNOOP_APPROX_STATS_L2(13)                      
	// 			`SNOOP_APPROX_STATS_L2(14)                      
	// 			`SNOOP_APPROX_STATS_L2(15)                      
	// 			`SNOOP_APPROX_STATS_L2(16)                      
	// 			`SNOOP_APPROX_STATS_L2(17)                      
	// 			`SNOOP_APPROX_STATS_L2(18)                      
	// 			`SNOOP_APPROX_STATS_L2(19)                      
	// 			`SNOOP_APPROX_STATS_L2(20)                         
	// 			`SNOOP_APPROX_STATS_L2(21)                      
	// 			`SNOOP_APPROX_STATS_L2(22)                      
	// 			`SNOOP_APPROX_STATS_L2(23)                      
	// 			`SNOOP_APPROX_STATS_L2(24)                      
	// 			`SNOOP_APPROX_STATS_L2(25)                      
	// 			`SNOOP_APPROX_STATS_L2(26)                      
	// 			`SNOOP_APPROX_STATS_L2(27)                      
	// 			`SNOOP_APPROX_STATS_L2(28)                      
	// 			`SNOOP_APPROX_STATS_L2(29)                      
	// 			`SNOOP_APPROX_STATS_L2(30)                      
	// 			`SNOOP_APPROX_STATS_L2(31)                      
	// 			`SNOOP_APPROX_STATS_L2(32)                      
	// 			`SNOOP_APPROX_STATS_L2(33)                      
	// 			`SNOOP_APPROX_STATS_L2(34)                      
	// 			`SNOOP_APPROX_STATS_L2(35)                      
	// 			`SNOOP_APPROX_STATS_L2(36)                      
	// 			`SNOOP_APPROX_STATS_L2(37)                      
	// 			`SNOOP_APPROX_STATS_L2(38)                      
	// 			`SNOOP_APPROX_STATS_L2(39)                      
	// 			// `SNOOP_APPROX_STATS_L2(40)                         
	// 			// `SNOOP_APPROX_Smonitor_errors_l1();
	// 		// TATS_L2(41)                      
	// 		// 	// `SNOOP_APPROX_Smonitor_errors_l1();
	// 		// TATS_L2(42)                      
	// 		// 	// `SNOOP_APPROX_Smonitor_errors_l1();
	// 		// TATS_L2(43)                      
	// 		// 	// `SNOOP_APPROX_Smonitor_errors_l1();
	// 		// TATS_L2(44)                      
	// 		// 	// `SNOOP_APPROX_STATS_L2(45)                      
	// 			// `SNOOP_APPROX_STATS_L2(46)                      
	// 			// `SNOOP_APPROX_STATS_L2(47)                      
	// 			// `SNOOP_APPROX_STATS_L2(48)                      
	// 			// `SNOOP_APPROX_STATS_L2(49)                     
	// 			// `L2_SNOOP_CALLS()
	// 		join
	// 		// end
	// 	end

	// 	for(int i = 0; i<L2_NUM_OFM; i++) begin
	// 		total_false_negatives += false_negatives[i];
	// 		total_false_positives += false_positives[i];
	// 		total_negatives		  += negatives[i];
	// 		total_no_op			  += no_op[i];
	// 	end

	// 	mismatch_count = total_false_negatives + total_false_positives;
	// 	total_count = L2_CONV_DIM * L2_CONV_DIM * L2_NUM_OFM;

	// 	$display("L2 False Positive : %d, False Negative : %d, Mismatch Count : %d, Total Negative : %d Total No OP : %d Total Count : %d", total_false_positives, total_false_negatives, mismatch_count, total_negatives, total_no_op,total_count);
	// endtask
	
	initial begin
		counter = 0;
		row_counter = 0;
		col_counter = 0;
    	clk	= 1'b0;
    	reset = 1'b1;
		data_in	= {DATA_WIDTH{1'b0}};
		data_valid_in = 1'b0;
		out_file = $fopen("/media/data3/duvindu/projects/modified_max_pool_50_all_layers/logs/out_file.txt","w");
    	repeat(5) begin
    		@(posedge clk);
    	end

    	reset = 1'b0;

		repeat(20) begin
			@(posedge clk);
		end
		
		fork
			write_image();
			read_out();
			// monitor_errors_l1();
			// monitor_errors_l2();
		join

		$fclose(out_file);
		repeat(100) begin
			@(posedge clk);
		end 

		
    	$finish;
    end

endmodule
