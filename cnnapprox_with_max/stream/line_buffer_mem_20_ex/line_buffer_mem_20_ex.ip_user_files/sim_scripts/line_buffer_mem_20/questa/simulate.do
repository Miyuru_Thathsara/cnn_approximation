onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib line_buffer_mem_20_opt

do {wave.do}

view wave
view structure
view signals

do {line_buffer_mem_20.udo}

run -all

quit -force
