onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -t 1ps -L blk_mem_gen_v8_4_2 -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -L xpm -lib xil_defaultlib xil_defaultlib.vpool_cache_108_28 xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {vpool_cache_108_28.udo}

run -all

quit -force
