onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib vpool_cache_108_28_opt

do {wave.do}

view wave
view structure
view signals

do {vpool_cache_108_28.udo}

run -all

quit -force
