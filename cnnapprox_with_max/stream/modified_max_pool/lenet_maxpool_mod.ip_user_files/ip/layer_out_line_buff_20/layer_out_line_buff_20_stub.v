// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Fri Apr 12 18:39:56 2019
// Host        : hesl-HP-Z420-Workstation running 64-bit Ubuntu 18.04 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/hesl/duvindu/pool_approximator/lenet/modified_pool_index_cache/lenet_maxpool_mod.runs/layer_out_line_buff_20_synth_1/layer_out_line_buff_20_stub.v
// Design      : layer_out_line_buff_20
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu9p-fsgd2104-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_2,Vivado 2018.3" *)
module layer_out_line_buff_20(clka, ena, wea, addra, dina, douta)
/* synthesis syn_black_box black_box_pad_pin="clka,ena,wea[19:0],addra[3:0],dina[159:0],douta[159:0]" */;
  input clka;
  input ena;
  input [19:0]wea;
  input [3:0]addra;
  input [159:0]dina;
  output [159:0]douta;
endmodule
