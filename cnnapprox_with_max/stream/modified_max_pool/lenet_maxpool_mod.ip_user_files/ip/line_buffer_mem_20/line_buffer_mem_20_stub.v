// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Thu Apr 11 12:34:14 2019
// Host        : hesl-HP-Z420-Workstation running 64-bit Ubuntu 18.04 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/hesl/duvindu/pool_approximator/lenet/modified_pool_index_cache/lenet_maxpool_mod.runs/line_buffer_mem_20_synth_1/line_buffer_mem_20_stub.v
// Design      : line_buffer_mem_20
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu9p-fsgd2104-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_2,Vivado 2018.3" *)
module line_buffer_mem_20(clka, ena, wea, addra, dina, clkb, enb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,ena,wea[19:0],addra[3:0],dina[159:0],clkb,enb,addrb[3:0],doutb[159:0]" */;
  input clka;
  input ena;
  input [19:0]wea;
  input [3:0]addra;
  input [159:0]dina;
  input clkb;
  input enb;
  input [3:0]addrb;
  output [159:0]doutb;
endmodule
