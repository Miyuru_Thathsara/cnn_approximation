// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Tue Jul 30 15:43:51 2019
// Host        : HESL running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/hesl/Desktop/cnnapprox_with_max/stream/modified_max_pool/lenet_maxpool_mod.runs/vpool_cahce_108_24_synth_1/vpool_cahce_108_24_stub.v
// Design      : vpool_cahce_108_24
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu9p-fsgd2104-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_2,Vivado 2018.3" *)
module vpool_cahce_108_24(clka, wea, addra, dina, douta)
/* synthesis syn_black_box black_box_pad_pin="clka,wea[0:0],addra[4:0],dina[107:0],douta[107:0]" */;
  input clka;
  input [0:0]wea;
  input [4:0]addra;
  input [107:0]dina;
  output [107:0]douta;
endmodule
