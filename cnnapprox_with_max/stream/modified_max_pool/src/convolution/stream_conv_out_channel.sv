`timescale 1ns / 1ps

module stream_conv_out_channel
    (
        clk,
        reset,

        pixel_bus_in,
        neigh_idx_in,
        no_op_in,
        pixel_valid_in,

        data_out,
        data_is_valid_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "params/layer_params.v"
   `include "params/weight_params.sv"
   `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   BIT_SHIFT_MODE      = 0;
    //parameter                                                   DATA_WIDTH          = 8;
    parameter                                                   WEIGHT_WIDTH        = (BIT_SHIFT_MODE) ? 3 : DATA_WIDTH;
    parameter                                                   CONV_KERNEL_DIM     = 3; 
    parameter                                                   LAYER_IDX           = 1;
    parameter                                                   OFM_IDX             = 0;
    parameter                                                   IFM_COUNT           = 64;
    parameter                                                   USE_DSP             = 0;
    parameter                                                   WINDOW_SEL_MODE     = 0;
    parameter                                                   MAX_POOL_NEIGH_DIM_WIDTH = 2;
//    parameter                                                   PIPELINE_DELAY_STAGES   = (BIT_SHIFT_MODE) ? 2 : 3;
    parameter                                                   PIPELINE_DELAY_STAGES   = 2;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  PRE_CONV_KERNEL_DIM     = (CONV_KERNEL_DIM+1);
    localparam                                                  PRE_CONV_KERNEL_NEIGH_DIM = PRE_CONV_KERNEL_DIM * PRE_CONV_KERNEL_DIM;
    localparam                                                  ACCUM_DATA_WIDTH        = (BIT_SHIFT_MODE) ? APPROX_SUM_WIDTH : SUM_WIDTH;
    localparam                                                  CONV_KERNEL_NEIGH_DIM   = CONV_KERNEL_DIM * CONV_KERNEL_DIM;
    localparam                                                  CONV_KERNEL_DATA_WIDTH  = CONV_KERNEL_NEIGH_DIM * DATA_WIDTH;
    localparam                                                  PRE_CONV_KERNEL_DATA_WIDTH = PRE_CONV_KERNEL_NEIGH_DIM * DATA_WIDTH;
    localparam                                                  CONV_IFM_BUS_DATA_WIDTH = (WINDOW_SEL_MODE) ?   PRE_CONV_KERNEL_DATA_WIDTH * IFM_COUNT:
                                                                                                                CONV_KERNEL_DATA_WIDTH * IFM_COUNT;
    localparam                                                  EXTRACTED_CONV_IFM_BUS_DATA_WIDTH 
                                                                                        = CONV_KERNEL_DATA_WIDTH * IFM_COUNT;
    localparam                                                  MULT_RES_DATA_WIDTH     = DATA_WIDTH + WEIGHT_WIDTH;
    localparam                                                  CONV_OUT_DATA_WIDTH     = MULT_RES_DATA_WIDTH + count2width(CONV_KERNEL_NEIGH_DIM);
    localparam                                                  CONV_OFM_OUT_DATA_WIDTH = CONV_OUT_DATA_WIDTH + count2width(IFM_COUNT);

    localparam                                                  CONV_RESULT_BUS_DATA_WIDTH = CONV_OUT_DATA_WIDTH * IFM_COUNT; 
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [CONV_IFM_BUS_DATA_WIDTH-1 : 0]                 pixel_bus_in;
    input       [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                neigh_idx_in;
    input                                                       no_op_in;
    input                                                       pixel_valid_in;

    // output      [CONV_OFM_OUT_DATA_WIDTH-1 : 0]                 data_out;
    output reg signed[ACCUM_DATA_WIDTH-1 : 0]                   data_out;
    output reg                                                  data_is_valid_out;
    output reg                                                  data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire                                                        compute_valid;
    wire                                                        is_result_zero;
    wire                                                        is_result_zero_delayed;
    wire                                                        pixel_valid_in_delayed;

    reg  signed [EXTRACTED_CONV_IFM_BUS_DATA_WIDTH-1 : 0]       extracted_pixel_bus;
    reg                                                         extracted_pixel_bus_valid;
    // wire        [DATA_WIDTH-1 : 0]                              i8_bias;                    
    // wire        [CONV_OUT_DATA_WIDTH-1 : 0]                     sign_extend_bias;                    
    wire signed  [ACCUM_DATA_WIDTH-1 : 0]                       bias; 
    
    wire signed [CONV_RESULT_BUS_DATA_WIDTH-1 : 0]              conv_dout;
    wire        [IFM_COUNT-1 : 0]                               conv_data_valid;

    wire signed [CONV_OFM_OUT_DATA_WIDTH-1 : 0]                 conv_ofm_dout;
    wire signed [CONV_OFM_OUT_DATA_WIDTH-1 : 0]                 conv_ofm_with_bias_dout;
    wire signed [ACCUM_DATA_WIDTH-1 : 0]                        conv_ofm_with_bias_dout_se;
    wire                                                        conv_ofm_dout_valid;       
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    // assign i8_bias          = (LAYER_IDX == 0) ? bias_l1[OFM_IDX] : 
    //                           (LAYER_IDX == 1) ? bias_l2[OFM_IDX] : 
    //                           {CONV_OUT_DATA_WIDTH{1'b0}};
    // assign sign_extend_bias = { {(CONV_OUT_DATA_WIDTH-DATA_WIDTH){i8_bias[DATA_WIDTH-1]}}, i8_bias}; 
    // `ifdef DEBUG
        // assign compute_valid    = pixel_valid_in;
    // `else 
    assign compute_valid    = pixel_valid_in & ~no_op_in;
    // `endif

    assign is_result_zero   = pixel_valid_in & no_op_in;
    assign bias             =   bias_l2[OFM_IDX];
                                // (LAYER_IDX == 0) ? ( (BIT_SHIFT_MODE) ? bias_l1_approx[OFM_IDX] : bias_l1[OFM_IDX]) : 
                                // (LAYER_IDX == 1) ? ( (BIT_SHIFT_MODE) ? bias_l2_approx[OFM_IDX] : bias_l2[OFM_IDX]) : 
                                // {CONV_OFM_OUT_DATA_WIDTH{1'b0}};

    shift_reg
    #(
        .CLOCK_CYCLES   (PIPELINE_DELAY_STAGES),
        .DATA_WIDTH     (1)
    )   
    u_shift_reg_zero
    (   
        .clk            (clk),

        .enable         (1'b1),
        .data_in        (is_result_zero),
        .data_out       (is_result_zero_delayed)
    );

    shift_reg
    #(
        .CLOCK_CYCLES   (PIPELINE_DELAY_STAGES),
        .DATA_WIDTH     (1)
    )   
    u_shift_reg_valid
    (   
        .clk            (clk),

        .enable         (1'b1),
        .data_in        (pixel_valid_in),
        .data_out       (pixel_valid_in_delayed)
    );
    

    genvar i;
    generate 
        if(WINDOW_SEL_MODE) begin
            neigh_bus_mux
            #(
                .CHANNEL_COUNT      (IFM_COUNT),
                .DATA_WIDTH         (DATA_WIDTH),
                .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
                .MAX_POOL_NEIGH_DIM_WIDTH   
                                    (MAX_POOL_NEIGH_DIM_WIDTH)
            )
            u_neigh_bus_mux_l1
            (
                .clk                (clk),
                .reset              (reset),

                .neigh_in           (pixel_bus_in),
                .neigh_idx_in       (neigh_idx_in),
                .neigh_idx_valid_in (compute_valid),
                
                .neigh_out          (extracted_pixel_bus),
                .neigh_valid_out    (extracted_pixel_bus_valid)
            );
        end
        else begin
            // if(BIT_SHIFT_MODE) begin
            always@(*) begin
                extracted_pixel_bus       = pixel_bus_in;
                extracted_pixel_bus_valid = compute_valid;
            end 
            // end
            // else begin
            //     always@(posedge clk) begin
            //         if(reset) begin
            //             extracted_pixel_bus             <= {EXTRACTED_CONV_IFM_BUS_DATA_WIDTH{1'b0}};
            //             extracted_pixel_bus_valid       <= 1'b0;
            //         end
            //         else begin
            //             if(compute_valid) begin
            //                 extracted_pixel_bus         <= pixel_bus_in;
            //                 extracted_pixel_bus_valid   <= 1'b1;
            //             end
            //             else begin
            //                 extracted_pixel_bus_valid   <= 1'b0;
            //             end
            //         end
            //     end
            // end
        end

        for(i=0; i<IFM_COUNT; i=i+1) begin
            conv_elem
            #(
                .DATA_WIDTH         (DATA_WIDTH),
                .WEIGHT_WIDTH       (WEIGHT_WIDTH),
                .BIT_SHIFT_MODE     (BIT_SHIFT_MODE),
                .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
                .USE_DSP            (USE_DSP),
                .LAYER_IDX          (LAYER_IDX),
                .WEIGHT_INTERNAL    (1),
                .WEIGHT_KERNEL_IDX  ( (OFM_IDX * IFM_COUNT + i) * CONV_KERNEL_NEIGH_DIM)
            )
            u_conv_elem
            (
                .clk                (clk),
                .reset              (reset),

                .weight_bus_in      ({(CONV_KERNEL_NEIGH_DIM * DATA_WIDTH){1'b0}}),
                .pixel_bus_in       (extracted_pixel_bus   [i*CONV_KERNEL_DATA_WIDTH +: CONV_KERNEL_DATA_WIDTH]),
                .pixel_valid_in     (extracted_pixel_bus_valid),

                .pixel_out          (conv_dout      [i*CONV_OUT_DATA_WIDTH +: CONV_OUT_DATA_WIDTH]),
                .pixel_valid_out    (conv_data_valid[i])
            );
        end

        if(IFM_COUNT == 64) begin
            // adder_tree_21   
            // #(
            //     .DATA_WIDTH     (CONV_OUT_DATA_WIDTH)
            // )
            // u_adder_tree
            // (
            //     .clk            (clk),
            //     .reset          (reset),

            //     .data_in        ({sign_extend_bias, conv_dout}),
            //     .data_valid_in  (conv_data_valid[0]),

            //     .data_out       (conv_ofm_dout),
            //     .data_valid_out (conv_ofm_dout_valid)
            // );
            adder_tree_64   
            #(
                .DATA_WIDTH     (CONV_OUT_DATA_WIDTH)
            )
            u_adder_tree
            (
                .clk            (clk),
                .reset          (reset),

                .data_in        (conv_dout),
                .data_valid_in  (conv_data_valid[0]),

                .data_out       (conv_ofm_dout),
                .data_valid_out (conv_ofm_dout_valid)
            );

            // assign conv_ofm_with_bias_dout = (BIT_SHIFT_MODE) ? conv_ofm_dout : (conv_ofm_dout + bias);
            assign conv_ofm_with_bias_dout = conv_ofm_dout + bias;
            sign_extend
            #(
                .INPUT_WIDTH    (CONV_OFM_OUT_DATA_WIDTH),
                .OUTPUT_WIDTH   (ACCUM_DATA_WIDTH)
            )
            u_data_out_se
            (
                .data_in        (conv_ofm_with_bias_dout),
                .data_out       (conv_ofm_with_bias_dout_se)
            );

            if(BIT_SHIFT_MODE) begin
                always@(*) begin
                    data_out        = conv_ofm_with_bias_dout_se;
                    data_is_valid_out
                                    = 1'b0;
                    data_valid_out  = conv_ofm_dout_valid;
                end
            end
            else begin
                always@(posedge clk) begin
                    if(reset) begin
                        data_out            <= {ACCUM_DATA_WIDTH{1'b0}};
                        data_is_valid_out   <= 1'b0;
                        data_valid_out      <= 1'b0;
                    end
                    else begin
                        data_valid_out      <= pixel_valid_in_delayed;
                        if(pixel_valid_in_delayed) begin
                            if(is_result_zero_delayed) begin
                                data_is_valid_out   
                                            <= 1'b0;
                                // data_out    <= {ACCUM_DATA_WIDTH{1'b0}};        
                                // data_out    <= {1'b1, {(ACCUM_DATA_WIDTH-2){1'b0}}, 1'b1};        
                            end
                            else begin
                                data_is_valid_out   
                                            <= 1'b1;
                                data_out    <= conv_ofm_with_bias_dout_se;
                            end
                        end
                    end
                end
            end
            // assign data_out       = (is_result_zero_delayed) ? {ACCUM_DATA_WIDTH{1'b0}} : conv_ofm_with_bias_dout_se;
            // assign data_valid_out = (is_result_zero_delayed) ? 1'b1 : conv_ofm_dout_valid;
        end
        else begin
            // assign data_out       = conv_dout;
            // assign conv_ofm_with_bias_dout = (BIT_SHIFT_MODE) ? conv_dout : (conv_dout + bias);
            
            // assign data_out       = (is_result_zero_delayed) ? {ACCUM_DATA_WIDTH{1'b0}} : conv_ofm_with_bias_dout_se;
            // assign data_valid_out = (is_result_zero_delayed) ? 1'b1 : conv_data_valid[0];
            assign conv_ofm_with_bias_dout = conv_dout + bias;
            sign_extend
            #(
                .INPUT_WIDTH    (CONV_OFM_OUT_DATA_WIDTH),
                .OUTPUT_WIDTH   (ACCUM_DATA_WIDTH)
            )
            u_data_out_se
            (
                .data_in        (conv_ofm_with_bias_dout),
                .data_out       (conv_ofm_with_bias_dout_se)
            );
            if(BIT_SHIFT_MODE) begin
                always@(*) begin
                    data_out        = conv_ofm_with_bias_dout_se;
                    data_is_valid_out
                                    = 1'b1;
                    data_valid_out  = conv_data_valid[0];
                end
            end
            else begin
                always@(posedge clk) begin
                    if(reset) begin
                        data_out            <= {ACCUM_DATA_WIDTH{1'b0}};
                        data_is_valid_out   <= 1'b0;
                        data_valid_out      <= 1'b0;
                    end
                    else begin
                        data_valid_out      <= pixel_valid_in_delayed;
                        if(pixel_valid_in_delayed) begin
                            if(is_result_zero_delayed) begin
                                // data_out    <= {ACCUM_DATA_WIDTH{1'b0}};       
                                data_is_valid_out   
                                            <= 1'b0; 
                                // data_out    <= {1'b1, {(ACCUM_DATA_WIDTH-2){1'b0}}, 1'b1};        
                            end
                            else begin
                                data_is_valid_out   
                                            <= 1'b1;
                                data_out    <= conv_ofm_with_bias_dout_se;
                            end
                        end
                    end
                end
            end
        end
    endgenerate

endmodule
