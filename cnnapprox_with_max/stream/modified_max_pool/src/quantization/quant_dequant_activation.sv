`timescale 1ns / 1ps

module quant_dequant_activation
    (
        clk,
        reset,

        pixel_in,
        pixel_valid_in,

        pixel_out,
        pixel_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
   `include "params/weight_params.sv"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   IN_DATA_WIDTH        = 32;
    parameter                                                   LAYER_IDX            = 0;
    parameter                                                   FM_IDX               = 0;
    parameter                                                   ACTIVATION_TYPE      = "none";
    parameter                                                   OUT_DATA_WIDTH       = 8;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [IN_DATA_WIDTH-1 : 0]                           pixel_in;
    input                                                       pixel_valid_in;

    output      [OUT_DATA_WIDTH-1 : 0]                          pixel_out;
    output                                                      pixel_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [31 : 0]                                        fixed_in_data;
    //params
    wire        [31 : 0]                                        dequant_factor;
    wire        [31 : 0]                                        quant_factor;
    wire        [31 : 0]                                        dequant_quant_factor;
    // wire        [31 : 0]                                        bias;

    wire        [31 : 0]                                        pixel_float;
    wire                                                        pixel_float_valid; 

    // wire        [31 : 0]                                        dequant_with_bias_result;
    // wire                                                        dequant_with_bias_result_valid;    

    wire        [31 : 0]                                        fquant_result;
    wire                                                        fquant_result_valid;    
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    // assign dequant_factor       = dequant_factors[LAYER_IDX];
    // assign quant_factor         = quant_factors[LAYER_IDX];
   // assign dequant_quant_factor = quant_dequant_factors[LAYER_IDX];
    // assign bias           = (LAYER_IDX == 0) ? l1_bias_terms[FM_IDX] : 
    //                         (LAYER_IDX == 1) ? l2_bias_terms[FM_IDX] : 
    //                         {32'h0};
    
    sign_extend
    #(
        .INPUT_WIDTH    (IN_DATA_WIDTH),
        .OUTPUT_WIDTH   (32)
    )
    u_data_out_se
    (
        .data_in        (pixel_in),
        .data_out       (fixed_in_data)
    );
    //convert to float
    fixed2float 
    u_fixed2float
    (
        .aclk                   (clk),                                 
        .s_axis_a_tvalid        (pixel_valid_in),           
        .s_axis_a_tdata         (fixed_in_data),             
        .m_axis_result_tvalid   (pixel_float_valid), 
        .m_axis_result_tdata    (pixel_float)    
    );

    generate;
        if(ACTIVATION_TYPE == "none") begin
            quantize_scale_multiply 
            u_quantize_scale_multiply 
            (
                .aclk                   (clk),                  
                .aclken                 (1'b1),                
                .s_axis_a_tvalid        (pixel_float_valid),       
                .s_axis_a_tdata         (pixel_float),        
                .s_axis_b_tvalid        (pixel_float_valid),       
                .s_axis_b_tdata         (dequant_quant_factor),        
                .m_axis_result_tvalid   (fquant_result_valid),  
                .m_axis_result_tdata    (fquant_result)    
            );
        end
        else begin
            // dequant_with_bias 
            // u_dequant_with_bias  
            // (
            //     .aclk                   (clk),                  
            //     .s_axis_a_tvalid        (pixel_float_valid),       
            //     .s_axis_a_tdata         (dequant_factor),        
            //     .s_axis_b_tvalid        (pixel_float_valid),       
            //     .s_axis_b_tdata         (pixel_float),        
            //     .s_axis_c_tvalid        (pixel_float_valid),       
            //     .s_axis_c_tdata         (bias),        
            //     .m_axis_result_tvalid   (dequant_with_bias_result_valid),  
            //     .m_axis_result_tdata    (dequant_with_bias_result)    
            // );

            // quantize_scale_multiply 
            // u_quantize_scale_multiply 
            // (
            //     .aclk                   (clk),                  
            //     .aclken                 (1'b1),                
            //     .s_axis_a_tvalid        (dequant_with_bias_result_valid),       
            //     .s_axis_a_tdata         (dequant_with_bias_result),        
            //     .s_axis_b_tvalid        (dequant_with_bias_result_valid),       
            //     .s_axis_b_tdata         (quant_factor),        
            //     .m_axis_result_tvalid   (fquant_result_valid),  
            //     .m_axis_result_tdata    (fquant_result)    
            // );
        end
    endgenerate
    

    float2fixed 
    u_float2fixed
    (
        .aclk                   (clk),                 
        .aclken                 (1'b1),               
        .s_axis_a_tvalid        (fquant_result_valid),      
        .s_axis_a_tdata         (fquant_result),       
        .m_axis_result_tvalid   (pixel_valid_out), 
        .m_axis_result_tdata    (pixel_out)   
    );

endmodule
