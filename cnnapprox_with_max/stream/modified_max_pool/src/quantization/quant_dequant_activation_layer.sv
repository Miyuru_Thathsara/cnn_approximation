`timescale 1ns / 1ps

module quant_dequant_activation_layer
    (
        clk,
        reset,

        pixel_in,
        pixel_valid_in,

        pixel_out,
        pixel_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   IN_DATA_WIDTH        = 32;
    parameter                                                   OUT_DATA_WIDTH       = 8;
    parameter                                                   ACTIVATION_TYPE      = "none";
    parameter                                                   MODE                 = "int"; 
    parameter                                                   OFM_COUNT            = 1;
    parameter                                                   LAYER_IDX            = 0;
    parameter                                                   SCALE_FACTOR_A       = 9;
    parameter                                                   SCALE_FACTOR_B       = 14;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [IN_DATA_WIDTH * OFM_COUNT-1 : 0]               pixel_in;
    input                                                       pixel_valid_in;

    output      [OUT_DATA_WIDTH * OFM_COUNT-1 : 0]              pixel_out;
    output                                                      pixel_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [OFM_COUNT-1 : 0]                               out_valid;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    genvar i;
    generate;
        for(i=0; i<OFM_COUNT; i=i+1) begin
            if(MODE == "int") begin
                quant_dequant_activation_int
                #(
                    .IN_DATA_WIDTH          (IN_DATA_WIDTH),
                    .OUT_DATA_WIDTH         (OUT_DATA_WIDTH),
                    .LAYER_IDX              (LAYER_IDX),
                    .FM_IDX                 (i),
                    .ACTIVATION_TYPE        (ACTIVATION_TYPE),
                    .SCALE_FACTOR_A         (SCALE_FACTOR_A),
                    .SCALE_FACTOR_B         (SCALE_FACTOR_B)
                )
                u_quant_dequant_activation_int
                (
                    .clk                    (clk),
                    .reset                  (reset),

                    .pixel_in               (pixel_in   [i * IN_DATA_WIDTH +: IN_DATA_WIDTH]),
                    .pixel_valid_in         (pixel_valid_in),

                    .pixel_out              (pixel_out  [i * OUT_DATA_WIDTH +: OUT_DATA_WIDTH]),
                    .pixel_valid_out        (out_valid  [i])
                );  
            end
            else begin
                quant_dequant_activation
                #(
                    .IN_DATA_WIDTH          (IN_DATA_WIDTH),
                    .OUT_DATA_WIDTH         (OUT_DATA_WIDTH),
                    .LAYER_IDX              (LAYER_IDX),
                    .FM_IDX                 (i),
                    .ACTIVATION_TYPE        (ACTIVATION_TYPE)
                )
                u_quant_dequant_activation
                (
                    .clk                    (clk),
                    .reset                  (reset),

                    .pixel_in               (pixel_in   [i * IN_DATA_WIDTH +: IN_DATA_WIDTH]),
                    .pixel_valid_in         (pixel_valid_in),

                    .pixel_out              (pixel_out  [i * OUT_DATA_WIDTH +: OUT_DATA_WIDTH]),
                    .pixel_valid_out        (out_valid  [i])
                );  
            end
        end
    endgenerate
    assign pixel_valid_out = out_valid[0];

endmodule