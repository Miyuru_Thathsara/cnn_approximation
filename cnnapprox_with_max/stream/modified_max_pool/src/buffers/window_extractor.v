`timescale 1ns / 1ps

module window_extractor
    (
        clk,
        reset,

        //write port
        line_buffer_addr_in,
        row_count_in,
        line_buff_data_in,
        pix_bus_valid_in,

        //read port
        line_out,
        line_valid_out,
        window_out,
        window_valid_out,
        window_valid_prev_out,
        window_buff_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
  
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   DATA_WIDTH      = 8;
    parameter                                                   NUM_IFM         = 64;
    parameter                                                   CONV_KERNEL_DIM = 3;
    parameter                                                   DIM_WIDTH       = 8;
    // parameter                                                   APPROX_MODE     = 1;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  LINE_BUS_DIM          = NUM_IFM * CONV_KERNEL_DIM;
    localparam                                                  LINE_BUS_DATA_WIDTH   = LINE_BUS_DIM * DATA_WIDTH;
    localparam                                                  WINDOW_DIM            = CONV_KERNEL_DIM * CONV_KERNEL_DIM;
    localparam                                                  WINDOW_BUS_DIM        = WINDOW_DIM * NUM_IFM;
    localparam                                                  WINDOW_BUS_DATA_WIDTH = WINDOW_BUS_DIM * DATA_WIDTH;
    // localparam                                                  VALID_DIM_THRESH = (APPROX_MODE) ? (CONV_KERNEL_DIM-2): (CONV_KERNEL_DIM-1);
    localparam                                                  VALID_DIM_THRESH = (CONV_KERNEL_DIM-1);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    //input port
    input       [DIM_WIDTH-1 : 0]                               line_buffer_addr_in;
    input       [DIM_WIDTH-1 : 0]                               row_count_in;
    // input       [DATA_WIDTH * (CONV_KERNEL_DIM-1) -1 : 0]       line_buff_data_in;
    input       [LINE_BUS_DATA_WIDTH-1 : 0]                     line_buff_data_in;
    input                                                       pix_bus_valid_in;
    
    //output port
    output      [LINE_BUS_DATA_WIDTH - 1 : 0]                   line_out;
    output reg                                                  line_valid_out;

    output  reg [WINDOW_BUS_DATA_WIDTH-1 : 0]                   window_out;
    output  reg                                                 window_valid_out;
    output                                                      window_valid_prev_out;
    output  reg                                                 window_buff_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [WINDOW_BUS_DATA_WIDTH-1 : 0]                   window_mem_in;
    wire        [WINDOW_BUS_DATA_WIDTH-1 : 0]                   window_mem_out;
    // reg         [DATA_WIDTH-1 : 0]                              window_reg_debug    [WINDOW_DIM-1 : 0];
    reg                                                         window_valid_reg;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
//     always @(posedge clk) begin
//         if(reset) begin
//             line_valid_out                  <= 1'b0;

//             window_valid_out                <= 1'b0;
//             window_buff_valid_out           <= 1'b0;
//             //window_out                      <= {(WINDOW_DIM * WINDOW_DIM * DATA_WIDTH){1'b0}};
//         end
//         else begin
//             // if(pix_bus_valid_reg) begin
//             if(pix_bus_valid_in) begin
//                 // window_out                  <= {line_buff_din, window_out[CONV_KERNEL_DIM * DATA_WIDTH +: (CONV_KERNEL_DIM-1) * CONV_KERNEL_DIM * DATA_WIDTH]};
//                 //window_out                  <= {line_buff_wrt_data_out, window_out[CONV_KERNEL_DIM * DATA_WIDTH +: (CONV_KERNEL_DIM-1) * CONV_KERNEL_DIM * DATA_WIDTH]};
//                 window_out                  <= {line_buff_data_in, window_out[CONV_KERNEL_DIM * DATA_WIDTH +: (CONV_KERNEL_DIM-1) * CONV_KERNEL_DIM * DATA_WIDTH]};
//                 window_valid_out            <= 1'b0;
//                 window_buff_valid_out       <= 1'b0;  
//                 //if(row_count_delayed >= CONV_KERNEL_DIM - 1) begin
//                 if(row_count_in >= VALID_DIM_THRESH) begin
//                     // if(line_buffer_addr_delayed >= CONV_KERNEL_DIM - 1) begin
// //                    if(row_count_in > VALID_DIM_THRESH) begin
// //                        line_valid_out      <= 1'b1;
// //                    end 
//                     if(line_buffer_addr_in >= VALID_DIM_THRESH) begin
//                         window_valid_out    <= 1'b1;
//                         if(row_count_in % 2 == 1 && line_buffer_addr_in % 2 == 1) begin
//                             window_buff_valid_out
//                                             <= 1'b1;        
//                         end 
//                     end
//                 end
//             end
//             else begin 
//                 line_valid_out              <= 1'b0;
//                 window_valid_out            <= 1'b0;
//                 window_buff_valid_out       <= 1'b0;    
//             end
//         end
//     end
//     // integer i;
//     // always@(*) begin
//     //     for(i=0; i<WINDOW_DIM; i=i+1) begin
//     //         window_reg_debug[i] = window_out[i*DATA_WIDTH +: DATA_WIDTH];
//     //     end 
//     // end

//     assign line_out = window_out[(WINDOW_DIM * DATA_WIDTH-1) -: (CONV_KERNEL_DIM * DATA_WIDTH)];
    
    assign window_mem_in = {line_buff_data_in, window_mem_out[LINE_BUS_DATA_WIDTH +: (WINDOW_BUS_DATA_WIDTH-LINE_BUS_DATA_WIDTH)]};
    genvar i;
    generate
        for(i=0; i<CONV_KERNEL_DIM; i=i+1) begin
            if(NUM_IFM == 1) begin
                window_buff_column_1
                u_window_buff_column_1
                (
                    .clka       (clk),
                    .ena        (1'b1), 
                    .wea        (pix_bus_valid_in),     
                    .addra      (1'h0), 
                    .dina       (window_mem_in  [i*LINE_BUS_DATA_WIDTH +: LINE_BUS_DATA_WIDTH]),   

                    // .clkb       (clk),
                    // .enb        (1'b1), 
                    // .addrb      (1'h0),  
                    .douta      (window_mem_out [i*LINE_BUS_DATA_WIDTH +: LINE_BUS_DATA_WIDTH])
                );
            end
            else if(NUM_IFM == 20) begin
                window_buff_column_20
                u_window_buff_column_20
                (
                    .clka       (clk),
                    .ena        (1'b1), 
                    .wea        (pix_bus_valid_in),     
                    .addra      (1'h0), 
                    .dina       (window_mem_in  [i*LINE_BUS_DATA_WIDTH +: LINE_BUS_DATA_WIDTH]),   

                    // .clkb       (clk),
                    // .enb        (1'b1), 
                    // .addrb      (1'h0),  
                    .douta      (window_mem_out [i*LINE_BUS_DATA_WIDTH +: LINE_BUS_DATA_WIDTH])
                );
            end
        end
    endgenerate

    always @(posedge clk) begin
        if(reset) begin
            window_out                      <= {WINDOW_BUS_DATA_WIDTH{1'b0}};

            window_valid_reg                <= 1'b0;
            window_valid_out                <= 1'b0;
        end
        else begin
            if(pix_bus_valid_in) begin
                window_valid_reg            <= 1'b0;
                if(row_count_in >= VALID_DIM_THRESH) begin
                    if(line_buffer_addr_in >= VALID_DIM_THRESH) begin
                        window_valid_reg    <= 1'b1;
                    end
                end
            end
            else begin 
                window_valid_reg            <= 1'b0;    
            end

            window_valid_out                <= window_valid_reg;
            if(window_valid_reg) begin
                window_out                  <= window_mem_out;
            end
        end
    end
//    assign window_out = (window_valid_out) ? window_mem_out : {WINDOW_BUS_DATA_WIDTH{1'b0}};
    // assign window_out = window_mem_out;
    assign window_valid_prev_out = window_valid_reg;

endmodule  
