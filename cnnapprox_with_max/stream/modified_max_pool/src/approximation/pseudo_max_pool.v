`timescale 1ns / 1ps

module pseudo_max_pool_layer
    (
        clk,
        reset,

        //RX iface
        rx_data_bus_in,
        rx_data_valid_bus_in,
        rx_row_valid_in,
        
        //TX interface
        tx_data_bus_out,
        tx_data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                         "params/global_params.v"
    `include                                                        "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       CONV_IM_DIM     = 24;
    parameter                                                       MAX_POOL_DIM    = 2;
    parameter                                                       DATA_WIDTH      = 8;
    parameter                                                       NUM_FM          = 20;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       DIM_WIDTH                = 8; 
    localparam                                                      MAX_POOL_NEIGH_DIM_WIDTH = count2width(MAX_POOL_DIM * MAX_POOL_DIM);
    localparam                                                      INDEX_CACHE_ADDR_WIDTH   = count2width(CONV_IM_DIM/2);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                           clk;
    input                                                           reset;

    //RX interface
    input       [NUM_FM * DATA_WIDTH-1: 0]                          rx_data_bus_in;
    input       [NUM_FM-1 : 0]                                      rx_data_valid_bus_in;
    input                                                           rx_row_valid_in;

    //TX interface
    output      [NUM_FM * DATA_WIDTH-1: 0]                          tx_data_bus_out;
    output reg                                                      tx_data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    //row, col state nets/regs
    wire        [DIM_WIDTH-1 : 0]                                   col_count;
    wire        [DIM_WIDTH-1 : 0]                                   row_count;
    wire        [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                    current_max_pool_neigh_idx;
    reg         [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                    current_max_pool_neigh_idx_reg;
    wire                                                            is_row_odd;
    reg                                                             is_row_odd_reg;
    wire                                                            is_col_odd;
    reg                                                             is_col_odd_reg;
    wire                                                            row_last_act;
    reg                                                             row_last_act_reg;
    wire                                                            im_last_act;

    //input registers
    reg        [NUM_FM * DATA_WIDTH-1: 0]                           rx_data_bus_reg;
    reg        [NUM_FM-1 : 0]                                       rx_data_valid_bus_reg;
    reg                                                             rx_row_valid_reg;

    // buffer signals
    // wire        [INDEX_CACHE_ADDR_WIDTH-1 : 0]                      max_pool_index_cache_wrt_addr;
    // wire        [INDEX_CACHE_ADDR_WIDTH-1 : 0]                      max_pool_index_cache_rd_addr;
    wire        [NUM_FM-1 : 0]                                      data_bus_cache_wrt_en;
    wire        [INDEX_CACHE_ADDR_WIDTH-1 : 0]                      data_bus_cache_addr;
    wire        [NUM_FM * DATA_WIDTH-1: 0]                          data_bus_cache_rd_data;

    //tx signals
    wire                                                            tx_data_valid;

    // //index comparator signals
    // reg                                                             index_comparator_in_valid;
    // reg         [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]          max_pool_index;    

    // integer                                                         state;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    //The following block maintains the row, column state w.r.t. the window valid
    feature_map_state_handler
    #(
        .CONV_IM_DIM            (CONV_IM_DIM),
        .DIM_PREDEFINED         (1)
    )
    u_feature_map_state_handler
    (
        .clk                    (clk),
        .reset                  (reset),

        .cfg_row_count_in       (),
        .cfg_col_count_in       (),
        .cfg_valid_in           (),

        .valid_in               (rx_row_valid_in),

        .row_count_out          (row_count),
        .col_count_out          (col_count),
        .max_pool_neigh_idx_out (current_max_pool_neigh_idx),

        .is_row_odd_out         (is_row_odd),
        .is_col_odd_out         (is_col_odd),
        .row_last_act_out       (row_last_act),
        .im_last_act_out        (im_last_act)
    );
    assign data_bus_cache_wrt_en = (~is_row_odd) ? rx_data_valid_bus_in : {NUM_FM{1'b0}};
    assign data_bus_cache_addr   = col_count/MAX_POOL_DIM;

    always@(posedge clk) begin
        if(reset) begin
            rx_data_bus_reg         <= {(NUM_FM * DATA_WIDTH){1'b0}};
            rx_data_valid_bus_reg   <= {NUM_FM{1'b0}};
            rx_row_valid_reg        <= 1'b0;
            is_row_odd_reg          <= 1'b0;
            is_col_odd_reg          <= 1'b0;
        end
        else begin
            rx_data_bus_reg         <= rx_data_bus_in;
            rx_data_valid_bus_reg   <= rx_data_valid_bus_in;
            rx_row_valid_reg        <= rx_row_valid_in;
            is_row_odd_reg          <= is_row_odd;
            is_col_odd_reg          <= is_col_odd;
        end
    end
    
    // always@(posedge clk) begin
    //     if(reset) begin
    //         current_max_pool_neigh_idx_reg  <= {MAX_POOL_NEIGH_DIM_WIDTH{1'b0}};
    //         index_comparator_in_valid       <= 1'b0;
    //         state                           <= STATE_EVEN_ROW;
    //     end
    //     else begin
    //         current_max_pool_neigh_idx_reg  <= current_max_pool_neigh_idx;
    //         index_comparator_in_valid       <= rx_window_valid_prev_in;
    //         case(state)
    //             STATE_EVEN_ROW : begin
    //                 if(rx_window_valid_prev_in && row_last_act) begin
    //                     state               <= STATE_ODD_ROW;
    //                 end
    //             end
    //             STATE_ODD_ROW : begin
    //                 if(rx_window_valid_prev_in && row_last_act) begin
    //                     state               <= STATE_EVEN_ROW;
    //                 end
    //             end
    //         endcase
    //     end
    // end

    // always@(*) begin
    //     case(state)
    //         STATE_EVEN_ROW : begin
    //             max_pool_index              = rx_max_pool_idx_in;
    //             max_pool_index_cache_wrt_en = rx_max_pool_idx_valid_in;
    //         end
    //         STATE_ODD_ROW : begin
    //             max_pool_index              = max_pool_index_cache_rd_data;
    //             max_pool_index_cache_wrt_en = 1'b0;
    //         end
    //         default : begin
    //             max_pool_index              = {(NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH) {1'b0}}; 
    //             max_pool_index_cache_wrt_en = 1'b0;
    //         end
    //     endcase
    // end

    generate 
        if(NUM_FM == 20) begin
            layer_out_line_buff_20
            u_layer_out_line_buff_20
            ( 
                .clka       (clk),
                .ena        (1'b1),      
                .wea        (data_bus_cache_wrt_en),
                .addra      (data_bus_cache_addr),  
                .dina       (rx_data_bus_in),  
                .douta      (data_bus_cache_rd_data)
            );
        end 
        else if(NUM_FM == 50) begin
            layer_out_line_buff_50
            u_layer_out_line_buff_50
            (
                .clka       (clk),                  
                .ena        (1'b1),                 
                .wea        (data_bus_cache_wrt_en),     
                .addra      (data_bus_cache_addr),  
                .dina       (rx_data_bus_in),
                .douta      (data_bus_cache_rd_data)
            );
        end
    endgenerate

    // genvar i;
    // generate
    //     for(i=0; i<NUM_OFM; i=i+1) begin
    //         index_comparator
    //         u_index_comparator
    //         (
    //             //data receive port
    //             .current_idx_in         (current_max_pool_neigh_idx_reg),
    //             .max_idx_in             (max_pool_index    [i*MAX_POOL_NEIGH_DIM_WIDTH +: MAX_POOL_NEIGH_DIM_WIDTH]),
    //             .valid_in               (index_comparator_in_valid), //todo : change after validating synchronization

    //             //data transmit port
    //             .no_op_out              (no_op_out      [i]),
    //             .valid_out              ()
    //         );
    //     end
    // endgenerate
    // assign no_op_valid_out = index_comparator_in_valid;

    // // assign no_op_valid_out = tx_op_cmd_valid_bus[0];

    //async implementations
    // integer i;
    // always@(*) begin
    //     for(i=0; i<NUM_FM; i=i+1) begin
    //         if(rx_data_valid_bus_reg[i]) begin
    //             tx_data_bus_out[i*DATA_WIDTH +: DATA_WIDTH] = rx_data_bus_reg[i*DATA_WIDTH +: DATA_WIDTH];
    //         end
    //         else begin
    //             tx_data_bus_out[i*DATA_WIDTH +: DATA_WIDTH] = data_bus_cache_rd_data[i*DATA_WIDTH +: DATA_WIDTH];
    //         end
    //     end
    // end

    // assign tx_data_valid_out = is_row_odd_reg && is_col_odd_reg && rx_row_valid_reg;
    
    // assign tx_data_valid = is_row_odd_reg && is_col_odd_reg && rx_row_valid_reg;
    // integer i;
    
    // always@(posedge clk) begin
    //     if(reset) begin
    //         tx_data_bus_out                                         <= {(NUM_FM * DATA_WIDTH){1'b0}};
    //         tx_data_valid_out                                       <= 1'b0;
    //     end
    //     else begin
    //         tx_data_valid_out                                       <= tx_data_valid;
    //         for(i=0; i<NUM_FM; i=i+1) begin
    //             if(tx_data_valid) begin
    //                 if(rx_data_valid_bus_reg[i]) begin
    //                     tx_data_bus_out[i*DATA_WIDTH +: DATA_WIDTH] <= rx_data_bus_reg[i*DATA_WIDTH +: DATA_WIDTH];
    //                 end
    //                 else if
    //                 else begin
    //                     tx_data_bus_out[i*DATA_WIDTH +: DATA_WIDTH] <= data_bus_cache_rd_data[i*DATA_WIDTH +: DATA_WIDTH];
    //                 end
    //             end 
    //         end
    //     end
    // end

    assign tx_data_valid = is_row_odd && is_col_odd && rx_row_valid_in;
    genvar i;
    generate
        for(i=0; i<NUM_FM; i=i+1) begin
            pseudo_max_pool_mux
            u_pseudo_max_pool_mux
            (
                .clk                        (clk),
                .reset                      (reset),

                //RX port
                .rx_data_in                 (rx_data_bus_in         [i * DATA_WIDTH +: DATA_WIDTH]),
                .rx_data_reg_in             (rx_data_bus_reg        [i * DATA_WIDTH +: DATA_WIDTH]),
                .rx_cache_rd_data_in        (data_bus_cache_rd_data [i * DATA_WIDTH +: DATA_WIDTH]),
                .rx_is_data_valid_in        (rx_data_valid_bus_in   [i]),
                .rx_is_data_valid_reg_in    (rx_data_valid_bus_reg  [i]),
                .rx_valid_in                (tx_data_valid),

                //TX port
                .tx_data_out                (tx_data_bus_out        [i*DATA_WIDTH +: DATA_WIDTH]),
                .tx_data_valid_out          ()
            );
        end
    endgenerate

    always@(posedge clk) begin
        if(reset) begin
            tx_data_valid_out   <= 1'b0;
        end
        else begin
            tx_data_valid_out   <= tx_data_valid;
        end
    end

endmodule
