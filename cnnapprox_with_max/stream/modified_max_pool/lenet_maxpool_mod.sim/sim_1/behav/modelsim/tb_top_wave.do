onerror {resume}
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4607:4536]} pixel_bus_in64
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4535:4464]} pixel_bus_in63
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4463:4392]} pixel_bus_in62
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4391:4320]} pixel_bus_in61
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4319:4248]} pixel_bus_in60
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4247:4176]} pixel_bus_in59
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4175:4104]} pixel_bus_in58
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4103:4032]} pixel_bus_in57
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[4031:3960]} pixel_bus_in56
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3959:3888]} pixel_bus_in55
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3887:3816]} pixel_bus_in54
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3815:3744]} pixel_bus_in53
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3743:3672]} pixel_bus_in52
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3671:3600]} pixel_bus_in51
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3599:3528]} pixel_bus_in50
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3527:3456]} pixel_bus_in49
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3455:3384]} pixel_bus_in48
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3383:3312]} pixel_bus_in47
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3311:3240]} pixel_bus_in46
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3239:3168]} pixel_bus_in45
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3167:3096]} pixel_bus_in44
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3095:3024]} pixel_bus_in43
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[3023:2952]} pixel_bus_in42
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2951:2880]} pixel_bus_in41
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2879:2808]} pixel_bus_in40
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2807:2736]} pixel_bus_in39
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2735:2664]} pixel_bus_in38
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2663:2592]} pixel_bus_in37
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2591:2520]} pixel_bus_in36
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2519:2448]} pixel_bus_in35
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2447:2376]} pixel_bus_in34
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2375:2304]} pixel_bus_in33
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2303:2232]} pixel_bus_in32
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2231:2160]} pixel_bus_in31
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2159:2088]} pixel_bus_in30
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2087:2016]} pixel_bus_in29
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[2015:1944]} pixel_bus_in28
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1943:1872]} pixel_bus_in27
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1871:1800]} pixel_bus_in26
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1799:1728]} pixel_bus_in25
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1727:1656]} pixel_bus_in24
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1655:1584]} pixel_bus_in23
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1583:1512]} pixel_bus_in22
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1511:1440]} pixel_bus_in21
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1439:1368]} pixel_bus_in20
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1367:1296]} pixel_bus_in19
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1295:1224]} pixel_bus_in18
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1223:1152]} pixel_bus_in17
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1151:1080]} pixel_bus_in16
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1079:1008]} pixel_bus_in15
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[1007:936]} pixel_bus_in14
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[935:864]} pixel_bus_in13
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[863:792]} pixel_bus_in12
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[791:720]} pixel_bus_in11
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[719:648]} pixel_bus_in10
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[647:576]} pixel_bus_in9
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[575:504]} pixel_bus_in8
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[503:432]} pixel_bus_in7
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[431:360]} pixel_bus_in6
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[359:288]} pixel_bus_in5
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[287:216]} pixel_bus_in4
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[215:144]} pixel_bus_in3
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[143:72]} pixel_bus_in2
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in[71:0]} pixel_bus_in1
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[431:405]} data_out16
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[404:378]} data_out15
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[377:351]} data_out14
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[350:324]} data_out13
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[323:297]} data_out12
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[296:270]} data_out11
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[269:243]} data_out10
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[242:216]} data_out9
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[215:189]} data_out8
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[188:162]} data_out7
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[161:135]} data_out6
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[134:108]} data_out5
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[107:81]} data_out4
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[80:54]} data_out3
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[53:27]} data_out2
quietly virtual signal -install /tb_top/u_conv_top/u_conv_layer_l2 { /tb_top/u_conv_top/u_conv_layer_l2/data_out[26:0]} data_out1
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[71:64]} pixel_bus_in9
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[63:56]} pixel_bus_in8
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[55:48]} pixel_bus_in7
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[47:40]} pixel_bus_in6
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[39:32]} pixel_bus_in5
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[31:24]} pixel_bus_in4
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[23:16]} pixel_bus_in3
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[15:8]} pixel_bus_in2
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/pixel_bus_in[7:0]} pixel_bus_in1
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[71:64]} weight_bus9
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[63:56]} weight_bus8
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[55:48]} weight_bus7
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[47:40]} weight_bus6
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[39:32]} weight_bus5
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[31:24]} weight_bus4
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[23:16]} weight_bus3
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[15:8]} weight_bus2
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[15]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[7:0]} weight_bus1
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[71:64]} weight_bus9
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[63:56]} weight_bus8
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[55:48]} weight_bus7
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[47:40]} weight_bus6
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[39:32]} weight_bus5
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[31:24]} weight_bus4
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[23:16]} weight_bus3
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[15:8]} weight_bus2
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[63]/u_conv_elem/weight_bus[7:0]} weight_bus1
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[71:64]} weight_bus9
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[63:56]} weight_bus8
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[55:48]} weight_bus7
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[47:40]} weight_bus6
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[39:32]} weight_bus5
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[31:24]} weight_bus4
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[23:16]} weight_bus3
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[15:8]} weight_bus2
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/weight_bus[7:0]} weight_bus1
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[71:64]} pixel_bus_in9
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[63:56]} pixel_bus_in8
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[55:48]} pixel_bus_in7
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[47:40]} pixel_bus_in6
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[39:32]} pixel_bus_in5
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[31:24]} pixel_bus_in4
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[23:16]} pixel_bus_in3
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[15:8]} pixel_bus_in2
quietly virtual signal -install {/tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem} { /tb_top/u_conv_top/u_conv_layer_l2/genblk1[0]/u_stream_conv_out_ch/genblk2[0]/u_conv_elem/pixel_bus_in[7:0]} pixel_bus_in1
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[431:405]} data_in16
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[404:378]} data_in15
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[377:351]} data_in14
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[350:324]} data_in13
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[323:297]} data_in12
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[296:270]} data_in11
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[269:243]} data_in10
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[242:216]} data_in9
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[215:189]} data_in8
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[188:162]} data_in7
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[161:135]} data_in6
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[134:108]} data_in5
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[107:81]} data_in4
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[80:54]} data_in3
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[53:27]} data_in2
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_in[26:0]} data_in1
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[431:405]} data_out16
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[404:378]} data_out15
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[377:351]} data_out14
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[350:324]} data_out13
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[323:297]} data_out12
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[296:270]} data_out11
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[269:243]} data_out10
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[242:216]} data_out9
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[215:189]} data_out8
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[188:162]} data_out7
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[161:135]} data_out6
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[134:108]} data_out5
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[107:81]} data_out4
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[80:54]} data_out3
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[53:27]} data_out2
quietly virtual signal -install /tb_top/u_conv_top/u_max_pool_layer_l2 { /tb_top/u_conv_top/u_max_pool_layer_l2/data_out[26:0]} data_out1
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/dina[107:81]} dina4
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/dina[80:54]} dina3
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/dina[53:27]} dina2
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/dina[26:0]} dina1
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/douta[107:81]} douta4
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/douta[80:54]} douta3
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/douta[53:27]} douta2
quietly virtual signal -install {/tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache} { /tb_top/u_conv_top/u_max_pool_layer_l2/genblk1[0]/genblk1/u_vpool_cache/douta[26:0]} douta1
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[431:405]} pixel_in16
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[404:378]} pixel_in15
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[377:351]} pixel_in14
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[350:324]} pixel_in13
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[323:297]} pixel_in12
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[296:270]} pixel_in11
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[269:243]} pixel_in10
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[242:216]} pixel_in9
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[215:189]} pixel_in8
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[188:162]} pixel_in7
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[161:135]} pixel_in6
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[134:108]} pixel_in5
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[107:81]} pixel_in4
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[80:54]} pixel_in3
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[53:27]} pixel_in2
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in[26:0]} pixel_in1
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[127:120]} pixel_out16
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[119:112]} pixel_out15
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[111:104]} pixel_out14
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[103:96]} pixel_out13
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[95:88]} pixel_out12
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[87:80]} pixel_out11
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[79:72]} pixel_out10
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[71:64]} pixel_out9
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[63:56]} pixel_out8
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[55:48]} pixel_out7
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[47:40]} pixel_out6
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[39:32]} pixel_out5
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[31:24]} pixel_out4
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[23:16]} pixel_out3
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[15:8]} pixel_out2
quietly virtual signal -install /tb_top/u_conv_top/u_quant_dequant_layer_l2 { /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out[7:0]} pixel_out1
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group IOinterface /tb_top/u_conv_top/IO/clk
add wave -noupdate -expand -group IOinterface /tb_top/u_conv_top/IO/reset
add wave -noupdate -expand -group IOinterface /tb_top/u_conv_top/IO/pixel_in
add wave -noupdate -expand -group IOinterface /tb_top/u_conv_top/IO/pixel_valid_in
add wave -noupdate -expand -group IOinterface /tb_top/u_conv_top/IO/pixel_out
add wave -noupdate -expand -group IOinterface /tb_top/u_conv_top/IO/pixel_valid_out
add wave -noupdate -expand -group IOinterface /tb_top/u_conv_top/IO/counter
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/clk
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/reset
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_in
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_valid_in
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_out
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_valid_out
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/window_out
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/window_valid_out
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/window_valid_prev_out
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/window_buff_valid_out
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/col_count
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/row_count
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/row_count_delayed
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_addr
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_addr_delayed
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_rd_addr
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_wrt_addr
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_reg
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_valid_reg
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_din
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_din_rearranged
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_dout
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_wrt_en
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/window_extractor_out
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/window_extractor_out_valid
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/i
add wave -noupdate -expand -group Line_Buffer /tb_top/u_conv_top/u_line_buff/line_out_valid
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/clk
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/reset
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/neigh_idx_bus_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/no_op_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_valid_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_is_valid_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_valid_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_odd_row_valid_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/dout_valid
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/is_out_row_odd
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in64
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in63
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in62
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in61
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in60
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in59
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in58
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in57
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in56
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in55
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in54
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in53
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in52
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in51
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in50
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in49
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in48
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in47
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in46
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in45
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in44
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in43
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in42
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in41
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in40
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in39
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in38
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in37
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in36
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in35
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in34
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in33
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in32
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in31
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in30
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in29
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in28
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in27
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in26
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in25
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in24
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in23
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in22
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in21
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in20
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in19
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in18
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in17
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in16
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in15
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in14
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in13
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in12
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in11
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in10
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in9
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in8
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in7
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in6
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in5
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in4
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in3
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in2
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in1
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out16
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out15
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out14
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out13
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out12
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out11
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out10
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out9
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out8
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out7
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out6
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out5
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out4
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out3
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out2
add wave -noupdate -expand -group Convolution_Layer -radix decimal /tb_top/u_conv_top/u_conv_layer_l2/data_out1
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5211820 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 253
configure wave -valuecolwidth 79
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {9709568 ps}
