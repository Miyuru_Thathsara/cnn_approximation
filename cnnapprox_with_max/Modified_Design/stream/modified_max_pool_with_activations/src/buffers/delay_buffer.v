
`timescale 1ns / 1ps

module delay_buffer
    (
        clk,
        reset,

        //write port
        // window_in,
        // window_valid_in,
        pixel_in,
        pixel_valid_in,

        //read port
        // window_out,
        // window_valid_out
        pixel_out,
        pixel_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
  `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   DATA_WIDTH      = 8;
    parameter                                                   CONV_KERNEL_DIM = 5;
    parameter                                                   DELAY_STAGES    = 5;
    parameter                                                   NUM_IFM         = 20;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  LINE_DATA_WIDTH       = DATA_WIDTH * CONV_KERNEL_DIM * NUM_IFM;
    localparam                                                  WINDOW_DATA_WIDTH     = DATA_WIDTH * CONV_KERNEL_DIM  * CONV_KERNEL_DIM;
    localparam                                                  WINDOW_BUS_DATA_WIDTH = WINDOW_DATA_WIDTH * NUM_IFM; 
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    //input port
//    input       [LINE_DATA_WIDTH-1 : 0]                         line_in;
//    input                                                       line_valid_in;
    // input       [WINDOW_BUS_DATA_WIDTH-1 : 0]                   window_in;
    // input                                                       window_valid_in;
    input          [DATA_WIDTH * NUM_IFM-1 : 0]                 pixel_in;
    input                                                       pixel_valid_in;
    
    //output port
    // output      [WINDOW_BUS_DATA_WIDTH-1 : 0]                   window_out;
    // output                                                      window_valid_out;
    output         [DATA_WIDTH * NUM_IFM-1 : 0]                 pixel_out;
    output                                                      pixel_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire                                                        rd_en;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    shift_reg
    #(
        .CLOCK_CYCLES   (DELAY_STAGES),
        .DATA_WIDTH     (1)
    )   
    u_shift_reg
    (   
        .clk            (clk),

        .enable         (1'b1),
        .data_in        (pixel_valid_in),
        .data_out       (rd_en)
    );
    
    genvar i;
    generate
        
            // window_delay_buff
            // u_window_delay_buff
            // (
            //     .clk            (clk),                 
            //     .srst           (reset),               

            //     .din            (window_in      [i * WINDOW_DATA_WIDTH +: WINDOW_DATA_WIDTH]),                 
            //     .wr_en          (window_valid_in),             
                
            //     .rd_en          (rd_en),             
            //     .dout           (window_out     [i * WINDOW_DATA_WIDTH +: WINDOW_DATA_WIDTH]),               

            //     .full           (),               
            //     .empty          (),             
                
            //     .wr_rst_busy    (), 
            //     .rd_rst_busy    ()  
            // );
        if(NUM_IFM == 1) begin
            pixel_delay_buff_1
            u_pixel_delay_buff_1
            (
                .clk            (clk),                 
                .srst           (reset),               

                .din            (pixel_in),                 
                .wr_en          (pixel_valid_in),             
                
                .rd_en          (rd_en),             
                .dout           (pixel_out),               

                .full           (),               
                .empty          (),             
                
                .wr_rst_busy    (), 
                .rd_rst_busy    ()  
            );
        end
        else if(NUM_IFM == 20) begin
            // for(i=0; i<NUM_IFM; i=i+1) begin
            pixel_delay_buff_20
            u_pixel_delay_buff_20
            (
                .clk            (clk),                 
                .srst           (reset),               

                .din            (pixel_in),                 
                .wr_en          (pixel_valid_in),             
                
                .rd_en          (rd_en),             
                .dout           (pixel_out),                 

                .full           (),               
                .empty          (),             
                
                .wr_rst_busy    (), 
                .rd_rst_busy    ()  
            );
            // end 
        end
    endgenerate

    // assign window_valid_out = rd_en;
    assign pixel_valid_out = rd_en;
        
endmodule  