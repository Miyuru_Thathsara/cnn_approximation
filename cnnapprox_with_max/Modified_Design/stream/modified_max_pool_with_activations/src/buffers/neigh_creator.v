`timescale 1ns / 1ps

module neigh_creator
    (
        clk,
        reset,

        im_cols_in,
        cfg_valid_in,

        line_in,
        line_valid_in,

        neigh_out,
        neigh_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   DATA_WIDTH               = 8;
    parameter                                                   CONV_KERNEL_DIM          = 5;
    parameter                                                   IM_COLS                  = 28;
    parameter                                                   DIM_PREDEFINED           = 1;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  PRE_CONV_KERNEL_DIM      = CONV_KERNEL_DIM + 1;
    localparam                                                  COUNTER_WIDTH            = count2width(IM_COLS);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [7:0]                                           im_cols_in;
    input                                                       cfg_valid_in;

    input       [DATA_WIDTH * PRE_CONV_KERNEL_DIM-1 : 0]        line_in;
    input                                                       line_valid_in;

    output      [PRE_CONV_KERNEL_DIM * PRE_CONV_KERNEL_DIM * DATA_WIDTH -1 : 0]
                                                                neigh_out;
    output reg                                                  neigh_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg         [7:0]                                           im_col_count;
    reg         [PRE_CONV_KERNEL_DIM * PRE_CONV_KERNEL_DIM * DATA_WIDTH -1 : 0]
                                                                stiched_neighborhood;
    reg         [COUNTER_WIDTH-1 : 0]                           column_counter;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    
    always@(posedge clk) begin
        if(reset) begin
            im_col_count            <= 8'b0;
            neigh_valid_out         <= 1'b0;
            column_counter          <= {COUNTER_WIDTH{1'b0}};
        end
        else begin
            if(cfg_valid_in) begin
                im_col_count        <= im_cols_in;
            end

            neigh_valid_out         <= 1'b0;
            if(line_valid_in) begin
                if(column_counter >= PRE_CONV_KERNEL_DIM-1) begin
                    neigh_valid_out <= line_valid_in;
                end
                
                if(DIM_PREDEFINED) begin
                    if(column_counter == IM_COLS-1) begin
                        column_counter  <= {COUNTER_WIDTH{1'b0}};
                    end
                    else begin
                        column_counter  <= column_counter + 1'b1;
                    end
                end
                else begin
                    if(column_counter == im_col_count-1) begin
                        column_counter  <= {COUNTER_WIDTH{1'b0}};
                    end
                    else begin
                        column_counter  <= column_counter + 1'b1;
                    end
                end
            end
        end
    end

    always @(posedge clk) begin
        // if(reset) begin
        //     stiched_neighborhood    <= {(PRE_CONV_KERNEL_DIM * PRE_CONV_KERNEL_DIM * DATA_WIDTH){1'b0}};
        // end
        // else begin
        //     //shift oldest column out and newest column in
            if(line_valid_in) begin
                stiched_neighborhood    <= {line_in, stiched_neighborhood[ (PRE_CONV_KERNEL_DIM * DATA_WIDTH) +: ( (PRE_CONV_KERNEL_DIM-1) * PRE_CONV_KERNEL_DIM * DATA_WIDTH)]}; 
            end
        // end
    end

    assign neigh_out        = stiched_neighborhood;

endmodule