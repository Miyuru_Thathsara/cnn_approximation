`timescale 1ns / 1ps

module conv_layer
    (
        clk,
        reset,

        pixel_bus_in,
        neigh_idx_bus_in,
        no_op_in,
        pixel_valid_in,

        data_out,
        data_is_valid_out,
        data_valid_out,
        data_odd_row_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "params/layer_params.v"
   `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
//    parameter                                                   DATA_WIDTH          = 8;
    parameter                                                   WEIGHT_WIDTH        = 8;
    parameter                                                   BIT_SHIFT_MODE      = 0 ;
    parameter                                                   CONV_KERNEL_DIM     = 5; 
    parameter                                                   LAYER_IDX           = 1;
    parameter                                                   OFM_COUNT           = 20;
    parameter                                                   IFM_COUNT           = 1;
    parameter                                                   USE_DSP             = 0;
    parameter                                                   WINDOW_SEL_MODE     = 0;
    parameter                                                   MAX_POOL_NEIGH_DIM_WIDTH = 2;
    parameter                                                   PIPELINE_DELAY_STAGES   = (BIT_SHIFT_MODE) ? 2 : 3;
    parameter                                                   CONV_IM_DIM             = 24;
    parameter                                                   NO_OP_CMD_WIDTH         = 2;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  PRE_CONV_KERNEL_DIM     = (CONV_KERNEL_DIM+1);
    localparam                                                  PRE_CONV_KERNEL_NEIGH_DIM = PRE_CONV_KERNEL_DIM * PRE_CONV_KERNEL_DIM;
    localparam                                                  ACCUM_DATA_WIDTH        = (BIT_SHIFT_MODE) ? APPROX_SUM_WIDTH : SUM_WIDTH;
    localparam                                                  CONV_KERNEL_NEIGH_DIM   = CONV_KERNEL_DIM * CONV_KERNEL_DIM;
    localparam                                                  CONV_KERNEL_DATA_WIDTH  = CONV_KERNEL_NEIGH_DIM * DATA_WIDTH;
    localparam                                                  PRE_CONV_KERNEL_DATA_WIDTH = PRE_CONV_KERNEL_NEIGH_DIM * DATA_WIDTH;
    localparam                                                  CONV_IFM_BUS_DATA_WIDTH = (WINDOW_SEL_MODE) ?   PRE_CONV_KERNEL_DATA_WIDTH * IFM_COUNT:
                                                                                                                CONV_KERNEL_DATA_WIDTH * IFM_COUNT;
    localparam                                                  MULT_RES_DATA_WIDTH     = DATA_WIDTH + WEIGHT_WIDTH;
    localparam                                                  CONV_OUT_DATA_WIDTH     = MULT_RES_DATA_WIDTH + count2width(CONV_KERNEL_NEIGH_DIM);
    localparam                                                  CONV_OFM_OUT_DATA_WIDTH = CONV_OUT_DATA_WIDTH + count2width(IFM_COUNT);

    localparam                                                  CONV_RESULT_BUS_DATA_WIDTH = CONV_OUT_DATA_WIDTH * IFM_COUNT; 
    // localparam                                                  CONV_LAYER_OUT_DATA_WIDTH  = CONV_RESULT_BUS_DATA_WIDTH * OFM_COUNT;
    localparam                                                  CONV_LAYER_OUT_DATA_WIDTH  = ACCUM_DATA_WIDTH * OFM_COUNT;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [CONV_IFM_BUS_DATA_WIDTH-1 : 0]                 pixel_bus_in;
    input       [MAX_POOL_NEIGH_DIM_WIDTH * OFM_COUNT-1 : 0]    neigh_idx_bus_in;
    input       [NO_OP_CMD_WIDTH * OFM_COUNT-1 : 0]             no_op_in;
    input                                                       pixel_valid_in;

    output      [CONV_LAYER_OUT_DATA_WIDTH-1 : 0]               data_out;
    output      [OFM_COUNT-1 : 0]                               data_is_valid_out;
    output                                                      data_valid_out;
    output                                                      data_odd_row_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [OFM_COUNT-1 : 0]                               dout_valid;
    wire                                                        is_out_row_odd;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    genvar i;
    generate 
        for(i=0; i<OFM_COUNT; i=i+1) begin
            stream_conv_out_channel
            #(
                // .DATA_WIDTH     (DATA_WIDTH),
                .WEIGHT_WIDTH   (WEIGHT_WIDTH),
                .BIT_SHIFT_MODE (BIT_SHIFT_MODE),
                .CONV_KERNEL_DIM(CONV_KERNEL_DIM),    
                .LAYER_IDX      (LAYER_IDX),  
                .IFM_COUNT      (IFM_COUNT), 
                .USE_DSP        (USE_DSP), 
                .OFM_IDX        (i),
                .WINDOW_SEL_MODE(WINDOW_SEL_MODE),
                .MAX_POOL_NEIGH_DIM_WIDTH   
                                (MAX_POOL_NEIGH_DIM_WIDTH),
                .PIPELINE_DELAY_STAGES
                                (PIPELINE_DELAY_STAGES)
            )
            u_stream_conv_out_ch
            (
                .clk            (clk),
                .reset          (reset),

                .pixel_bus_in   (pixel_bus_in),
                .neigh_idx_in   (neigh_idx_bus_in   [i*MAX_POOL_NEIGH_DIM_WIDTH +: MAX_POOL_NEIGH_DIM_WIDTH]),
                .no_op_in       (no_op_in           [i*NO_OP_CMD_WIDTH +: NO_OP_CMD_WIDTH]),
                .pixel_valid_in (pixel_valid_in),

                .data_out       (data_out           [i*ACCUM_DATA_WIDTH +: ACCUM_DATA_WIDTH]),
                .data_is_valid_out   
                                (data_is_valid_out  [i]),
                .data_valid_out (dout_valid         [i])
            ); 
        end
    endgenerate
    
    

    generate
        if(BIT_SHIFT_MODE) begin
            assign data_valid_out         = dout_valid[0];
            assign data_odd_row_valid_out = 1'b0;
        end
        else begin
            //maintains row/col states of conv layer output
            feature_map_state_handler
            #(
                .CONV_IM_DIM            (CONV_IM_DIM),
                .DIM_PREDEFINED         (1)
            )
            u_feature_map_state_handler
            (
                .clk                    (clk),
                .reset                  (reset),

                .cfg_row_count_in       (),
                .cfg_col_count_in       (),
                .cfg_valid_in           (),

                .valid_in               (dout_valid[0]),

                .row_count_out          (),
                .col_count_out          (),
                .max_pool_neigh_idx_out (),

                .is_row_odd_out         (is_out_row_odd),
                .is_col_odd_out         (),
                .row_last_act_out       (),
                .im_last_act_out        ()
            );
            // assign data_valid_out = is_out_row_odd && dout_valid[0];
            // wire data_valid_out_temp;
            // assign data_valid_out_temp = is_out_row_odd && dout_valid[0];
            assign data_valid_out         = dout_valid[0];
            assign data_odd_row_valid_out = is_out_row_odd && dout_valid[0];
        end
    endgenerate

endmodule