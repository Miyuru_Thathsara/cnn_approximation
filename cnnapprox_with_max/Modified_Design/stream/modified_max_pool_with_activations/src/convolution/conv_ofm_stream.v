`timescale 1ns / 1ps

module conv_out_channel_l2
    (
        clk,
        reset,

        pixel_bus_in,
        pixel_valid_in,

        data_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   DATA_WIDTH          = 8;
    parameter                                                   WEIGHT_WIDTH        = 8;
    parameter                                                   CONV_KERNEL_DIM     = 5; 
    parameter                                                   LAYER_IDX           = 0;
    parameter                                                   OFM_IDX             = 0;
    parameter                                                   IFM_COUNT           = 20;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  CONV_KERNEL_NEIGH_DIM   = CONV_KERNEL_DIM * CONV_KERNEL_DIM;
    localparam                                                  CONV_KERNEL_DATA_WIDTH  = CONV_KERNEL_NEIGH_DIM * DATA_WIDTH;
    localparam                                                  CONV_IFM_BUS_DATA_WIDTH = CONV_KERNEL_DATA_WIDTH * IFM_COUNT;
    localparam                                                  MULT_RES_DATA_WIDTH     = DATA_WIDTH + WEIGHT_WIDTH;
    localparam                                                  CONV_OUT_DATA_WIDTH     = MULT_RES_DATA_WIDTH + count2width(CONV_KERNEL_NEIGH_DIM);
    localparam                                                  CONV_OFM_OUT_DATA_WIDTH = CONV_OUT_DATA_WIDTH + count2width(IFM_COUNT);

    localparam                                                  CONV_RESULT_BUS_DATA_WIDTH = CONV_OUT_DATA_WIDTH * IFM_COUNT; 
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [CONV_IFM_BUS_DATA_WIDTH-1 : 0]                 pixel_bus_in;
    input                                                       pixel_valid_in;

    output      [CONV_OFM_OUT_DATA_WIDTH-1 : 0]                 data_out;
    output                                                      data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [CONV_RESULT_BUS_DATA_WIDTH-1 : 0]              conv_dout;
    wire        [IFM_COUNT-1 : 0]                               conv_data_valid;

    wire        [CONV_OFM_OUT_DATA_WIDTH-1 : 0]                 conv_ofm_dout;
    wire                                                        conv_ofm_dout_valid;       
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    genvar i;
    generate 
        for(i=0; i<IFM_COUNT; i=i+1) begin
            conv_elem
            #(
                .DATA_WIDTH         (DATA_WIDTH),
                .WEIGHT_WIDTH       (WEIGHT_WIDTH),
                .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
                .USE_DSP            (USE_DSP),
                .LAYER_IDX          (LAYER_IDX),
                .WEIGHT_INTERNAL    (1),
                .WEIGHT_KERNEL_IDX  ( (OFM_IDX * IFM_COUNT + i) * CONV_KERNEL_NEIGH_DIM)
            )
            u_conv_elem
            (
                .clk                (clk),
                .reset              (reset),

                .pixel_bus_in       (pixel_bus_in   [i*DATA_IN_BUS_WIDTH +: DATA_IN_BUS_WIDTH]),
                .pixel_valid_in     (pixel_valid_in),

                .pixel_out          (conv_dout      [i*CONV_OUT_BUS_WIDTH +: CONV_OUT_BUS_WIDTH]),
                .pixel_valid_out    (conv_data_valid[i])
            );
        end

        if(IFM_COUNT == 20) begin
            assign data_out       = conv_ofm_dout;
            assign data_valid_out = conv_ofm_dout_valid;
            adder_tree_20
            #(
                .DATA_WIDTH     (CONV_OUT_BUS_WIDTH)
            )
            u_adder_tree
            (
                .clk            (clk),
                .reset          (reset),

                .pixel_bus_in   (conv_dout),
                .pixel_valid_in (conv_data_valid[0]),

                .data_out       (conv_ofm_dout),
                .data_valid_out (conv_ofm_dout_valid)
            );
        end
        else begin
            assign data_out       = conv_dout;
            assign data_valid_out = conv_data_valid[0];
        end
    endgenerate

    

endmodule