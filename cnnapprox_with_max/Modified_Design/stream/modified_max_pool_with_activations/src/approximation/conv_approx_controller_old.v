`timescale 1ns / 1ps

module conv_approx_controller
    (
        clk,
        reset,

        //data receive port
        rx_col_in,
        rx_max_pool_idx_in,
        rx_valid_in,

        //data transmit port
        tx_addr_gen_valid_in,
        no_op_out,
        no_op_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                         "params/global_params.v"
    `include                                                        "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       CONV_KERNEL_DIM          = 5; 
    parameter                                                       CONV_IM_DIM              = 24;
    parameter                                                       MAX_POOL_DIM             = 2;
    parameter                                                       OFM_ITER_COUNT           = 1;     
    parameter                                                       DIM_WIDTH                = 8; 
    parameter                                                       DIM_PREDEFINED           = 1;
    parameter                                                       APPROX_MODE              = 0;
    parameter                                                       NUM_OFM                  = 20;
    parameter                                                       LAYER_IDX                = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                      MAX_POOL_NEIGH_DIM       = MAX_POOL_DIM * MAX_POOL_DIM;
    localparam                                                      MAX_POOL_NEIGH_DIM_WIDTH = count2width(MAX_POOL_NEIGH_DIM);
    localparam                                                      MAX_POOL_IM_DIM          = CONV_IM_DIM/2;
    localparam                                                      VPOOL_CACHE_DEPTH        = MAX_POOL_IM_DIM * OFM_ITER_COUNT;
    localparam                                                      PIX_COL_ADDR_WIDTH       = count2width(VPOOL_CACHE_DEPTH) ;//count2width(OFM_ITER_COUNT * CONV_IM_DIM);
    //localparam                                                      PIX_BUFF_ADDR_WIDTH      = PIX_COL_ADDR_WIDTH + 1;
    localparam                                                      PIX_BUFF_ADDR_WIDTH      = PIX_COL_ADDR_WIDTH;
    localparam                                                      OFM_ITER_COUNT_WIDTH     = count2width(OFM_ITER_COUNT);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                           clk;
    input                                                           reset;

    input        [DIM_WIDTH-1 : 0]                                  rx_col_in;
    input        [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]         rx_max_pool_idx_in;
    input                                                           rx_valid_in;

    input                                                           tx_addr_gen_valid_in;
    output       [NUM_OFM-1 : 0]                                    no_op_out;
    output                                                          no_op_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [PIX_BUFF_ADDR_WIDTH-1 : 0]                         rx_ch_addr;
    wire                                                            rx_ch_addr_valid;
    
    wire        [PIX_BUFF_ADDR_WIDTH-1 : 0]                         tx_ch_addr;
    wire        [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                    current_idx;
    wire        [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]          tx_max_pool_idx;
    wire                                                            tx_ch_addr_valid;

    //mem
    wire        [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]          mem_dout_max_pool_idx;

    wire        [NUM_OFM-1 : 0]                                     tx_op_cmd_valid_bus;

    // reg                                                             valid_out_reg;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    //RX channel 

    //address gen rx 
    assign rx_ch_addr       = rx_col_in/2;
    assign rx_ch_addr_valid = rx_valid_in;

    //TX channel
    index_cache_addr_gen_state
    #(
        .CONV_KERNEL_DIM            (CONV_KERNEL_DIM),
        .CONV_IM_DIM                (CONV_IM_DIM),
        .MAX_POOL_DIM               (MAX_POOL_DIM),
        .OFM_ITER_COUNT             (1),
        .DIM_WIDTH                  (8),
        .DIM_PREDEFINED             (1),
        .APPROX_MODE                (1),
        .NUM_OFM                    (NUM_OFM),
        .LAYER_IDX                  (LAYER_IDX)
    ) 
    u_tx_channel_index_cache_addr_gen
    ( 
        .clk                        (clk),
        .reset                      (reset),

        .data_valid_in              (tx_addr_gen_valid_in),
        //rx signals
        .rx_col_count_in            (rx_col_in),
        .rx_max_pool_idx_in         (rx_max_pool_idx_in),
        .rx_cache_wrt_en_in         (rx_valid_in),

        //from mem
        .mem_max_pool_idx_in        (mem_dout_max_pool_idx),
      
        //tx signals
        .pix_buff_addr_out          (tx_ch_addr),
        .current_idx_out            (current_idx),
        .max_pool_idx_out           (tx_max_pool_idx),
        // .valid_out                  (tx_ch_addr_valid)
        .valid_out                  (no_op_valid_out)
    );
      
    generate 
        if(NUM_OFM == 20) begin
            max_pool_idx_mem_l1
            u_max_pool_idx_mem_l1
            ( 
                .clka       (clk),
                .ena        (1'b1),      
                .wea        (rx_ch_addr_valid),
                .addra      (tx_ch_addr),  
                .dina       (rx_max_pool_idx_in),  
                .douta      (mem_dout_max_pool_idx)
            );
        end 
        else if(NUM_OFM == 5) begin
            max_pool_idx_mem_l2
            u_max_pool_idx_mem_l2
            (
                .clka       (clk),                  
                .ena        (1'b1),                 
                .wea        (rx_ch_addr_valid),     
                .addra      (tx_ch_addr),  
                .dina       (rx_max_pool_idx_in),
                .douta      (mem_dout_max_pool_idx)
            );
        end
    endgenerate

    genvar i;
    generate
        // always@(posedge clk) begin
        //     if(reset) begin
        //         valid_out_reg   <= 1'b0;
        //     end
        //     else begin
        //         valid_out_reg   <= tx_addr_gen_valid_in;
        //     end
        // end
        for(i=0; i<NUM_OFM; i=i+1) begin
            index_comparator
            u_index_comparator
            (
                //data receive port
                .current_idx_in         (current_idx),
                .max_idx_in             (tx_max_pool_idx    [i*MAX_POOL_NEIGH_DIM_WIDTH +: MAX_POOL_NEIGH_DIM_WIDTH]),
                .valid_in               (no_op_valid_out), //todo : change after validating synchronization

                //data transmit port
                .no_op_out              (no_op_out      [i]),
                .valid_out              (tx_op_cmd_valid_bus[i])
            );
        end
    endgenerate

    // assign no_op_valid_out = tx_op_cmd_valid_bus[0];

endmodule
