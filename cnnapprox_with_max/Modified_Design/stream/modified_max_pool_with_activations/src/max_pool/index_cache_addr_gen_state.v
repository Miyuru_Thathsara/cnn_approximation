`timescale 1ns / 1ps

module index_cache_addr_gen_state
    (
        clk,
        reset,

        //MAX pool interface
        rx_max_pool_idx_in,
        rx_max_pool_idx_valid_in,
        
        //line buffer interface
        window_valid_prev_in,
        
        //index cache interface
        max_idx_cache_addr_out,
        max_idx_cache_data_in,
        

        current_idx_out,
        max_pool_idx_out,
        valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                         "params/global_params.v"
    `include                                                        "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       CONV_KERNEL_DIM          = 5; 
    parameter                                                       CONV_IM_DIM              = 24;
    parameter                                                       MAX_POOL_DIM             = 2;
    parameter                                                       OFM_ITER_COUNT           = 1;     
    parameter                                                       DIM_WIDTH                = 8; 
    parameter                                                       DIM_PREDEFINED           = 1;
    parameter                                                       APPROX_MODE              = 0;
    parameter                                                       NUM_OFM                  = 20;
    parameter                                                       LAYER_IDX                = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                      MAX_POOL_NEIGH_DIM       = MAX_POOL_DIM * MAX_POOL_DIM;
    localparam                                                      MAX_POOL_NEIGH_DIM_WIDTH = count2width(MAX_POOL_NEIGH_DIM);
    localparam                                                      MAX_POOL_IM_DIM          = CONV_IM_DIM/2;
    localparam                                                      VPOOL_CACHE_DEPTH        = MAX_POOL_IM_DIM * OFM_ITER_COUNT;
    localparam                                                      PIX_COL_ADDR_WIDTH       = count2width(VPOOL_CACHE_DEPTH) ;//count2width(OFM_ITER_COUNT * CONV_IM_DIM);
    // localparam                                                      PIX_BUFF_ADDR_WIDTH      = PIX_COL_ADDR_WIDTH + 1;
    localparam                                                      PIX_BUFF_ADDR_WIDTH      = PIX_COL_ADDR_WIDTH;
    localparam                                                      OFM_ITER_COUNT_WIDTH     = count2width(OFM_ITER_COUNT);

    localparam                                                      INPUT_GAP                = MAX_POOL_DIM ** LAYER_IDX;
    localparam                                                      INTER_ROW_GAP              = 
    
    localparam                                                      STATE_ROW_1_1              = 0;
    localparam                                                      STATE_ROW_1_2              = 1;
    localparam                                                      STATE_INTER_ROW_GAP        = 2;
    localparam                                                      STATE_ROW_2_1              = 3;
    localparam                                                      STATE_ROW_2_2              = 4;
    localparam                                                      STATE_ROW_END              = 5;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                           clk;
    input                                                           reset;

    input        [DIM_WIDTH-1 : 0]                                  cfg_row_count_in;
    input        [DIM_WIDTH-1 : 0]                                  cfg_col_count_in;
    input                                                           cfg_valid_in;

    input                                                           window_valid_prev_in;
    input                                                           start_in;
    input        [DIM_WIDTH-1 : 0]                                  rx_col_count_in;
    input        [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH -1 : 0]        rx_max_pool_idx_in;
    input                                                           rx_max_pool_idx_valid_in;

    input        [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH -1 : 0]        max_idx_cache_data_in;

    
    output reg   [PIX_BUFF_ADDR_WIDTH-1 : 0]                        max_idx_cache_addr_out;
    output reg   [MAX_POOL_NEIGH_DIM_WIDTH- 1: 0]                   current_idx_out;
    output reg   [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH -1 : 0]        max_pool_idx_out;
    output reg                                                      valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg   [DIM_WIDTH-1 : 0]                                  col_count_out;
    reg   [DIM_WIDTH-1 : 0]                                  row_count_out;
    
    reg          [DIM_WIDTH-1 : 0]                                  cfg_col_count;
    reg          [DIM_WIDTH-1 : 0]                                  cfg_row_count;
    reg          [DIM_WIDTH-1 : 0]                                  wait_counter;

    reg          [OFM_ITER_COUNT_WIDTH-1 : 0]                       ofm_iter_count;
    reg          [DIM_WIDTH-1 : 0]                                  col_count;
    reg          [DIM_WIDTH-1 : 0]                                  row_count;
    wire         [MAX_POOL_NEIGH_DIM_WIDTH- 1: 0]                   current_idx;   
    reg          [MAX_POOL_NEIGH_DIM_WIDTH- 1: 0]                   current_idx_reg;

    wire         [PIX_COL_ADDR_WIDTH-1 : 0]                         pix_column_addr;
    wire                                                            buff_select;

    reg                                                             valid_out_reg;
    reg                                                             valid_out_reg_b;

    integer                                                         state;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    assign current_idx = (col_count_out%MAX_POOL_DIM) * MAX_POOL_DIM + (row_count_out % MAX_POOL_DIM);

    always@(posedge clk) begin : pix_count_blk
        if(reset) begin
            ofm_iter_count          <= {OFM_ITER_COUNT_WIDTH{1'b0}};
            current_idx_reg         <= {MAX_POOL_NEIGH_DIM_WIDTH{1'b0}};
            col_count               <= {DIM_WIDTH{1'b0}};
            row_count               <= {DIM_WIDTH{1'b0}};
            col_count_out           <= {DIM_WIDTH{1'b0}};
            row_count_out           <= {DIM_WIDTH{1'b0}};
            wait_counter            <= {DIM_WIDTH{1'b0}};
            // max_pool_idx_out        <= {(NUM_OFM * MAX_POOL_NEIGH_DIM){1'b0}};
            valid_out_reg           <= 1'b0;
            valid_out_reg_b         <= 1'b0;

            state                   <= STATE_ROW_1_1;
        end
        else begin
            valid_out_reg                       <= window_valid_prev_in;
            current_idx_reg                     <= current_idx;
            case(state)
                STATE_ROW_1_1 : begin
                    if(rx_max_pool_idx_valid_in) begin
                        // max_pool_idx_out <= rx_max_pool_idx_in;
                        col_count_out           <= col_count_out + 1'b1;
                        state                   <= STATE_ROW_1_2;
                    end
                end
                STATE_ROW_1_2 : begin
                    if(wait_counter == INPUT_GAP-1) begin
                        wait_counter             <= {DIM_WIDTH{1'b0}};
                        if(col_count_out % MAX_POOL_DIM == MAX_POOL_DIM-1) begin
                            if(col_count_out == CONV_IM_DIM-1) begin
                                col_count_out    <= {DIM_WIDTH{1'b0}};

                                if(row_count_out == CONV_IM_DIM-1) begin
                                    row_count_out<= {DIM_WIDTH{1'b0}};
                                end
                                else begin
                                    row_count_out<= row_count_out + 1'b1;
                                end

                                state            <= STATE_INTER_ROW_GAP;
                            end
                            else begin
                                col_count_out    <= col_count_out + 1'b1;
                                state            <= STATE_ROW_1_1;
                            end
                        end
                    end
                    else begin
                        wait_counter             <= wait_counter + 1'b1;
                    end
                end
                STATE_INTER_ROW_GAP : begin
                    if(wait_counter == 7-1) begin
                        wait_counter             <= {DIM_WIDTH{1'b0}};
                        valid_out_reg_b          <= 1'b1;
                        state                    <= STATE_ROW_2_1;
                    end
                    else begin
                        wait_counter             <= wait_counter + 1'b1;
                    end
                end
                STATE_ROW_2_1 : begin
                    if(wait_counter == INPUT_GAP-1) begin
                        valid_out_reg_b          <= 1'b1;
                        wait_counter             <= {DIM_WIDTH{1'b0}};
                        col_count_out            <= col_count_out + 1'b1;
                        state                    <= STATE_ROW_2_2;
                    end
                    else begin
                        valid_out_reg_b          <= 1'b0;
                        wait_counter             <= wait_counter + 1'b1;
                    end    
                end
                STATE_ROW_2_2 : begin
                    if(wait_counter == INPUT_GAP-1) begin
                        wait_counter             <= {DIM_WIDTH{1'b0}};
                        if(col_count_out % MAX_POOL_DIM == MAX_POOL_DIM-1) begin
                            if(col_count_out == CONV_IM_DIM-1) begin
                                col_count_out    <= {DIM_WIDTH{1'b0}};

                                if(row_count_out == CONV_IM_DIM-1) begin
                                    row_count_out<= {DIM_WIDTH{1'b0}};
                                end
                                else begin
                                    row_count_out<= row_count_out + 1'b1;
                                end
                                state            <= STATE_ROW_END;
                            end
                            else begin
                                col_count_out    <= col_count_out + 1'b1;
                                state            <= STATE_ROW_2_1;
                            end
                        end
                    end
                    else begin
                        wait_counter             <= wait_counter + 1'b1;
                    end    
                end
                STATE_ROW_END : begin
                    valid_out_reg_b              <= 1'b0;
                    state                        <= STATE_ROW_1_1;
                end
            endcase
        end
    end

    always@(*) begin
        case(state)
            // STATE_ROW_1_1 : begin
            //     max_pool_idx_out  = rx_max_pool_idx_in;
            //     max_idx_cache_addr_out = rx_col_count_in/2;
            // end
            STATE_ROW_1_1: begin
                max_pool_idx_out  = rx_max_pool_idx_in;
                max_idx_cache_addr_out = rx_col_count_in/2;
                current_idx_out   = current_idx;
                valid_out         = valid_out_reg;
            end
            STATE_ROW_1_2 : begin
                max_pool_idx_out  = rx_max_pool_idx_in;
                max_idx_cache_addr_out = rx_col_count_in/2;
                current_idx_out   = current_idx;
                // if(wait_counter == INPUT_GAP-1) begin
                    valid_out     = valid_out_reg;
                // end
                // else begin
                //     valid_out     = 1'b0;
                // end
            end
            // STATE_ROW_2_1 : begin
            //     max_pool_idx_out  = mem_dout_max_pool_idx;
            //     max_idx_cache_addr_out = col_count_out/2;
            // end
            STATE_ROW_2_1:  begin
                max_pool_idx_out  = max_idx_cache_data_in;
                max_idx_cache_addr_out = col_count_out/2;
                current_idx_out   = current_idx_reg;
                if(wait_counter == INPUT_GAP-1) begin
                    valid_out     = valid_out_reg_b;
                end 
                else begin
                    valid_out     = 1'b0;
                end
            end
            STATE_ROW_2_2 : begin
                max_pool_idx_out  = max_idx_cache_data_in;
                max_idx_cache_addr_out = col_count_out/2;
                current_idx_out   = current_idx_reg;
                if(wait_counter == INPUT_GAP-1) begin
                    valid_out     = valid_out_reg_b;
                end 
                else begin
                    valid_out     = 1'b0;
                end
            end 
            STATE_ROW_END : begin
                max_pool_idx_out  = max_idx_cache_data_in;
                max_idx_cache_addr_out = col_count_out/2;
                current_idx_out   = current_idx_reg;
                if(wait_counter == INPUT_GAP-1) begin
                    valid_out     = valid_out_reg_b;
                end
                else begin
                    valid_out     = 1'b0;
                end
            end
            default : begin
                max_pool_idx_out  = {(NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH){1'b0}}; 
                max_idx_cache_addr_out = {PIX_BUFF_ADDR_WIDTH{1'b0}};
                current_idx_out   = {MAX_POOL_NEIGH_DIM_WIDTH{1'b0}};
                valid_out         = 1'b0;
            end
        endcase
    end

endmodule
