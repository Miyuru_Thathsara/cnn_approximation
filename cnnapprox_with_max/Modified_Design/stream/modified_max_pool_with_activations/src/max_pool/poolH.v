`timescale 1ns / 1ps

module poolH
    (
        clk,
        reset,

        col_count_in,
        row_count_in,
        ofm_iter_count_in,

        data_in,
        data_idx_in,
        data_valid_in,

        data_out,
        data_idx_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                     "params/global_params.v"
    `include                                                     "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   POOL_DATA_WIDTH         = 8;
    parameter                                                   MAX_POOL_DIM            = 2;
    parameter                                                   MAX_POOL_NEIGH_DIM      = MAX_POOL_DIM * MAX_POOL_DIM;
    parameter                                                   OFM_ITER_COUNT          = 1;
    parameter                                                   DIM_WIDTH               = 8;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  MAX_POOL_NEIGH_DIM_WIDTH= count2width(MAX_POOL_NEIGH_DIM);
    localparam                                                  OFM_ITER_COUNT_WIDTH    = count2width(OFM_ITER_COUNT);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;
                         
    input               [DIM_WIDTH-1 : 0]                       col_count_in;
    input               [DIM_WIDTH-1 : 0]                       row_count_in;
    input               [OFM_ITER_COUNT_WIDTH-1 : 0]            ofm_iter_count_in;
                     
    input signed        [POOL_DATA_WIDTH-1 : 0]                 data_in;
    input               [MAX_POOL_NEIGH_DIM_WIDTH-1:0]          data_idx_in;
    input                                                       data_valid_in;
                     
    output reg signed   [POOL_DATA_WIDTH-1 : 0]                 data_out;
    output reg          [MAX_POOL_NEIGH_DIM_WIDTH-1:0]          data_idx_out;
    output reg                                                  data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg signed         [POOL_DATA_WIDTH-1 : 0]                  pix_buff [OFM_ITER_COUNT-1 : 0];
    reg                [MAX_POOL_NEIGH_DIM_WIDTH-1:0]           idx_reg  [OFM_ITER_COUNT-1 : 0];
    
    wire               [POOL_DATA_WIDTH-1 : 0]                  h_max;
    wire               [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]         h_max_idx;
    
    integer                                                     i;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    always@(posedge clk) begin
        if(reset) begin
            for(i=0; i<OFM_ITER_COUNT; i=i+1) begin
                pix_buff [i]                    <= {(POOL_DATA_WIDTH){1'b0}};
                idx_reg  [i]                    <= {(MAX_POOL_NEIGH_DIM_WIDTH){1'b0}};
            end
            data_out                            <= {POOL_DATA_WIDTH{1'b0}};
            data_idx_out                        <= {MAX_POOL_NEIGH_DIM_WIDTH{1'b0}};
            data_valid_out                      <= 1'b0;
        end
        else begin
            data_valid_out                      <= 1'b0;
            if(data_valid_in) begin
                if(col_count_in%2 == 0) begin
                    pix_buff[ofm_iter_count_in] <= data_in;
                    idx_reg [ofm_iter_count_in] <= data_idx_in - 2;
                end
                else begin
                    // data_out                    <= h_max;
                    // data_idx_out                <= h_max_idx;
                    if(pix_buff[ofm_iter_count_in] >= data_in) begin
                        data_out                <= pix_buff[ofm_iter_count_in];
                        data_idx_out            <= idx_reg [ofm_iter_count_in];
                    end
                    else begin
                        data_out                <= data_in;
                        data_idx_out            <= data_idx_in;
                    end
                    data_valid_out              <= 1'b1;
                end
            end
        end
    end

    // assign h_max     = (pix_buff[ofm_iter_count_in] > data_in) ? pix_buff[ofm_iter_count_in] : data_in;
    // assign h_max_idx = (pix_buff[ofm_iter_count_in] > data_in) ? idx_reg [ofm_iter_count_in] : data_idx_in;

endmodule