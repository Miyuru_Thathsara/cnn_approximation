-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
-- Date        : Fri Apr 12 20:38:41 2019
-- Host        : hesl-HP-Z420-Workstation running 64-bit Ubuntu 18.04 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/hesl/duvindu/pool_approximator/lenet/modified_pool_index_cache/lenet_maxpool_mod.runs/layer_out_line_buff_50_synth_1/layer_out_line_buff_50_stub.vhdl
-- Design      : layer_out_line_buff_50
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu9p-fsgd2104-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity layer_out_line_buff_50 is
  Port ( 
    clka : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 49 downto 0 );
    addra : in STD_LOGIC_VECTOR ( 1 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 399 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 399 downto 0 )
  );

end layer_out_line_buff_50;

architecture stub of layer_out_line_buff_50 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clka,ena,wea[49:0],addra[1:0],dina[399:0],douta[399:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "blk_mem_gen_v8_4_2,Vivado 2018.3";
begin
end;
