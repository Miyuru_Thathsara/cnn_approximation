onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib vpool_cahce_108_24_opt

do {wave.do}

view wave
view structure
view signals

do {vpool_cahce_108_24.udo}

run -all

quit -force
