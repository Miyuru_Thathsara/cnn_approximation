onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib vpool_cache_27_28_opt

do {wave.do}

view wave
view structure
view signals

do {vpool_cache_27_28.udo}

run -all

quit -force
