onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+pixel_delay_buff_20 -L xil_defaultlib -L xpm -L fifo_generator_v13_2_3 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.pixel_delay_buff_20 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {pixel_delay_buff_20.udo}

run -all

endsim

quit -force
