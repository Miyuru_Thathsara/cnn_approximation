onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib pixel_delay_buff_20_opt

do {wave.do}

view wave
view structure
view signals

do {pixel_delay_buff_20.udo}

run -all

quit -force
