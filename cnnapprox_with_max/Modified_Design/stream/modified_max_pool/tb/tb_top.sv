`timescale 1ns / 1ps

module tb_top;

	`include "image.sv"
	`include "tb_defs.sv" 

	localparam						REG_SIZE	  = 8; //Number of feature maps scattered into 8 feature maps
	localparam                      DATA_WIDTH    = 8;
	localparam						L1_NUM_OFM	  = 64;
	localparam						L2_NUM_OFM	  = 16;
	localparam						INPUT_NUM_FM  = 64;
	localparam                      INPUT_DATA_WIDTH  = DATA_WIDTH * INPUT_NUM_FM;
    localparam                      OUTPUT_DATA_WIDTH = DATA_WIDTH * L2_NUM_OFM;
	localparam						IMAGE_DIM	  = 30;
	localparam						L1_CONV_DIM   = 3;

	localparam						L2_CONV_DIM   = 3;
	localparam						OUT_IMAGE_DIM = 4;

	localparam      				APPROX_LEVEL_COUNT  = 2;    //for lenet, max abs(approx_weight) = 2
	localparam      				SUM_WIDTH           = 27;
	localparam      				APPROX_SUM_WIDTH    = SUM_WIDTH - 8 + APPROX_LEVEL_COUNT;
    
	reg clk;
	reg reset;

	reg  [REG_SIZE*DATA_WIDTH-1 : 0]	data_in;
	reg 							data_valid_in;

	wire [OUTPUT_DATA_WIDTH-1 : 0]	out_data;
	wire 							out_data_valid;

	integer 						out_file;

	integer 						counter;
	integer							counter2;
	integer 						row_counter;
	integer 						col_counter;
	integer							j;


	top
    u_conv_top
    (
        .clk			(clk),
        .reset			(reset),

		.pixel_in		(data_in),
		.pixel_valid	(data_valid_in),

        .data_out 		(out_data),
        .data_valid_out (out_data_valid)
    );

	always #5 clk = ~clk;

	task write_image();
		integer i;

		repeat(IMAGE_DIM) begin
			repeat(IMAGE_DIM) begin
				repeat(INPUT_NUM_FM/REG_SIZE)begin
					@(posedge clk);	
					for (j=0;j<(INPUT_NUM_FM/REG_SIZE);j=j+1)begin    //iterating for 8 cycles, which is the scattered size of image feature maps
						data_in[((INPUT_NUM_FM/REG_SIZE)-1-j)*DATA_WIDTH +: DATA_WIDTH] = pixel_array_data[(counter2+j+1)*(IMAGE_DIM * IMAGE_DIM) - 1 - counter];
					end
					data_valid_in = 1'b1;
					counter2 = counter2 + 8;
				end
				counter2 = 0;
				counter = counter + 1'b1;
				col_counter = col_counter + 1'b1;
			end	
			col_counter = 0;
			row_counter = row_counter + 1'b1;
		end
		data_valid_in = 1'b0;
	endtask

	task read_out();
		repeat(OUT_IMAGE_DIM * OUT_IMAGE_DIM) begin
			while(!out_data_valid) begin
				@(posedge clk);
			end
			$display("Out pixel bus : %x", out_data);
			$fwrite(out_file,"%h\n",out_data);
			@(posedge clk);
		end
	endtask
	
	
	initial begin
		counter = 0;
		counter2 = 0;
		row_counter = 0;
		col_counter = 0;
    	clk	= 1'b0;
    	reset = 1'b1;
		data_in	= {DATA_WIDTH{1'b0}};
		data_valid_in = 1'b0;
		out_file = $fopen("out_file.txt","w");
    	repeat(5) begin
    		@(posedge clk);
    	end

    	reset = 1'b0;

		repeat(20) begin
			@(posedge clk);
		end

		write_image();

		$fclose(out_file);
		repeat(100) begin
			@(posedge clk);
		end 

		
    	$finish;
    end

endmodule
