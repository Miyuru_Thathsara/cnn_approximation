`timescale 1ns / 1ps

module adder_tree_64(
        clk,
        reset,

        data_in,
        data_valid_in,

        data_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------

    parameter                                                         DATA_WIDTH = 11;
    parameter                                                         NO_INPUTS  = 64; 
      
    parameter                                                         OUTPUT_DATA_WIDTH = DATA_WIDTH + count2width(NO_INPUTS);

//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                        DATA_IN_BUS_WIDTH = DATA_WIDTH * NO_INPUTS;
    
    localparam                                                        LVL1_OUTPUTS      = NO_INPUTS/2;
    localparam                                                        LVL2_OUTPUTS      = LVL1_OUTPUTS/2;
    localparam                                                        LVL3_OUTPUTS      = LVL2_OUTPUTS/2;
    localparam                                                        LVL4_OUTPUTS      = LVL3_OUTPUTS/2;
    localparam                                                        LVL5_OUTPUTS      = LVL4_OUTPUTS/2;
    localparam                                                        LVL6_OUTPUTS      = LVL5_OUTPUTS/2;
    
    localparam                                                        LVL1_DATA_WIDTH   = DATA_WIDTH + 1;
    localparam                                                        LVL2_DATA_WIDTH   = LVL1_DATA_WIDTH + 1;
    localparam                                                        LVL3_DATA_WIDTH   = LVL2_DATA_WIDTH + 1;
    localparam                                                        LVL4_DATA_WIDTH   = LVL3_DATA_WIDTH + 1;
    localparam                                                        LVL5_DATA_WIDTH   = LVL4_DATA_WIDTH + 1;
    localparam                                                        LVL6_DATA_WIDTH   = LVL5_DATA_WIDTH + 1;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                             clk;
    input                                                             reset;

    input  signed     [DATA_IN_BUS_WIDTH-1 : 0]                       data_in;
    input                                                             data_valid_in;

    output reg signed [OUTPUT_DATA_WIDTH-1 : 0]                       data_out;
    output reg                                                        data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg signed        [DATA_WIDTH-1 : 0]                              in_data           [NO_INPUTS-1 : 0];
    reg signed        [LVL1_DATA_WIDTH-1 : 0]                         partial_sum_lvl_a [LVL1_OUTPUTS-1 : 0];
    reg signed        [LVL2_DATA_WIDTH-1 : 0]                         partial_sum_lvl_b [LVL2_OUTPUTS-1 : 0];
    reg signed        [LVL3_DATA_WIDTH-1 : 0]                         partial_sum_lvl_c [LVL3_OUTPUTS-1 : 0];
    reg signed        [LVL4_DATA_WIDTH-1 : 0]                         partial_sum_lvl_d [LVL4_OUTPUTS-1 : 0];
    reg signed        [LVL5_DATA_WIDTH-1 : 0]                         partial_sum_lvl_e [LVL5_OUTPUTS-1 : 0];
    reg signed        [LVL6_DATA_WIDTH-1 : 0]                         partial_sum_lvl_f [LVL6_OUTPUTS-1 : 0];
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

integer i1, i2, i3, i4, i5, i6, m;
    always@(*) begin
        for(m=0; m<NO_INPUTS; m=m+1) begin
            in_data[m]           = data_in[m*DATA_WIDTH +: DATA_WIDTH];
        end
        for(i1=0; i1<LVL1_OUTPUTS; i1=i1+1) begin
            partial_sum_lvl_a[i1] = in_data[2*i1] + in_data[((2*i1)+1)];
        end 
        for(i2=0; i2<LVL2_OUTPUTS; i2=i2+1) begin
            partial_sum_lvl_b[i2] = partial_sum_lvl_a[2*i2] + partial_sum_lvl_a[(2*i2+1)];
        end 
        for(i3=0; i3<LVL3_OUTPUTS; i3=i3+1) begin
            partial_sum_lvl_c[i3] = partial_sum_lvl_b[2*i3] + partial_sum_lvl_b[(2*i3+1)];
        end 
        for(i4=0; i4<LVL4_OUTPUTS; i4=i4+1) begin
            partial_sum_lvl_d[i4] = partial_sum_lvl_c[2*i4] + partial_sum_lvl_c[(2*i4+1)];
        end 
        for(i5=0; i5<LVL5_OUTPUTS; i5=i5+1) begin
            partial_sum_lvl_e[i5] = partial_sum_lvl_d[2*i5] + partial_sum_lvl_d[(2*i5+1)];
        end 
        for(i6=0; i6<LVL6_OUTPUTS; i6=i6+1) begin
            partial_sum_lvl_f[i6] = partial_sum_lvl_e[2*i6] + partial_sum_lvl_e[(2*i6+1)];
        end 
    end 

    always@(posedge clk) begin
        if(reset) begin
            data_valid_out  <= 1'b0;
            data_out        <= {OUTPUT_DATA_WIDTH{1'b0}};
        end 
        else begin
            data_valid_out  <= data_valid_in;
            if(data_valid_in) begin
                data_out    <= partial_sum_lvl_f[0];
            end
        end 
    end 
endmodule
