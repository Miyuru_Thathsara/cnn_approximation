`timescale 1ns / 1ps
//(* use_dsp48 = "yes" *) 
module adder_tree_9
    (
        clk,
        reset,

        data_in,
        data_valid_in,

        data_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                         DATA_WIDTH = 8;
    parameter                                                         NO_INPUTS  = 9; 
      
    parameter                                                         OUTPUT_DATA_WIDTH = DATA_WIDTH + count2width(NO_INPUTS);
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                        DATA_IN_BUS_WIDTH = DATA_WIDTH * NO_INPUTS;
    
    localparam                                                        LVL1_OUTPUTS      = NO_INPUTS/2;
    localparam                                                        LVL2_OUTPUTS      = LVL1_OUTPUTS/2;
    localparam                                                        LVL3_OUTPUTS      = LVL2_OUTPUTS/2;
    
    localparam                                                        LVL1_DATA_WIDTH   = DATA_WIDTH + 1;
    localparam                                                        LVL2_DATA_WIDTH   = LVL1_DATA_WIDTH + 1;
    localparam                                                        LVL3_DATA_WIDTH   = LVL2_DATA_WIDTH + 1;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                             clk;
    input                                                             reset;

    input  signed     [DATA_IN_BUS_WIDTH-1 : 0]                       data_in;
    input                                                             data_valid_in;

    output reg signed [OUTPUT_DATA_WIDTH-1 : 0]                       data_out;
    output reg                                                        data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg signed        [DATA_WIDTH-1 : 0]                              in_data           [NO_INPUTS-1 : 0];
    reg signed        [LVL1_DATA_WIDTH-1 : 0]                         partial_sum_lvl_a [LVL1_OUTPUTS-1 : 0];
    reg signed        [LVL2_DATA_WIDTH-1 : 0]                         partial_sum_lvl_b [LVL2_OUTPUTS-1 : 0];
    reg signed        [LVL3_DATA_WIDTH-1 : 0]                         partial_sum_lvl_c [LVL3_OUTPUTS-1 : 0];
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    integer i, j, k, m;
    always@(*) begin
        for(m=0; m<NO_INPUTS; m=m+1) begin
            in_data[m]           = data_in[m*DATA_WIDTH +: DATA_WIDTH];
        end
        for(i=0; i<LVL1_OUTPUTS; i=i+1) begin
            partial_sum_lvl_a[i] = in_data[2*i] + in_data[((2*i)+1)];
        end 
        for(j=0; j<LVL2_OUTPUTS; j=j+1) begin
            partial_sum_lvl_b[j] = partial_sum_lvl_a[2*j] + partial_sum_lvl_a[(2*j+1)];
        end 
        for(k=0; k<LVL3_OUTPUTS; k=k+1) begin
            partial_sum_lvl_c[k] = partial_sum_lvl_b[2*k] + partial_sum_lvl_b[(2*k+1)];
        end 
    end 
    
    wire signed   [LVL3_DATA_WIDTH-1 : 0] last_byte;
    assign last_byte =  { {(LVL3_DATA_WIDTH - DATA_WIDTH){data_in[DATA_IN_BUS_WIDTH-1]}} ,in_data[(NO_INPUTS-1)]};
    always@(posedge clk) begin
        if(reset) begin
            data_valid_out  <= 1'b0;
            data_out        <= {OUTPUT_DATA_WIDTH{1'b0}};
        end 
        else begin
            data_valid_out  <= data_valid_in;
            if(data_valid_in) begin
                data_out    <= partial_sum_lvl_c[0] + last_byte;
            end
        end 
    end 

endmodule