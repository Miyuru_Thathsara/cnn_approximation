`timescale 1ns / 1ps

module poolV
    (
        clk,
        reset,

        row_count_in,
        pix_buff_rd_data_in,
        data_in,
        data_valid_in,

        data_out,
        data_idx_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                     "params/global_params.v"
    `include                                                     "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   POOL_DATA_WIDTH         = 8;
    parameter                                                   IMAGE_WIDTH             = 28;
    parameter                                                   DIM_WIDTH               = 8;
    parameter                                                   MAX_POOL_DIM            = 2;
    parameter                                                   MAX_POOL_NEIGH_DIM      = MAX_POOL_DIM * MAX_POOL_DIM;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  MAX_POOL_NEIGH_DIM_WIDTH= count2width(MAX_POOL_NEIGH_DIM);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input signed      [POOL_DATA_WIDTH-1 : 0]                   data_in;
    input                                                       data_valid_in;
    input signed      [POOL_DATA_WIDTH-1 : 0]                   pix_buff_rd_data_in;  
    input             [DIM_WIDTH-1 : 0]                         row_count_in;

    output reg signed [POOL_DATA_WIDTH-1 : 0]                   data_out;
    output reg        [MAX_POOL_NEIGH_DIM_WIDTH-1:0]            data_idx_out;
    output reg                                                  data_valid_out;
    reg                                                         data_valid_in_1;
    reg                                                         data_valid_in_2;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    // wire              [POOL_DATA_WIDTH-1 : 0]                   v_max;
    // wire              [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]          v_max_idx;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    always@(posedge clk) begin
        data_valid_in_1                    <= data_valid_in;
        if(reset) begin
            data_out                       <= {POOL_DATA_WIDTH{1'b0}};
            data_idx_out                   <= 8'h0;
            data_valid_out                 <= 1'b0;
        end
        else begin
            data_valid_out                 <= 1'b0;
            if(data_valid_in) begin
                if(row_count_in%2 != 0) begin
                    if((pix_buff_rd_data_in >= data_in)) begin
                        data_out           <= pix_buff_rd_data_in;
                        data_idx_out       <= 2;
                    end
                    else begin
                        data_out           <= data_in;
                        data_idx_out       <= 3;
                    end
                    data_valid_out         <= 1'b1;
                end
            end
        end
    end

    // assign v_max     = 
    // assign v_max_idx = (pix_buff_rd_data_in > data_in) ? 2 : 3;
    
endmodule
