`timescale 1ns / 1ps

module neigh_bus_mux
    (
        clk,
        reset,

        neigh_in,
        neigh_idx_in,
        neigh_idx_valid_in,
        

        neigh_out,
        neigh_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
   parameter                                                    CHANNEL_COUNT            = 16;
   parameter                                                    DATA_WIDTH               = 8;
   parameter                                                    CONV_KERNEL_DIM          = 3;
   parameter                                                    MAX_POOL_NEIGH_DIM_WIDTH = 3;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
   localparam                                                   APPROX_KERNEL_DIM        = CONV_KERNEL_DIM + 1; 
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [DATA_WIDTH * APPROX_KERNEL_DIM * APPROX_KERNEL_DIM * CHANNEL_COUNT-1 : 0]   
                                                                neigh_in;
    input       [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                neigh_idx_in;
    input                                                       neigh_idx_valid_in;

    output      [DATA_WIDTH * CONV_KERNEL_DIM * CONV_KERNEL_DIM * CHANNEL_COUNT -1 : 0]
                                                                neigh_out;
    output                                                      neigh_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [CHANNEL_COUNT-1 : 0]                           valid_bus;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    genvar i;
    generate
        for(i=0;i<CHANNEL_COUNT;i=i+1) begin
            neigh_mux
            #(
                .DATA_WIDTH         (DATA_WIDTH),
                .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
                .MAX_POOL_NEIGH_DIM_WIDTH   
                                    (MAX_POOL_NEIGH_DIM_WIDTH)
            )
            u_neigh_mux
            (
                .clk                (clk),
                .reset              (reset),

                .neigh_in           (neigh_in       [i * (DATA_WIDTH * APPROX_KERNEL_DIM * APPROX_KERNEL_DIM) +: (DATA_WIDTH * APPROX_KERNEL_DIM * APPROX_KERNEL_DIM)]),
                .neigh_idx_in       (neigh_idx_in),
                .neigh_idx_valid_in (neigh_idx_valid_in),
                

                .neigh_out          (neigh_out      [i * (DATA_WIDTH * CONV_KERNEL_DIM * CONV_KERNEL_DIM) +: (DATA_WIDTH * CONV_KERNEL_DIM * CONV_KERNEL_DIM)]),
                .neigh_valid_out    (valid_bus      [i])
            );
        end
    endgenerate
    assign neigh_valid_out = valid_bus[0];

endmodule
