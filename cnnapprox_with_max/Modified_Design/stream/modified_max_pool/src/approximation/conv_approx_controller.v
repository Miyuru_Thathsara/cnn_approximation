`timescale 1ns / 1ps

module conv_approx_controller
    (
        clk,
        reset,

        //MAX pool interface
        rx_max_pool_idx_in,
        rx_max_pool_idx_valid_in,
        //line buffer interface
        rx_window_valid_prev_in,

        //TX interface
        no_op_out,
        no_op_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                         "params/global_params.v"
    `include                                                        "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       CONV_KERNEL_DIM          = 5; 
    parameter                                                       CONV_IM_DIM              = 24;
    parameter                                                       MAX_POOL_DIM             = 2;
    parameter                                                       OFM_ITER_COUNT           = 1;     
    parameter                                                       DIM_WIDTH                = 8; 
    parameter                                                       DIM_PREDEFINED           = 1;
    parameter                                                       APPROX_MODE              = 0;
    parameter                                                       NUM_OFM                  = 20;
    parameter                                                       LAYER_IDX                = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                      MAX_POOL_NEIGH_DIM       = MAX_POOL_DIM * MAX_POOL_DIM;
    localparam                                                      MAX_POOL_NEIGH_DIM_WIDTH = count2width(MAX_POOL_NEIGH_DIM);
    localparam                                                      MAX_POOL_IM_DIM          = CONV_IM_DIM/2;
    localparam                                                      INDEX_CACHE_ADDR_WIDTH   = count2width(CONV_IM_DIM/2);
    localparam                                                      OFM_ITER_COUNT_WIDTH     = count2width(OFM_ITER_COUNT);

    localparam                                                      STATE_EVEN_ROW           = 0;
    localparam                                                      STATE_INTER_ROW_WAIT     = 1;
    localparam                                                      STATE_ODD_ROW            = 2;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                           clk;
    input                                                           reset;

    //RX interface
    //MAX pool interface
    input        [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]         rx_max_pool_idx_in;
    input                                                           rx_max_pool_idx_valid_in;
    //line buffer interface
    input                                                           rx_window_valid_prev_in;

    //TX interface
    output       [NUM_OFM-1 : 0]                                    no_op_out;
    output                                                          no_op_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    //row, col state nets/regs
    wire        [DIM_WIDTH-1 : 0]                                   col_count;
    wire        [DIM_WIDTH-1 : 0]                                   row_count;
    wire        [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                    current_max_pool_neigh_idx;
    reg         [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                    current_max_pool_neigh_idx_reg;
    wire                                                            row_last_act;
    reg                                                             row_last_act_reg;
    wire                                                            im_last_act;

    //index buffer signals
    wire        [INDEX_CACHE_ADDR_WIDTH-1 : 0]                      max_pool_index_cache_wrt_addr;
    wire        [INDEX_CACHE_ADDR_WIDTH-1 : 0]                      max_pool_index_cache_rd_addr;
    wire        [INDEX_CACHE_ADDR_WIDTH-1 : 0]                      max_pool_index_cache_addr;
    reg                                                             max_pool_index_cache_wrt_en;
    wire        [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]          max_pool_index_cache_rd_data;

    //index comparator signals
    reg                                                             index_comparator_in_valid;
    reg         [NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]          max_pool_index;    

    integer                                                         state;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    //The following block maintains the row, column state w.r.t. the window valid
    feature_map_state_handler
    #(
        .CONV_IM_DIM            (CONV_IM_DIM),
        .DIM_PREDEFINED         (1)
    )
    u_feature_map_state_handler
    (
        .clk                    (clk),
        .reset                  (reset),

        .cfg_row_count_in       (),
        .cfg_col_count_in       (),
        .cfg_valid_in           (),

        .valid_in               (rx_window_valid_prev_in),

        .row_count_out          (row_count),
        .col_count_out          (col_count),
        .max_pool_neigh_idx_out (current_max_pool_neigh_idx),

        .row_last_act_out       (row_last_act),
        .im_last_act_out        (im_last_act)
    );
    assign max_pool_index_cache_addr = col_count/MAX_POOL_DIM;

    always@(posedge clk) begin
        if(reset) begin
            current_max_pool_neigh_idx_reg  <= {MAX_POOL_NEIGH_DIM_WIDTH{1'b0}};
            index_comparator_in_valid       <= 1'b0;
            row_last_act_reg                <= 1'b0;
            state                           <= STATE_EVEN_ROW;
        end
        else begin
            current_max_pool_neigh_idx_reg  <= current_max_pool_neigh_idx;
            index_comparator_in_valid       <= rx_window_valid_prev_in;
            row_last_act_reg                <= row_last_act;
            case(state)
                STATE_EVEN_ROW : begin
                    // if(rx_window_valid_prev_in && row_last_act) begin
                    if(index_comparator_in_valid && row_last_act_reg) begin
                        state               <= STATE_ODD_ROW;
                    end
                end
                STATE_INTER_ROW_WAIT : begin

                end
                STATE_ODD_ROW : begin
                    if(index_comparator_in_valid && row_last_act_reg) begin
                        state               <= STATE_EVEN_ROW;
                    end
                end
            endcase
        end
    end

    always@(*) begin
        case(state)
            STATE_EVEN_ROW : begin
                max_pool_index              = rx_max_pool_idx_in;
                max_pool_index_cache_wrt_en = rx_max_pool_idx_valid_in;
            end
            STATE_ODD_ROW : begin
                max_pool_index              = max_pool_index_cache_rd_data;
                max_pool_index_cache_wrt_en = 1'b0;
            end
            default : begin
                max_pool_index              = {(NUM_OFM * MAX_POOL_NEIGH_DIM_WIDTH) {1'b0}}; 
                max_pool_index_cache_wrt_en = 1'b0;
            end
        endcase
    end

    generate 
        if(NUM_OFM == 20) begin
            max_pool_idx_mem_l1
            u_max_pool_idx_mem_l1
            ( 
                .clka       (clk),
                .ena        (1'b1),      
                .wea        (max_pool_index_cache_wrt_en),
                .addra      (max_pool_index_cache_addr),  
                .dina       (rx_max_pool_idx_in),  
                .douta      (max_pool_index_cache_rd_data)
            );
        end 
        else if(NUM_OFM == 50) begin
            max_pool_idx_mem_l2
            u_max_pool_idx_mem_l2
            (
                .clka       (clk),                  
                .ena        (1'b1),                 
                .wea        (max_pool_index_cache_wrt_en),     
                .addra      (max_pool_index_cache_addr),  
                .dina       (rx_max_pool_idx_in),
                .douta      (max_pool_index_cache_rd_data)
            );
        end
    endgenerate

    genvar i;
    generate
        for(i=0; i<NUM_OFM; i=i+1) begin
            index_comparator
            u_index_comparator
            (
                //data receive port
                .current_idx_in         (current_max_pool_neigh_idx_reg),
                .max_idx_in             (max_pool_index    [i*MAX_POOL_NEIGH_DIM_WIDTH +: MAX_POOL_NEIGH_DIM_WIDTH]),
                .valid_in               (index_comparator_in_valid), //todo : change after validating synchronization

                //data transmit port
                .no_op_out              (no_op_out      [i]),
                .valid_out              ()
            );
        end
    endgenerate
    assign no_op_valid_out = index_comparator_in_valid;

    // // assign no_op_valid_out = tx_op_cmd_valid_bus[0];

endmodule
