`timescale 1ns / 1ps

////////////////////////This Convolution Approx has some issue, When approximation levels are increasing, the system is getting very erronous/////////////////////////////

module top
    (
        clk,
        reset,


        pixel_in,
        pixel_valid,

        data_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
   `include "params/layer_params.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
//    parameter                                                   DATA_WIDTH          = 8;
    parameter                                                   WEIGHT_WIDTH        = 8;
    parameter                                                   BIT_SHIFT_MODE      = 0 ;
    parameter                                                   CONV_KERNEL_DIM     = 3; 
    parameter                                                   LAYER_IDX           = 0;
    parameter                                                   OFM_COUNT           = 20;
    parameter                                                   IFM_COUNT           = 1;
    parameter                                                   USE_DSP             = 0;
    parameter                                                   REG_SIZE            = 8;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  DIM_WIDTH = 8;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [REG_SIZE*DATA_WIDTH-1 : 0]                     pixel_in;
    input                                                       pixel_valid;

    output      [CONV_OUT_WIDTH-1 : 0]                          data_out;
    output                                                      data_valid_out;

//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    //l1
    wire        [L1_WINDOW_WIDTH-1 : 0]                         l1_window_approx;
    wire                                                        l1_window_approx_valid;

    wire        [L1_WINDOW_WIDTH-1 : 0]                         l1_window;
    wire                                                        l1_window_valid;
    wire                                                        l1_window_prev_valid;
    wire        [L1_WINDOW_WIDTH-1 : 0]                         l1_window_delayed;
    wire                                                        l1_window_delayed_valid;

    wire        [L1_APPROX_CONV_OUT_DATA_WIDTH-1 : 0]           l1_approx_conv_result;
    wire                                                        l1_approx_conv_result_valid;

    wire        [L1_NUM_OFM-1 : 0]                              l1_approx_relu_result;
    wire                                                        l1_approx_relu_valid;

    wire        [L1_NUM_OFM-1 : 0]                              l1_no_op;
    reg         [L1_NUM_OFM-1 : 0]                              l1_no_op_reg;
    reg         [L1_NUM_OFM-1 : 0]                              l1_no_op_reg_a;

    wire                                                        l1_no_op_valid;
    reg                                                         l1_no_op_valid_reg;
    reg                                                         l1_no_op_valid_reg_a;


    wire        [L1_CONV_OUT_DATA_WIDTH-1 : 0]                  l1_conv_result;
    wire                                                        l1_conv_result_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_conv_result_is_data_valid_bus;
    wire                                                        l1_conv_result_odd_row_valid;

    wire        [L1_CONV_OUT_DATA_WIDTH-1 : 0]                  l1_pool_result;
    wire                                                        l1_pool_result_valid;

    wire        [L1_CONV_OUT_DATA_WIDTH-1 : 0]                  l1_relu_result;
    wire                                                        l1_relu_result_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_relu_result_is_data_valid;
    wire                                                        l1_relu_result_row_valid;
    
    wire        [L1_OUT_DATA_WIDTH-1 : 0]                       l1_quant_result_out;
    wire                                                        l1_quant_result_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_quant_is_data_valid;
    wire                                                        l1_quant_out_row_valid;

    wire        [L1_OUT_DATA_WIDTH-1 : 0]                       l1_out;
    wire                                                        l1_out_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_out_is_data_valid;
    wire                                                        l1_out_row_valid;

    wire        [L1_OUT_DATA_WIDTH-1 : 0]                       l1_out_delayed;
    wire                                                        l1_out_delayed_valid;


    wire       [CONV_IN_WIDTH-1 : 0]                            pixel_bus_in;
    wire                                                        pixel_valid_in;

    wire        [L2_WINDOW_WIDTH-1 : 0]                         l2_window_approx;
    wire                                                        l2_window_approx_valid;
    wire        [L2_WINDOW_WIDTH-1 : 0]                         l2_window;
    wire                                                        l2_window_valid;
    wire                                                        l2_window_prev_valid;
    wire                                                        l2_window_buff_valid;
    wire        [L2_WINDOW_WIDTH-1 : 0]                         l2_window_delayed;
    wire                                                        l2_window_delayed_valid;

    wire        [CONV_IN_WIDTH-1 : 0]                           l2_pix_bus_delayed;
    wire                                                        l2_pix_bus_delayed_valid;
    
    reg         [L2_NUM_OFM-1 : 0]                              l2_no_op;
    reg         [L2_NUM_OFM-1 : 0]                              l2_no_op_reg;
    reg         [L2_NUM_OFM-1 : 0]                              l2_no_op_reg_a;

    wire                                                        l2_no_op_valid;
    reg                                                         l2_no_op_valid_reg;
    
    wire        [L2_APPROX_CONV_OUT_DATA_WIDTH-1 : 0]           l2_approx_conv_result;
    wire                                                        l2_approx_conv_result_valid;

    wire        [L2_NUM_OFM-1 : 0]                              l2_approx_relu_result;
    wire                                                        l2_approx_relu_valid;

    wire        [L2_CONV_OUT_DATA_WIDTH-1 : 0]                  l2_conv_result;
    wire                                                        l2_conv_result_valid;
    wire        [L2_NUM_OFM-1 : 0]                              l2_conv_result_is_data_valid_bus;
    wire                                                        l2_conv_result_odd_row_valid;
    
    wire        [L2_CONV_OUT_DATA_WIDTH-1 : 0]                  l2_relu_result;
    wire                                                        l2_relu_result_valid;
    wire        [L2_NUM_OFM-1 : 0]                              l2_relu_result_is_data_valid;
    wire                                                        l2_relu_result_row_valid;

    wire        [L2_CONV_OUT_DATA_WIDTH-1 : 0]                  l2_pool_result;
    wire                                                        l2_pool_result_valid;

    wire        [L2_OUT_DATA_WIDTH-1 : 0]                       l2_quant_result_out;
    wire                                                        l2_quant_result_valid;
    wire        [L2_NUM_OFM-1 : 0]                              l2_quant_is_data_valid;
    wire                                                        l2_quant_out_row_valid;

    wire        [L2_OUT_DATA_WIDTH-1 : 0]                       l2_out;
    wire                                                        l2_out_valid;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
//VGG second CONV Layer Implementation
//---------------------------------------------------------------------------------------------------------------------
IOinterface
    IO
    (
    .clk(clk),
    .reset(reset),
    .pixel_in(pixel_in),
    .pixel_valid_in(pixel_valid),
    .pixel_out(pixel_bus_in),
    .pixel_valid_out(pixel_valid_in)
    );


line_buffer_reg
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .IMAGE_WIDTH        (L2_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L2_IMAGE_WIDTH),
        .NUM_IFM            (L2_NUM_IFM),
        .APPROX_MODE        (1),
        .CONV_KERNEL_DIM    (L2_CONV_DIM)
    )
    u_line_buff_l1
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        .pix_bus_in         (pixel_bus_in),
        .pix_bus_valid_in   (pixel_valid_in),

        //read port
        .line_out           (),
        .line_valid_out     (),

        .window_out         (l2_window_approx),
        .window_valid_out   (l2_window_approx_valid),
        .window_buff_valid_out
                            ()
    );

conv_layer
    #(
//        .DATA_WIDTH         (DATA_WIDTH),
        .WEIGHT_WIDTH       (3),
        .BIT_SHIFT_MODE     (1),
        .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
        .LAYER_IDX          (1),
        .OFM_COUNT          (L2_NUM_OFM),
        .IFM_COUNT          (L2_NUM_IFM),
        .USE_DSP            (0),
        .WINDOW_SEL_MODE    (0),
        .MAX_POOL_NEIGH_DIM_WIDTH
                            (L2_MAX_POOL_NEIGH_DIM_WIDTH),
        .PIPELINE_DELAY_STAGES
                            (3)
    )
    u_approx_conv_layer_l1
    (
        .clk                (clk),
        .reset              (reset),

        .pixel_bus_in       (l2_window_approx),
        .neigh_idx_bus_in   ({(L2_MAX_POOL_NEIGH_DIM_BUS_WIDTH){1'b0}}),
        .no_op_in           ({L2_NUM_OFM{1'b0}}),
        .pixel_valid_in     (l2_window_approx_valid),

        .data_out           (l2_approx_conv_result),
        .data_valid_out     (l2_approx_conv_result_valid)
    );

relu_layer
    #(
        .APPROX_MODE        (1),
        .DATA_WIDTH         (APPROX_SUM_WIDTH),
        .NUM_FM             (L2_NUM_OFM)
    )
    u_approx_relu_layer_l1
    (
        .data_bus_in        (l2_approx_conv_result),
        .data_bus_valid_in  (l2_approx_conv_result_valid),

        .data_bus_out       (l2_approx_relu_result),
        .data_bus_valid_out (l2_approx_relu_valid)
    );

delay_buffer
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .CONV_KERNEL_DIM    (L2_CONV_DIM),
        
        .DELAY_STAGES       (5),
        .NUM_IFM            (L2_NUM_IFM)
    )
    u_delay_buffer_l1
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        // .window_in          (pixel_bus_in),
        // .window_valid_in    (pixel_valid_in),
        .pixel_in           (pixel_bus_in),
        .pixel_valid_in     (pixel_valid_in),

        //read port
        // .window_out         (l1_pix_bus_delayed),
        // .window_valid_out   (l1_pix_bus_delayed_valid)
        .pixel_out          (l2_pix_bus_delayed),
        .pixel_valid_out    (l2_pix_bus_delayed_valid)
    );    


line_buffer_reg
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .IMAGE_WIDTH        (L2_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L2_IMAGE_WIDTH),
        .NUM_IFM            (L2_NUM_IFM),
        .APPROX_MODE        (0),
        .CONV_KERNEL_DIM    (L2_CONV_DIM)
    )
    u_line_buff
    (
        .clk                (clk),
        .reset              (reset),

        .pix_bus_in         (l2_pix_bus_delayed),
        .pix_bus_valid_in   (l2_pix_bus_delayed_valid),

        .line_out           (),
        .line_valid_out     (),

        .window_out         (l2_window),
        .window_valid_out   (l2_window_valid),
        .window_buff_valid_out  ()
    );

    integer j;
    always@(posedge clk) begin
        if(reset) begin
            l2_no_op            <= {L2_NUM_OFM{1'b0}};
            l2_no_op_reg        <= {L2_NUM_OFM{1'b0}};
            l2_no_op_reg_a      <= {L2_NUM_OFM{1'b0}};
        end
        else begin
            if(l2_approx_relu_valid) begin
                for(j=0; j<L2_NUM_OFM; j=j+1) begin
                    l2_no_op[j] <= ~l2_approx_relu_result[j] & l2_approx_relu_valid;
                end
            end 
            l2_no_op_reg        <= l2_no_op;
            l2_no_op_reg_a      <= l2_no_op_reg;
        end 
    end


conv_layer
    #(
        .WEIGHT_WIDTH       (WEIGHT_WIDTH),
        .BIT_SHIFT_MODE     (0),
        .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
        .LAYER_IDX          (1),
        .OFM_COUNT          (L2_NUM_OFM),
        .IFM_COUNT          (L2_NUM_IFM),
        .USE_DSP            (0),
        .WINDOW_SEL_MODE    (0),
        .MAX_POOL_NEIGH_DIM_WIDTH
                            (L2_MAX_POOL_NEIGH_DIM_WIDTH),
        .PIPELINE_DELAY_STAGES  
                            (3)
    )
    u_conv_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .pixel_bus_in       (l2_window),
        .neigh_idx_bus_in   ({(L2_MAX_POOL_NEIGH_DIM_BUS_WIDTH){1'b0}}),
        .no_op_in           (l2_no_op_reg_a),
        .pixel_valid_in     (l2_window_valid),

        .data_out           (l2_conv_result),
        .data_valid_out     (l2_conv_result_valid)
    );

max_pool_layer
    #(
        .POOL_DATA_WIDTH    (SUM_WIDTH),
        .IMAGE_WIDTH        (L2_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L2_IMAGE_WIDTH),
        .MAX_POOL_DIM       (L2_MAX_POOL_DIM),
        .OFM_COUNT          (L2_NUM_OFM),
        .OFM_ITER_COUNT     (1),
        .DIM_WIDTH          (8),
        .DIM_PREDEFINED     (1),
        .APPROX_MODE        (0)
    )
    u_max_pool_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .data_in            (l2_conv_result),
        .data_valid_in      (l2_conv_result_valid),

        .data_out           (l2_pool_result),
        .data_idx_out       (),
        .data_valid_out     (l2_pool_result_valid)
    );

relu_layer
    #(
        .APPROX_MODE        (0),
        .DATA_WIDTH         (SUM_WIDTH),
        .NUM_FM             (L2_NUM_OFM)
    )
    u_relu_layer_l1
    (
        .data_bus_in        (l2_pool_result),
        .data_bus_valid_in  (l2_pool_result_valid),

        .data_bus_out       (l2_relu_result),
        .data_bus_valid_out (l2_relu_result_is_data_valid)
    );


quant_dequant_activation_layer
    #(
        .IN_DATA_WIDTH      (SUM_WIDTH),
        .OFM_COUNT          (L2_NUM_OFM),
        .LAYER_IDX          (1),
        .MODE               ("int"),
        .SCALE_FACTOR_A     (L2_SCALE_FACTOR_A),
        .SCALE_FACTOR_B     (L2_SCALE_FACTOR_B)
    )
    u_quant_dequant_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .pixel_in           (l2_relu_result),
        .pixel_valid_in     (l2_relu_result_is_data_valid),

        .pixel_out          (data_out),
        .pixel_valid_out    (data_valid_out)
    );
 
    
endmodule
