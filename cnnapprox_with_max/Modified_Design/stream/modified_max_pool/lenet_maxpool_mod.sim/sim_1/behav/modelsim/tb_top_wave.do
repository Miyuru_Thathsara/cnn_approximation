onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/clk
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/reset
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/pixel_bus_in
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/neigh_idx_bus_in
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/no_op_in
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/pixel_valid_in
add wave -noupdate -expand -group Conv_Approx -radix decimal /tb_top/u_conv_top/u_approx_conv_layer_l1/data_out
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/data_is_valid_out
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/data_valid_out
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/data_odd_row_valid_out
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/dout_valid
add wave -noupdate -expand -group Conv_Approx /tb_top/u_conv_top/u_approx_conv_layer_l1/is_out_row_odd
add wave -noupdate -expand -group ReLU_Approx -radix decimal /tb_top/u_conv_top/u_approx_relu_layer_l1/data_bus_in
add wave -noupdate -expand -group ReLU_Approx /tb_top/u_conv_top/u_approx_relu_layer_l1/data_bus_valid_in
add wave -noupdate -expand -group ReLU_Approx /tb_top/u_conv_top/u_approx_relu_layer_l1/data_bus_out
add wave -noupdate -expand -group ReLU_Approx /tb_top/u_conv_top/u_approx_relu_layer_l1/data_bus_valid_out
add wave -noupdate -expand -group ReLU_Approx /tb_top/u_conv_top/u_approx_relu_layer_l1/data_bus_valid
add wave -noupdate -expand -group Delay_Buffer /tb_top/u_conv_top/u_delay_buffer_l1/clk
add wave -noupdate -expand -group Delay_Buffer /tb_top/u_conv_top/u_delay_buffer_l1/reset
add wave -noupdate -expand -group Delay_Buffer /tb_top/u_conv_top/u_delay_buffer_l1/pixel_in
add wave -noupdate -expand -group Delay_Buffer /tb_top/u_conv_top/u_delay_buffer_l1/pixel_valid_in
add wave -noupdate -expand -group Delay_Buffer /tb_top/u_conv_top/u_delay_buffer_l1/pixel_out
add wave -noupdate -expand -group Delay_Buffer /tb_top/u_conv_top/u_delay_buffer_l1/pixel_valid_out
add wave -noupdate -expand -group Delay_Buffer /tb_top/u_conv_top/u_delay_buffer_l1/rd_en
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/clk
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/reset
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_in
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_valid_in
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_out
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_valid_out
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/window_out
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/window_valid_out
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/window_valid_prev_out
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/window_buff_valid_out
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/col_count
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/row_count
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/row_count_delayed
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_addr
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_addr_delayed
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_rd_addr
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buffer_wrt_addr
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_reg
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/pix_bus_valid_reg
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_din
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_din_rearranged
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_dout
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_buff_wrt_en
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/window_extractor_out
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/window_extractor_out_valid
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/i
add wave -noupdate -expand -group Delayed_Line_Buffer /tb_top/u_conv_top/u_line_buff/line_out_valid
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/clk
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/reset
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_bus_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/neigh_idx_bus_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/no_op_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/pixel_valid_in
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_is_valid_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_valid_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/data_odd_row_valid_out
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/dout_valid
add wave -noupdate -expand -group Convolution_Layer /tb_top/u_conv_top/u_conv_layer_l2/is_out_row_odd
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/clk
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/reset
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/cfg_row_count_in
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/cfg_col_count_in
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/cfg_valid_in
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_in
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_valid_in
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_out
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_idx_out
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/col_count_out
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/row_count_out
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_valid_out
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_in_reg
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_in_reg_1
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_in_reg_2
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_in_reg_3
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_valid_in_reg
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/data_valid_in_reg_1
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/pix_buff_addr
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/pix_buff_rd_data
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/pix_buff_wrt_data
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/pix_buff_wrt_en
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/cfg_col_count
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/cfg_row_count
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/ofm_iter_count
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/col_count
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/row_count
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/ofm_iter_count_reg
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/col_count_reg
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/row_count_reg
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/col_count_reg_b
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/row_count_reg_b
add wave -noupdate -expand -group Max_Pool /tb_top/u_conv_top/u_max_pool_layer_l2/valid_bus
add wave -noupdate -expand -group ReLU_Layer /tb_top/u_conv_top/u_relu_layer_l1/data_bus_in
add wave -noupdate -expand -group ReLU_Layer /tb_top/u_conv_top/u_relu_layer_l1/data_bus_valid_in
add wave -noupdate -expand -group ReLU_Layer /tb_top/u_conv_top/u_relu_layer_l1/data_bus_out
add wave -noupdate -expand -group ReLU_Layer /tb_top/u_conv_top/u_relu_layer_l1/data_bus_valid_out
add wave -noupdate -expand -group ReLU_Layer /tb_top/u_conv_top/u_relu_layer_l1/data_bus_valid
add wave -noupdate -expand -group Quant_Dequant_Layer /tb_top/u_conv_top/u_quant_dequant_layer_l2/clk
add wave -noupdate -expand -group Quant_Dequant_Layer /tb_top/u_conv_top/u_quant_dequant_layer_l2/reset
add wave -noupdate -expand -group Quant_Dequant_Layer /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_in
add wave -noupdate -expand -group Quant_Dequant_Layer /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_valid_in
add wave -noupdate -expand -group Quant_Dequant_Layer /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_out
add wave -noupdate -expand -group Quant_Dequant_Layer /tb_top/u_conv_top/u_quant_dequant_layer_l2/pixel_valid_out
add wave -noupdate -expand -group Quant_Dequant_Layer /tb_top/u_conv_top/u_quant_dequant_layer_l2/out_valid
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {535287 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 196
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1987648 ps}
