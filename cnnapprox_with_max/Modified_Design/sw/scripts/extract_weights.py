from parse_params import *
from kernel_apprx_ML import *
#from kernel_apprx_multi_level_per_channel_closest_level import *

#net = init_network('lenet_train_test_baseline.prototxt','_iter_10000.caffemodel')
net = init_network('lenet_train_test_baseline.prototxt','lenet_50.caffemodel')
#net = init_network('lenet_train_test.prototxt','_iter_10000.caffemodel')

fp  = open("weight_params.sv", "w")
fp2 = open("image.sv", "w")
fp3 = open("layer1_activations.sv", "w")
fp4 = open("layer2_activations.sv", "w")

l1_weight_thresh       = 0.576697  #0.605852
#l1_weight_thresh       = 0.603 #0.606
#l1_input_thresh	       = 0.994849
l1_input_thresh	       = 0.994849
l2_weight_thresh       = 0.21659 #0.291896
#l2_weight_thresh       = 0.20023 #0.267
l2_input_thresh        = 3.570243 #4.308580
#l2_input_thresh        = 3.367 #3.86796
l3_input_thresh        = 10.100615 #10.350300
#l3_input_thresh        = 11.245 #12.4551

i8_max		       = 127
sum_max		       = 2**27

l1_weight_sf = l1_weight_thresh/i8_max
l1_input_sf  = l1_input_thresh/i8_max
l1_out_sf    = l1_weight_sf * l1_input_sf

l2_weight_sf = l2_weight_thresh/i8_max
l2_input_sf  = l2_input_thresh/i8_max

#conv1
quant_weights = quantize_weights_layer(net, 'conv1', (1/l1_weight_sf))
convert_weights(fp,'l1', quant_weights)
quant_bias = quantize_bias_layer(net, 'conv1', ((1/l1_weight_sf)*(1/l1_input_sf)), sum_max)
print("Quant Bias Conv1")
print(quant_bias)
convert_bias(fp, 'l1', quant_bias, 27)
quant_bias = quantize_bias_layer(net, 'conv1', (4 * (1/l1_input_sf)), (i8_max * i8_max))
convert_bias(fp, 'l1_approx', quant_bias, 27)

#conv2
quant_weights = quantize_weights_layer(net, 'conv2', (1/l2_weight_sf))
print(quant_weights[2][3])
convert_weights(fp,'l2', quant_weights)
#scale_factor = (127 * 127/3.570243) 
#print(scale_factor)
quant_bias = quantize_bias_layer(net, 'conv2', ((1/l2_weight_sf)*(1/l2_input_sf)), sum_max)
#print(quant_bias)
convert_bias(fp, 'l2', quant_bias, 27)
#scale_factor = (8 * 127/3.570243)
quant_bias = quantize_bias_layer(net, 'conv2', (8 * (1/l1_input_sf)), (i8_max * i8_max))
print("Quant Bias Conv2")
print(quant_bias)
convert_bias(fp, 'l2_approx', quant_bias, 27)


#approx weights 1
approx_weights = kernel_apprx_ML(net,"conv1","conv1_cus",1)
#approx_weights =  kernel_apprx_multi_level_per_channel_closest_level(net3,"conv1","conv1_cus",8)
approx_weight_levels = approximate_weights(approx_weights)
#print(np.min(approx_weight_levels))
convert_weights(fp,'l1_approx', approx_weight_levels)
#np.min(abs(approx_weights[np.nonzero(approx_weights)])))
#print(np.max(abs(approx_weights[np.nonzero(approx_weights)])))

approx_weights = kernel_apprx_ML(net,"conv2","conv2_cus",1)
approx_weight_levels = approximate_weights(approx_weights)
convert_weights(fp,'l2_approx', approx_weight_levels)

net.forward()
quant_data = convert_activations(fp2, net, 'data', 127)
quant_l1   = convert_activations(fp3, net, 'pool1', ((1/l1_weight_sf)*(1/l1_input_sf)))
#scale_factor = (127*127/3.570243)
#print("scale factor : %f" %(scale_factor))
quant_l2   = convert_activations(fp4, net, 'conv2', ((1/l2_weight_sf)*(1/l2_input_sf)))

fp.close()
fp2.close()
fp3.close()
fp4.close()
