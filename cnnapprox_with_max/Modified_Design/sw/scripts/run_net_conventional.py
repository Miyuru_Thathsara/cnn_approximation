#####################################################################################
#																				    #
# Project Title : Lowering Dynamic Power of a Stream-based CNN Hardware Accelerator #
# Author        : Rukshan Wickramasinghe											#
# Supervisor    : Prof. Lam Siew Kei   											    #
# Lab           : Hardware and Embedded Systems Lab (HESL), NTU, Singapore		    #
# Script name   : run_net_conventional.py											#
# Description   : This will load and run the conventional model of the network and 	#
#                 give the top-1 and top-5 accuracies of the original model.    	#
# Last Modified : 6th Dec 2018 													    #
#																				    #
#####################################################################################

# ** IMPORTANT ** Before running this script, check the batch size in the prototxt file is set correctly.

import caffe
import numpy as np
import time
import struct
from  num_processing import *

def float_to_hex(f):
    return hex(struct.unpack('<I', struct.pack('<f', f))[0])

#net = caffe.Net('lenet_conventional.prototxt','lenet_iter_10000.caffemodel',caffe.TEST)
#net = caffe.Net('lenet_train_test_baseline.prototxt','_iter_10000.caffemodel',caffe.TEST)
net = caffe.Net('lenet_train_test_baseline.prototxt','lenet_50.caffemodel',caffe.TEST)

tic = time.time()
net.forward()
toc = time.time()
t   = toc-tic

print "\nTime elapsed: %s Minutes"%(t/60.0)
print "\nAccuracy: "+str(net.blobs["accuracy"].data*100) +"%"
#print "Accuracy (Top 5): "+str(net.blobs["accuracy_5"].data*100) +"%"

bias1 = net.params['conv1'][1].data
bias2 = net.params['conv2'][1].data

#print(bias1)
#print(bias2)

bias1_hex = []
bias2_hex = []
for i in range(0, len(bias1)) :
    bias1_hex.append(float_to_hex(bias1[i]))
    
for i in range(0, len(bias2)) :
    bias2_hex.append(float_to_hex(bias2[i]))

#print(bias1_hex)
#print(bias2_hex)

#data1 = []
print(net.blobs["data"].data.shape)
data1 = net.blobs["data"].data[67]
data1 = data1.flatten()
data1 = data1 * 127
data1 = data1.astype(int)
#data1_int = data1_int.astype(int)
#data1_int = data1_int.flatten()
#print(data1_int.shape)
#np.savetxt('logs/data_int.txt', data1_int, delimiter=',', fmt="%d")
#print(data1)

data2 = []
data2 = net.blobs["pool1"].data
data2 = data2.flatten()

data3 = []
data3 = net.blobs["pool2"].data
#print(data3)
data3 = data3.flatten()

data4 = []
data4 = net.blobs["conv2"].data
#print(data4)
data4 = data4.flatten()

weights1 = []
weights1 = net.params['conv1'][0].data
weights1 = weights1.flatten()

weights2 = []
weights2 = net.params['conv2'][0].data
weights2 = weights2.flatten()

bias1 = []
bias1 = net.params['conv1'][1].data
bias1 = bias1.flatten()

bias1_max = max(abs(bias1))
print(bias1_max)

bias1_quant = np.round((bias1 * 127 * 127), 0) #scaling factors of weights and pixels
bias1_quant = bias1_quant.astype(int)
bias1_quant_hex = [tohex(x, 27) for x in bias1_quant]


bias2 = []
bias2 = net.params['conv2'][1].data
bias2 = bias2.flatten()

bias2_quant = np.round((bias2 * 127 * 127), 0) #scaling factors of weights and pixels
bias2_quant = bias2_quant.astype(int)
bias2_quant_hex = [tohex(x, 27) for x in bias2_quant]

bias2_max = max(abs(bias2))
print(bias2_max)

np.savetxt('logs/data_blob.txt', data1, delimiter=" ", fmt="%s") 
#np.savetxt('logs/pool1_blob.txt', data2, delimiter=" ", fmt="%s") 
#np.savetxt('logs/pool2_blob.txt', data3, delimiter=" ", fmt="%s") 
#np.savetxt('logs/weights1.txt', weights1, delimiter=" ", fmt="%s") 
#np.savetxt('logs/weights2.txt', weights2, delimiter=" ", fmt="%s") 
#np.savetxt('logs/bias1.txt', bias1, delimiter=" ", fmt="%s") 
#np.savetxt('logs/bias2.txt', bias2, delimiter=" ", fmt="%s")
#np.savetxt('logs/bias1_quant.txt', bias1_quant, delimiter=" ", fmt="%d")
#np.savetxt('logs/bias2_quant.txt', bias2_quant, delimiter=" ", fmt="%d")
#np.savetxt('logs/bias1_quant_hex.txt', bias1_quant_hex, delimiter=" ", fmt="%s")
#np.savetxt('logs/bias2_quant_hex.txt', bias2_quant_hex, delimiter=" ", fmt="%s")
