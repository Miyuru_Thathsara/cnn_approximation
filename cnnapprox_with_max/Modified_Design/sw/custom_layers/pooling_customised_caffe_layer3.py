import caffe
import numpy as np
import math
from im2col import *

def print_neighborhood(im_col, batch_idx ,channel_idx, row_idx, col_idx, out_rows, out_cols, channel_count, batch_count):
    print("print_neighborhood ", batch_idx, channel_idx, row_idx, col_idx)
    #neighborhood = np.reshape(im_col[((batch + row_idx * out_cols + col_idx)  * channels + channel_idx], (3,3))
    #pos_idx = ((batch_idx * out_rows + row_idx) * out_cols + col_idx)  * channels + channel_idx
    pos_idx = batch_idx + ((row_idx * out_cols + col_idx) *  channel_count + channel_idx) * batch_count
    neighborhood = np.reshape(im_col[pos_idx], (3,3))
    print(neighborhood)

def get_neighborhood(im_col, batch_idx ,channel_idx, row_idx, col_idx, out_rows, out_cols, channel_count, batch_count):
    neigh_idx = batch_idx + ((row_idx * out_cols + col_idx) *  channel_count + channel_idx) * batch_count
    #neighborhood = np.reshape(im_col[neigh_idx], (3,3))
    neighborhood = im_col[neigh_idx]
    return neighborhood

def get_max_idx(neigh) : 
    max_idx = np.argmax(neigh)
    #print("Max Idx ", max_idx)
    return max_idx

def traverse_neighborhood(im_col, batch_idx ,channel_idx, row_idx, col_idx, out_rows, out_cols, channel_count, batch_count):
    neighbor_mask = [-1, -1, -1, -1]
    top_neighborhood = []
    right_neighborhood = []
    bottom_neighborhood = []
    left_neighborhood = []
    top_neigh_max_idx = right_neigh_max_idx = bottom_neigh_max_idx = left_neigh_max_idx = current_neigh_max_idx = 0
    
    print("****************************************************************")
    #check the neighboring neighborhoods
    if(row_idx-1 >= 0):
	neighbor_mask[0] = 1
        top_neighborhood = get_neighborhood(im_col, batch_idx, channel_idx, (row_idx-1), col_idx, out_rows, out_cols, channel_count, batch_count)    
        top_neigh_max_idx = get_max_idx(top_neighborhood)
        print("Top")
        print(np.reshape(top_neighborhood, (3,3)), top_neigh_max_idx)
    if(col_idx+1 < out_cols):
	neighbor_mask[1] = 1
        right_neighborhood = get_neighborhood(im_col, batch_idx, channel_idx, row_idx, (col_idx+1), out_rows, out_cols, channel_count, batch_count)    
        right_neigh_max_idx = get_max_idx(right_neighborhood) 
        print("Right")
        print(np.reshape(right_neighborhood, (3,3)), right_neigh_max_idx)
    if(row_idx+1 < out_rows):
	neighbor_mask[2] = 1
        bottom_neighborhood = get_neighborhood(im_col, batch_idx, channel_idx, (row_idx+1), col_idx, out_rows, out_cols, channel_count, batch_count)    
        bottom_neigh_max_idx = get_max_idx(bottom_neighborhood) 
        print("Bottom")
        print(np.reshape(bottom_neighborhood, (3,3)), bottom_neigh_max_idx)
    if(col_idx-1 >= 0):
	neighbor_mask[3] = 1
        left_neighborhood = get_neighborhood(im_col, batch_idx, channel_idx, row_idx, (col_idx-1), out_rows, out_cols, channel_count, batch_count)    
        left_neigh_max_idx = get_max_idx(left_neighborhood) 
        print("Left")
        print(np.reshape(left_neighborhood, (3,3)), left_neigh_max_idx)
    
    #get current neighborhood and max
    neighborhood = get_neighborhood(im_col, batch_idx, channel_idx, row_idx, col_idx, out_rows, out_cols, channel_count, batch_count)    
    current_neigh_max_idx = get_max_idx(neighborhood) 
    print(neighborhood)
    print("traverse_neighborhood ", batch_idx, channel_idx, row_idx, col_idx, neighbor_mask, current_neigh_max_idx)
    print("Current")
    print(np.reshape(neighborhood, (3,3)), current_neigh_max_idx)

    #count the computations by traversing the max pool neighborhood
    computations = 4
    
    # 1,1
    if(neighborhood[1] > neighborhood[0]) :
	computations += 1 
    elif(neighbor_mask[0] != -1) : 
        top_max_idx = get_max_idx(top_neighborhood[0:8])
        if(top_max_idx == 7) :
            computations += 1
    # 1,0
    current_max = get_max_idx(neighborhood[0:3])
    if(neighborhood[3] > current_max) : 
	computations += 1
    elif(neighbor_mask[3] != -1) : 
        left_max_idx = get_max_idx(left_neighborhood[0:6])
        if(left_max_idx == 5) : 
	    computations += 1

    # 1,1
    current_max = get_max_idx(neighborhood[0:5])
    if(current_max == 4) : 
        computations += 1
    
    # 1,2
    current_max = get_max_idx(neighborhood[0:6])
    if(current_max == 5) : 
        computations += 1
    elif(neighbor_mask[1] != -1) : 
        right_max_idx = get_max_idx(right_neighborhood[0:4])
        if(right_max_idx == 3) : 
	    computations += 1
	
    # 2,1  
    current_max = get_max_idx(neighborhood[0:8])
    if(current_max == 7) : 
        computations += 1
    elif(neighbor_mask[2] != -1) : 
        bottom_max_idx = get_max_idx(right_neighborhood[0:2])
        if(bottom_max_idx == 1) : 
	    computations += 1

    print("Computations in current neighborhood ", computations)    

def analyze_probabilistic_pool(im_col, fm_dim):
    np.set_printoptions(threshold=np.nan)
    kernel_dim = im_col.shape[0]
    kernel_count = im_col.shape[1]  
    [batch, channels, rows, cols] = fm_dim
    out_rows = rows/2
    out_cols = cols/2
    print("IM col kernel_dim : ", kernel_dim, " kernel_count : ", kernel_count)
    print("FM dim : ", [batch, channels, rows, cols])
    #for i in range kernel_count : 
        #2
     #   if(
    im_col_transposed = np.transpose(im_col)
    print("im col transposed shape ", im_col_transposed.shape)
    #print(np.reshape(im_col_transposed[0], (3,3)))
    #print(np.reshape(im_col_transposed[1], (3,3)))
    #print(np.reshape(im_col_transposed[2], (3,3)))
    batch_idx = 0
    channel = 2
    batch_count = 2
    row  = 4; col  = 3;
    traverse_neighborhood(im_col_transposed, batch_idx, channel, row, col, out_rows, out_cols, channels, batch_count)
    row  = 4; col  = 3;
    #print_neighborhood(im_col_transposed, batch_idx, channel, row, col, out_rows, out_cols, channels, batch_count)
    #print(np.reshape(im_col_transposed[(row * out_cols + col)  * channels], (3,3)))
    row  = 4; col  = 4;
    #print_neighborhood(im_col_transposed, batch_idx, channel, row, col, out_rows, out_cols, channels, batch_count)
    #print(np.reshape(im_col_transposed[(row * out_cols + col)  * channels], (3,3)))
    row  = 3; col  = 3;
    #print_neighborhood(im_col_transposed, batch_idx, channel, row, col, out_rows, out_cols, channels, batch_count)
    #print(np.reshape(im_col_transposed[(row * out_cols + col)  * channels], (3,3)))
    row  = 5; col  = 3;
    #print_neighborhood(im_col_transposed, batch_idx, channel, row, col, out_rows, out_cols, channels, batch_count)
    #print(np.reshape(im_col_transposed[(row * out_cols + col)  * channels], (3,3)))
    row  = 5; col  = 4;
    #print_neighborhood(im_col_transposed, batch_idx, channel, row, col, out_rows, out_cols, channels, batch_count)
    #print(np.reshape(im_col_transposed[(row * out_cols + col)  * channels], (3,3)))
    #print(im_col_transposed[32])
    #for i in range(0,2) : 
    #    print(im_col[:][i])

class pooling_customised_caffe_layer3(caffe.Layer):
    def setup(self, bottom, top):
        #if len(top) != 2:
        #    raise Exception("2 TOP BLOBS IS REQUIRED FOR CUSTOMISED CONVOLUTION LAYER")
        if len(bottom) != 2:
            raise Exception("2 BOTTOM BLOBS ARE REQUIRED FOR CUSTOMISED CONVOLUTION LAYER")
        #params = eval(self.param_str)
	#TODO: retrieve from prototxt
	params={"pool_kernel_size":3,
		"pool_stride":2,
		}
        self.pool_kernel_size = params["pool_kernel_size"]
        self.pool_stride = params["pool_stride"]


    def forward(self, bottom, top):
        image_ori=bottom[0].data
	image_mod=bottom[1].data
        in_batch, in_channel, in_row, in_col = image_ori.shape
	h_out = int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride)))
	w_out = int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))
        top[0].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
	top[0].data[...]=np.zeros((in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))))
	top[1].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
        top[1].data[...]=np.zeros((in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))))
	image_ori_reshaped= image_ori.reshape(in_batch * in_channel, 1, in_row, in_col)
	image_mod_reshaped= image_mod.reshape(in_batch * in_channel, 1, in_row, in_col)
	im_ori_col = im2col_indices(image_ori_reshaped, self.pool_kernel_size, self.pool_kernel_size, padding=0, stride=self.pool_stride)
	im_mod_col = im2col_indices(image_mod_reshaped, self.pool_kernel_size, self.pool_kernel_size, padding=0, stride=self.pool_stride)
	print("Im Shape : ", image_ori.shape, " Im Col Shape : ", im_mod_col.shape)
	max_idx = np.argmax(im_mod_col, axis=0)
        analyze_probabilistic_pool(im_mod_col, image_ori.shape)
	out_orig = im_ori_col[max_idx, range(max_idx.size)]
	out_orig = out_orig.reshape(h_out, w_out, in_batch, in_channel)
	out_orig = out_orig.transpose(2, 3, 0, 1)
	out_approx = im_mod_col[max_idx, range(max_idx.size)]
        out_approx = out_approx.reshape(h_out, w_out, in_batch, in_channel)
        out_approx = out_approx.transpose(2, 3, 0, 1)
	print(out_orig.shape)
	print(out_approx.shape)
	print(top[0].data.shape)
	print(top[1].data.shape)
	top[0].data[...] = out_orig
	top[1].data[...] = out_approx
        print "***** POOL1 for BATCH " + str(in_batch) + " completed ***** "


    def __getitem__(self):
        pass

    def reshape(self, bottom, top):
	image_ori=bottom[0].data
	in_batch, in_channel, in_row, in_col = image_ori.shape
	top[0].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
	top[1].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
        pass

    def backward(self, bottom, top):
        pass


