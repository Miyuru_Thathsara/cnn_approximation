#####################################################################################
#																				    #
# Project Title : Lowering Dynamic Power of a Stream-based CNN Hardware Accelerator #
# Author        : Rukshan Wickramasinghe											#
# Supervisor    : Prof. Lam Siew Kei   											    #
# Lab           : Hardware and Embedded Systems Lab (HESL), NTU, Singapore		    #
# Script name   : kernel_apprx_ML.py												#
# Description   : This function replaces the weights of the "cus_conv" layer of the #
#                 "net" with the quantized values of the weights of "conv" layer 	#
#                 using the number of levels given.									#
# Last Modified : 6th Dec 2018 													    #
#																				    #
#####################################################################################

import caffe
import numpy as np
import math


def kernel_apprx_ML(net,kernel_name_original,kernel_name_cus,num_levels=5):
	k=net.params[kernel_name_original][0].data
	shp=k.shape
	siz=k.size
	unrolled=k.reshape((1,siz))
	bins=[0]+[1/2.0**i for i in range(num_levels)][::-1]	#determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]
	
	levels=np.array(bins).reshape((len(bins),1))
	abs_distance=np.abs(np.abs(unrolled)-levels)
	print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
	print(levels.shape)
	print(unrolled.shape)
	print(abs_distance.shape)
	locations=np.argmin(abs_distance,axis=0).reshape((1,siz))
	print(locations.shape)
	print(unrolled[0][1:10])
	print(levels)
	print(locations[0][1:10])
	print(levels[locations[0][1:10]])
	neg_pos=np.zeros((1,siz))
	neg_pos[unrolled>0]=1.0
	neg_pos[unrolled<0]=-1.0
	temp=np.zeros((1,siz))
	for i in range(levels.size):
		temp[locations==i]=levels[i]
	temp_final=temp*neg_pos
	temp=temp_final.reshape(shp)
	#net.params[kernel_name_cus][0].data[...]=temp
	#net.params[kernel_name_cus][1].data[...]=0
	num_levels = math.log( (np.max(np.abs(temp[np.nonzero(temp)]))/np.min(np.abs(temp[np.nonzero(temp)])) ), 2) + 1
	print "\n\nApproximated kernels in layer "+kernel_name_original +" with "+str(num_levels)+" Levels\n"+str(bins)+"\n\n"
	print("Actual Min level : %f Max level : %f Num Levels : %d" %(np.min(k), np.max(k), num_levels) )
	print("Approx Min level : %f Max level : %f Num Levels : %d" %(np.min(np.abs(temp[np.nonzero(temp)])), np.max(np.abs(temp[np.nonzero(temp)])), num_levels) )
	return temp


def update_params(net,kernel_name_original,kernel_name_cus,temp):
    print("update params")
    net.params[kernel_name_cus][0].data[...]=temp
       #if(kernel_name_original == "conv2") : 
 	#    net.params[kernel_name_cus][1].data[...]=0
	#else :
	#    net.params[kernel_name_cus][1].data[...]=net.params[kernel_name_original][1].data
    #net.params[kernel_name_cus][1].data[...]=0
	#print(net.params[kernel_name_original][1].data)
    net.params[kernel_name_cus][1].data[...]=net.params[kernel_name_original][1].data
