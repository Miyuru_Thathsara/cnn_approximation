`timescale 1ns / 1ps

module top
    (
        clk,
        reset,

        

        pixel_bus_in,
        pixel_valid_in,

        data_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
   `include "params/layer_params.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
//    parameter                                                   DATA_WIDTH          = 8;
    parameter                                                   WEIGHT_WIDTH        = 8;
    parameter                                                   BIT_SHIFT_MODE      = 0 ;
    parameter                                                   CONV_KERNEL_DIM     = 5; 
    parameter                                                   LAYER_IDX           = 0;
    parameter                                                   OFM_COUNT           = 20;
    parameter                                                   IFM_COUNT           = 1;
    parameter                                                   USE_DSP             = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  DIM_WIDTH = 8;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [CONV_IN_WIDTH-1 : 0]                           pixel_bus_in;
    input                                                       pixel_valid_in;

    output      [CONV_OUT_WIDTH-1 : 0]                          data_out;
    output                                                      data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    //l1
    wire        [L1_WINDOW_WIDTH-1 : 0]                         l1_window_approx;
    wire                                                        l1_window_approx_valid;

    wire        [L1_WINDOW_WIDTH-1 : 0]                         l1_window;
    wire                                                        l1_window_valid;
    wire                                                        l1_window_prev_valid;
    // wire                                                        l1_window_buff_valid;
    wire        [L1_WINDOW_WIDTH-1 : 0]                         l1_window_delayed;
    wire                                                        l1_window_delayed_valid;

    wire        [CONV_IN_WIDTH-1 : 0]                           l1_pix_bus_delayed;
    wire                                                        l1_pix_bus_delayed_valid;
    
    wire        [L1_APPROX_CONV_OUT_DATA_WIDTH-1 : 0]           l1_approx_conv_result;
    wire                                                        l1_approx_conv_result_valid;

    wire        [L1_APPROX_CONV_OUT_DATA_WIDTH-1 : 0]           l1_approx_pool_data;
    wire        [L1_NUM_OFM * L1_MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]l1_approx_pool_idx;
    wire        [DIM_WIDTH-1 : 0]                               l1_approx_pool_col;
    wire                                                        l1_approx_pool_valid;

    wire        [L1_NUM_OFM-1 : 0]                              l1_approx_relu_result;
    wire                                                        l1_approx_relu_valid;

    wire        [L1_NO_OP_CMD_BUS_WIDTH-1 : 0]                  l1_no_op;
    reg         [L1_NO_OP_CMD_BUS_WIDTH-1 : 0]                  l1_no_op_reg;
    reg         [L1_NO_OP_CMD_BUS_WIDTH-1 : 0]                  l1_no_op_reg_a;

    wire                                                        l1_no_op_valid;
    reg                                                         l1_no_op_valid_reg;
    reg                                                         l1_no_op_valid_reg_a;


    wire        [L1_CONV_OUT_DATA_WIDTH-1 : 0]                  l1_conv_result;
    wire                                                        l1_conv_result_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_conv_result_is_data_valid_bus;
    wire                                                        l1_conv_result_odd_row_valid;

    wire        [L1_CONV_OUT_DATA_WIDTH-1 : 0]                  l1_pool_result;
    wire                                                        l1_pool_result_valid;

    wire        [L1_CONV_OUT_DATA_WIDTH-1 : 0]                  l1_relu_result;
    wire                                                        l1_relu_result_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_relu_result_is_data_valid;
    wire                                                        l1_relu_result_row_valid;
    
    wire        [L1_OUT_DATA_WIDTH-1 : 0]                       l1_quant_result_out;
    wire                                                        l1_quant_result_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_quant_is_data_valid;
    wire                                                        l1_quant_out_row_valid;

    wire        [L1_OUT_DATA_WIDTH-1 : 0]                       l1_out;
    wire                                                        l1_out_valid;
    wire        [L1_NUM_OFM-1 : 0]                              l1_out_is_data_valid;
    wire                                                        l1_out_row_valid;

    //l2
//    wire        [L2_LINE_WIDTH-1 : 0]                           l2_line;
//    wire        [L2_LINE_WIDTH-1 : 0]                           l2_line_delayed;
//    wire                                                        l2_line_valid;
//    wire                                                        l2_line_delayed_valid;
    wire        [L1_OUT_DATA_WIDTH-1 : 0]                       l1_out_delayed;
    wire                                                        l1_out_delayed_valid;

    wire        [L2_WINDOW_WIDTH-1 : 0]                         l2_window_approx;
    wire                                                        l2_window_approx_valid;
    wire        [L2_WINDOW_WIDTH-1 : 0]                         l2_window;
    wire                                                        l2_window_valid;
    wire                                                        l2_window_prev_valid;
    wire                                                        l2_window_buff_valid;
    wire        [L2_WINDOW_WIDTH-1 : 0]                         l2_window_delayed;
    wire                                                        l2_window_delayed_valid;

    wire        [L2_NO_OP_CMD_BUS_WIDTH-1 : 0]                  l2_no_op;
    reg         [L2_NO_OP_CMD_BUS_WIDTH-1 : 0]                  l2_no_op_reg;

    wire                                                        l2_no_op_valid;
    reg                                                         l2_no_op_valid_reg;
    
    wire        [L2_APPROX_CONV_OUT_DATA_WIDTH-1 : 0]           l2_approx_conv_result;
    wire                                                        l2_approx_conv_result_valid;

    wire        [L2_APPROX_CONV_OUT_DATA_WIDTH-1 : 0]           l2_approx_pool_result;
    wire        [L2_NUM_OFM * L2_MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]l2_approx_pool_idx;
    wire        [DIM_WIDTH-1 : 0]                               l2_approx_pool_col;
    wire                                                        l2_approx_pool_valid;


    wire        [L2_NUM_OFM-1 : 0]                              l2_approx_relu_result;
    wire                                                        l2_approx_relu_valid;

    wire        [L2_CONV_OUT_DATA_WIDTH-1 : 0]                  l2_conv_result;
    wire                                                        l2_conv_result_valid;
    wire        [L2_NUM_OFM-1 : 0]                              l2_conv_result_is_data_valid_bus;
    wire                                                        l2_conv_result_odd_row_valid;
    
    wire        [L2_CONV_OUT_DATA_WIDTH-1 : 0]                  l2_relu_result;
    wire                                                        l2_relu_result_valid;
    wire        [L2_NUM_OFM-1 : 0]                              l2_relu_result_is_data_valid;
    wire                                                        l2_relu_result_row_valid;

    wire        [L2_CONV_OUT_DATA_WIDTH-1 : 0]                  l2_pool_result;
    wire                                                        l2_pool_result_valid;

    wire        [L2_OUT_DATA_WIDTH-1 : 0]                       l2_quant_result_out;
    wire                                                        l2_quant_result_valid;
    wire        [L2_NUM_OFM-1 : 0]                              l2_quant_is_data_valid;
    wire                                                        l2_quant_out_row_valid;

    wire        [L2_OUT_DATA_WIDTH-1 : 0]                       l2_out;
    wire                                                        l2_out_valid;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    
    //***************************************************
    // Layer 1
    //***************************************************
    line_buffer_reg
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .IMAGE_WIDTH        (L1_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L1_IMAGE_WIDTH),
        .NUM_IFM            (L1_NUM_IFM),
        .APPROX_MODE        (1),
        .CONV_KERNEL_DIM    (L1_CONV_DIM)
    )
    u_line_buff_l1
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        .pix_bus_in         (pixel_bus_in),
        // .pix_bus_is_data_valid_in
        //                     (pixel_valid_in),
        .pix_bus_valid_in   (pixel_valid_in),

        //read port
        .line_out           (),
        .line_valid_out     (),

        .window_out         (l1_window_approx),
        .window_valid_out   (l1_window_approx_valid),
        .window_buff_valid_out
                            ()
    );

    conv_layer
    #(
//        .DATA_WIDTH         (DATA_WIDTH),
        .WEIGHT_WIDTH       (3),
        .BIT_SHIFT_MODE     (1),
        .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
        .LAYER_IDX          (0),
        .OFM_COUNT          (L1_NUM_OFM),
        .IFM_COUNT          (L1_NUM_IFM),
        .USE_DSP            (0),
        .WINDOW_SEL_MODE    (0),
        .MAX_POOL_NEIGH_DIM_WIDTH
                            (L1_MAX_POOL_NEIGH_DIM_WIDTH),
        .PIPELINE_DELAY_STAGES
                            (2)
    )
    u_approx_conv_layer_l1
    (
        .clk                (clk),
        .reset              (reset),

        .pixel_bus_in       (l1_window_approx),
        .neigh_idx_bus_in   ({(L1_MAX_POOL_NEIGH_DIM_BUS_WIDTH){1'b0}}),
        .no_op_in           ({(L1_NO_OP_CMD_BUS_WIDTH){1'b0}}),
        .pixel_valid_in     (l1_window_approx_valid),

        .data_out           (l1_approx_conv_result),
        .data_valid_out     (l1_approx_conv_result_valid)
    );

    // approx_max_pool_layer
    // #(
    //     .POOL_DATA_WIDTH        (SUM_WIDTH),
    //     .CONV_IM_DIM            (L1_CONV_IMAGE_WIDTH),
    //     .MAX_POOL_DIM           (L1_MAX_POOL_DIM),
    //     .OFM_COUNT              (L1_NUM_OFM),
    //     .OFM_ITER_COUNT         (1),
    //     .DIM_WIDTH              (8),
    //     .DIM_PREDEFINED         (1),
    //     .APPROX_MODE            (0)
    // )
    // u_approx_max_pool_layer_l1
    // (
    //     .clk                    (clk),
    //     .reset                  (reset),

    //     .data_in                (),
    //     .data_valid_in          (l1_approx_conv_result_valid),

    //     //out channel
    //     .out_ch_data_valid_in   (l1_window_valid)
    // );

    max_pool_layer
    #(
        .POOL_DATA_WIDTH    (APPROX_SUM_WIDTH),
        .IMAGE_WIDTH        (L1_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L1_IMAGE_WIDTH),
        .MAX_POOL_DIM       (L1_MAX_POOL_DIM),
        .OFM_COUNT          (L1_NUM_OFM),
        .OFM_ITER_COUNT     (1),
        .DIM_WIDTH          (8),
        .DIM_PREDEFINED     (1),
        .APPROX_MODE        (1)
    )
    u_approx_max_pool_layer_l1
    (
        .clk                (clk),
        .reset              (reset),

        .data_in            (l1_approx_conv_result),
        .data_valid_in      (l1_approx_conv_result_valid),

        .data_out           (l1_approx_pool_data),
        .data_idx_out       (l1_approx_pool_idx),
        .col_count_out      (l1_approx_pool_col),
        .row_count_out      (),
        .data_valid_out     (l1_approx_pool_valid)
    );

    relu_layer
    #(
        .APPROX_MODE        (1),
        .DATA_WIDTH         (APPROX_SUM_WIDTH),
        .NUM_FM             (L1_NUM_OFM)
    )
    u_approx_relu_layer_l1
    (
        .data_bus_in        (l1_approx_pool_data),
        .data_bus_row_valid_in  
                            (l1_approx_pool_valid),

        .data_bus_out       (l1_approx_relu_result),
        .data_bus_row_valid_out 
                            (l1_approx_relu_valid)
    );

    //relu is asynchronous. Hence pool and relu outputs are in sync. The following assignment combines the outputs from both.
    wire    [L1_APPROX_CONTROLLER_IDX_BUS_WIDTH-1 : 0] l1_approx_controller_in_idx;
    genvar i;
    generate
        for(i=0; i<L1_NUM_OFM; i=i+1) begin    
            assign l1_approx_controller_in_idx[i*L1_APPROX_CONTROLLER_IDX_WIDTH +: L1_APPROX_CONTROLLER_IDX_WIDTH] 
                                = {l1_approx_relu_result[i], l1_approx_pool_idx[i * L1_MAX_POOL_NEIGH_DIM_WIDTH +: L1_MAX_POOL_NEIGH_DIM_WIDTH]};
        end
    endgenerate

    conv_approx_controller
    #(
        .CONV_KERNEL_DIM        (L1_CONV_DIM),
        .CONV_IM_DIM            (L1_CONV_IMAGE_WIDTH),
        .MAX_POOL_DIM           (L1_MAX_POOL_DIM),
        .OFM_ITER_COUNT         (1),
        .DIM_WIDTH              (8),
        .DIM_PREDEFINED         (1),
        .APPROX_MODE            (1),
        .NUM_OFM                (L1_NUM_OFM),  
        .LAYER_IDX              (0)
    )
    u_conv_approx_controller_l1
    (
        .clk                    (clk),
        .reset                  (reset),

        //RX interface
        //MAX pool interface
        // .rx_col_in              (l1_approx_pool_col),
        // .rx_max_pool_idx_in     (l1_approx_pool_idx),
        .rx_max_pool_idx_in     (l1_approx_controller_in_idx),
        .rx_max_pool_idx_valid_in            
                                (l1_approx_pool_valid),
        .rx_window_valid_prev_in(l1_window_prev_valid),

        //TX interface
        .no_op_out              (l1_no_op),
        .no_op_valid_out        (l1_no_op_valid)
    );

    // relu_layer
    // #(
    //     .APPROX_MODE        (1),
    //     .DATA_WIDTH         (APPROX_SUM_WIDTH),
    //     .NUM_FM             (L1_NUM_OFM)
    // )
    // u_approx_relu_layer_l1
    // (
    //     .data_bus_in        (l1_approx_conv_result),
    //     .data_bus_valid_in  (l1_approx_conv_result_valid),

    //     .data_bus_out       (l1_approx_relu_result),
    //     .data_bus_valid_out (l1_approx_relu_valid)
    // );

    delay_buffer
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .CONV_KERNEL_DIM    (L1_CONV_DIM),
        
        .DELAY_STAGES       (4 + 33),
        .NUM_IFM            (L1_NUM_IFM)
    )
    u_delay_buffer_l1
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        // .window_in          (pixel_bus_in),
        // .window_valid_in    (pixel_valid_in),
        .pixel_in           (pixel_bus_in),
        .pixel_valid_in     (pixel_valid_in),

        //read port
        // .window_out         (l1_pix_bus_delayed),
        // .window_valid_out   (l1_pix_bus_delayed_valid)
        .pixel_out          (l1_pix_bus_delayed),
        .pixel_valid_out    (l1_pix_bus_delayed_valid)
    );

    line_buffer_reg
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .IMAGE_WIDTH        (L1_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L1_IMAGE_WIDTH),
        .NUM_IFM            (L1_NUM_IFM),
        .APPROX_MODE        (0),
        .CONV_KERNEL_DIM    (L1_CONV_DIM)
    )
    u_line_buff_delayed_l1
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        .pix_bus_in         (l1_pix_bus_delayed),
        // .pix_bus_is_data_valid_in
        //                     (l1_pix_bus_delayed_valid),
        .pix_bus_valid_in   (l1_pix_bus_delayed_valid),
//        .pix_bus_in         (pixel_bus_in),
//        .pix_bus_valid_in   (pixel_valid_in),

        //read port
        .line_out           (),
        .line_valid_out     (),

        .window_out         (l1_window),
        .window_valid_out   (l1_window_valid),
        .window_valid_prev_out
                            (l1_window_prev_valid),
        .window_buff_valid_out
                            ()
    );

//     integer j;
//     always@(posedge clk) begin
//         if(reset) begin
//             l1_no_op            <= {L1_NUM_OFM{1'b0}};
//             l1_no_op_reg        <= {L1_NUM_OFM{1'b0}};
//             l1_no_op_reg_a      <= {L1_NUM_OFM{1'b0}};

//             l1_no_op_valid      <= 1'b0;
//             l1_no_op_valid_reg  <= 1'b0;
//             l1_no_op_valid_reg_a<= 1'b0;
//         end
//         else begin
//             if(l1_approx_relu_valid) begin
// //            if(pixel_valid_in) begin
//                 for(j=0; j<L1_NUM_OFM; j=j+1) begin
//                     l1_no_op[j] <= ~l1_approx_relu_result[j] & l1_approx_relu_valid;
// //                     l1_no_op[j] <= 1'b0;
//                 end
//             end 
//             l1_no_op_reg        <= l1_no_op;
//             l1_no_op_reg_a      <= l1_no_op_reg;

//             l1_no_op_valid      <= l1_approx_relu_valid;
//             l1_no_op_valid_reg  <= l1_no_op_valid;
//             l1_no_op_valid_reg_a<= l1_no_op_valid_reg;
//         end 
//     end

//     wire                        l1_no_op_valid_delayed;
//     wire    [L1_NUM_OFM-1 : 0]  l1_no_op_delayed;

//     shift_reg
//     #(
//         .CLOCK_CYCLES   (33),
//         .DATA_WIDTH     (1 + L1_NUM_OFM)    //+2 for valid,last
//     )
//     u_no_op_shift_reg_l1
//     (
//         .clk            (clk),

//         .enable         (1'b1),
//         .data_in        ({l1_no_op_valid_reg_a, l1_no_op_reg_a}),
//         .data_out       ({l1_no_op_valid_delayed, l1_no_op_delayed})
//     );

    conv_layer
    #(
//        .DATA_WIDTH         (DATA_WIDTH),
        .WEIGHT_WIDTH       (WEIGHT_WIDTH),
        .BIT_SHIFT_MODE     (0),
        .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
        .LAYER_IDX          (0),
        .OFM_COUNT          (L1_NUM_OFM),
        .IFM_COUNT          (L1_NUM_IFM),
        .USE_DSP            (0),
        .WINDOW_SEL_MODE    (0),
        .MAX_POOL_NEIGH_DIM_WIDTH
                            (L1_MAX_POOL_NEIGH_DIM_WIDTH),
        .PIPELINE_DELAY_STAGES
                            (2),
        .CONV_IM_DIM        (L1_CONV_IMAGE_WIDTH)
    )
    u_conv_layer_l1
    (
        .clk                (clk),
        .reset              (reset),

        .pixel_bus_in       (l1_window),
        .neigh_idx_bus_in   ({(L1_MAX_POOL_NEIGH_DIM_BUS_WIDTH){1'b0}}),
        //.no_op_in           (l1_no_op_reg),
        // .no_op_in           (l1_no_op_reg_a),
        .no_op_in           (l1_no_op),
        // .no_op_in           ({L1_NUM_OFM{1'b0}}),
        .pixel_valid_in     (l1_window_valid),

        .data_out           (l1_conv_result),
        .data_valid_out     (l1_conv_result_valid),
        .data_is_valid_out  (l1_conv_result_is_data_valid_bus),
        .data_odd_row_valid_out
                            (l1_conv_result_odd_row_valid)
    );

    // max_pool_layer
    // #(
    //     .POOL_DATA_WIDTH    (SUM_WIDTH),
    //     .IMAGE_WIDTH        (L1_IMAGE_WIDTH),
    //     .IMAGE_HEIGHT       (L1_IMAGE_WIDTH),
    //     .MAX_POOL_DIM       (L1_MAX_POOL_DIM),
    //     .OFM_COUNT          (L1_NUM_OFM),
    //     .OFM_ITER_COUNT     (1),
    //     .DIM_WIDTH          (8),
    //     .DIM_PREDEFINED     (1),
    //     .APPROX_MODE        (0)
    // )
    // u_max_pool_layer_l1
    // (
    //     .clk                (clk),
    //     .reset              (reset),

    //     .data_in            (l1_conv_result),
    //     .data_valid_in      (l1_conv_result_valid),

    //     .data_out           (l1_pool_result),
    //     .data_idx_out       (),
    //     .data_valid_out     (l1_pool_result_valid)
    // );

    relu_layer
    #(
        .APPROX_MODE        (0),
        .DATA_WIDTH         (SUM_WIDTH),
        .NUM_FM             (L1_NUM_OFM)
    )
    u_relu_layer_l1
    (
        // .data_bus_in        (l1_pool_result),
        // .data_bus_valid_in  (l1_pool_result_valid),
        .data_bus_in        (l1_conv_result),
        .data_bus_valid_in  (l1_conv_result_is_data_valid_bus),
        .data_bus_row_valid_in
                            (l1_conv_result_valid),

        .data_bus_out       (l1_relu_result),
        .data_bus_valid_out (l1_relu_result_is_data_valid),
        .data_bus_row_valid_out
                            (l1_relu_result_row_valid)
    );


    quant_dequant_activation_layer
    #(
        .IN_DATA_WIDTH      (SUM_WIDTH),
        .OFM_COUNT          (L1_NUM_OFM),
        .LAYER_IDX          (0),
        .MODE               ("int"),
        .SCALE_FACTOR_A     (L1_SCALE_FACTOR_A),
        .SCALE_FACTOR_B     (L1_SCALE_FACTOR_B)
    )
    u_quant_dequant_layer_l1
    (
        .clk                (clk),
        .reset              (reset),

        .scale_factor_a_in  (),
        .scale_factor_b_in  (),

        .pixel_in           (l1_relu_result),
        .pixel_valid_in     (l1_relu_result_is_data_valid),
        .pixel_row_valid_in (l1_relu_result_row_valid),

        .pixel_out          (l1_quant_result_out),
        .pixel_valid_out    (l1_quant_is_data_valid),
        .pixel_row_valid_out(l1_quant_out_row_valid)
    );

    pseudo_max_pool_layer
    #(
        .CONV_IM_DIM         (L1_CONV_IMAGE_WIDTH),
        .NUM_FM              (L1_NUM_OFM)

    )
    u_pseudo_max_pool_layer_l1
    (
        .clk                 (clk),
        .reset               (reset),

        //RX iface
        .rx_data_bus_in      (l1_quant_result_out),
        .rx_data_valid_bus_in(l1_quant_is_data_valid),
        .rx_row_valid_in     (l1_quant_out_row_valid),
        
        //TX interface
        .tx_data_bus_out     (l1_out),
        .tx_data_valid_out   (l1_out_valid)
    );

//***************************************************
// Layer 2
//***************************************************

    line_buffer_reg
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .IMAGE_WIDTH        (L2_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L2_IMAGE_WIDTH),
        .NUM_IFM            (L2_NUM_IFM),
        .APPROX_MODE        (1),
        .CONV_KERNEL_DIM    (L2_CONV_DIM)
    )
    u_line_buff_l2
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        .pix_bus_in         (l1_out),
        // .pix_bus_is_data_valid_in
        //                     (l1_out_is_data_valid),
        .pix_bus_valid_in   (l1_out_valid),

        //read port
        .line_out           (),
        .line_valid_out     (),

        .window_out         (l2_window_approx),
        .window_valid_out   (l2_window_approx_valid),
        .window_buff_valid_out
                            ()
    );

    conv_layer
    #(
//        .DATA_WIDTH         (DATA_WIDTH),
        .WEIGHT_WIDTH       (3),
        .BIT_SHIFT_MODE     (1),
        .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
        .LAYER_IDX          (1),
        .OFM_COUNT          (L2_NUM_OFM),
        .IFM_COUNT          (L2_NUM_IFM),
        .USE_DSP            (0),
        .WINDOW_SEL_MODE    (0),
        .MAX_POOL_NEIGH_DIM_WIDTH
                            (L2_MAX_POOL_NEIGH_DIM_WIDTH),
        .PIPELINE_DELAY_STAGES
                            (3)
    )
    u_approx_conv_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .pixel_bus_in       (l2_window_approx),
        .neigh_idx_bus_in   ({(L2_MAX_POOL_NEIGH_DIM_BUS_WIDTH){1'b0}}),
        .no_op_in           ({L2_NO_OP_CMD_BUS_WIDTH{1'b0}}),
        .pixel_valid_in     (l2_window_approx_valid),

        .data_out           (l2_approx_conv_result),
        .data_valid_out     (l2_approx_conv_result_valid)
    );

    max_pool_layer
    #(
        .POOL_DATA_WIDTH    (APPROX_SUM_WIDTH),
        .IMAGE_WIDTH        (L2_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L2_IMAGE_WIDTH),
        .MAX_POOL_DIM       (L2_MAX_POOL_DIM),
        .OFM_COUNT          (L2_NUM_OFM),
        .OFM_ITER_COUNT     (1),
        .DIM_WIDTH          (8),
        .DIM_PREDEFINED     (1),
        .APPROX_MODE        (1)
    )
    u_approx_max_pool_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .data_in            (l2_approx_conv_result),
        .data_valid_in      (l2_approx_conv_result_valid),

        .data_out           (l2_approx_pool_result),
        .data_idx_out       (l2_approx_pool_idx),
        .col_count_out      (l2_approx_pool_col),
        .row_count_out      (),
        .data_valid_out     (l2_approx_pool_valid)
    );

    relu_layer
    #(
        .APPROX_MODE        (1),
        .DATA_WIDTH         (APPROX_SUM_WIDTH),
        .NUM_FM             (L2_NUM_OFM)
    )
    u_approx_relu_layer_l2
    (
        .data_bus_in        (l2_approx_pool_result),
        .data_bus_row_valid_in  
                            (l2_approx_pool_valid),

        .data_bus_out       (l2_approx_relu_result),
        .data_bus_row_valid_out 
                            (l2_approx_relu_valid)
    );


    //relu is asynchronous. Hence pool and relu outputs are in sync. The following assignment combines the outputs from both.
    wire    [L2_APPROX_CONTROLLER_IDX_BUS_WIDTH-1 : 0] l2_approx_controller_in_idx;
    genvar j;
    generate
        for(j=0; j<L2_NUM_OFM; j=j+1) begin    
            assign l2_approx_controller_in_idx[j*L2_APPROX_CONTROLLER_IDX_WIDTH +: L2_APPROX_CONTROLLER_IDX_WIDTH] 
                                = {l2_approx_relu_result[j], l2_approx_pool_idx[j * L2_MAX_POOL_NEIGH_DIM_WIDTH +: L2_MAX_POOL_NEIGH_DIM_WIDTH]};
        end
    endgenerate

    conv_approx_controller
    #(
        .CONV_KERNEL_DIM        (L2_CONV_DIM),
        .CONV_IM_DIM            (L2_CONV_IMAGE_WIDTH),
        .MAX_POOL_DIM           (L2_MAX_POOL_DIM),
        .OFM_ITER_COUNT         (1),
        .DIM_WIDTH              (8),
        .DIM_PREDEFINED         (1),
        .APPROX_MODE            (1),
        .NUM_OFM                (L2_NUM_OFM),
        .LAYER_IDX              (1)
    )
    u_conv_approx_controller_l2
    (
        .clk                    (clk),
        .reset                  (reset),        
        //RX interface
        //MAX pool interface
        // .rx_col_in              (l1_approx_pool_col),
        // .rx_max_pool_idx_in     (l2_approx_pool_idx),
        .rx_max_pool_idx_in     (l2_approx_controller_in_idx),
        .rx_max_pool_idx_valid_in            
                                (l2_approx_pool_valid),
        .rx_window_valid_prev_in(l2_window_prev_valid),

        //TX interface
        .no_op_out              (l2_no_op),
        .no_op_valid_out        (l2_no_op_valid)
    );
    // (
    //     .clk                    (clk),
    //     .reset                  (reset),

    //     //data receive port
    //     .rx_col_in              (l2_approx_pool_col),
    //     .rx_max_pool_idx_in     (l2_approx_pool_idx),
    //     .rx_valid_in            (l2_approx_pool_valid),

    //     //data transmit port
    //     .tx_addr_gen_valid_in   (l2_window_prev_valid),
    //     .no_op_out              (),
    //     .no_op_valid_out        ()
    // );


    
    delay_buffer
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .CONV_KERNEL_DIM    (L2_CONV_DIM),
        .DELAY_STAGES       (4 + 67),
        .NUM_IFM            (L2_NUM_IFM)
    )
    u_delay_buffer_l2
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        .pixel_in           (l1_out),
        .pixel_valid_in     (l1_out_valid),

        //read port
        // .window_out         (l1_pix_bus_delayed),
        // .window_valid_out   (l1_pix_bus_delayed_valid)
        .pixel_out          (l1_out_delayed),
        .pixel_valid_out    (l1_out_delayed_valid)
    );

     line_buffer_reg
    #(
        .DATA_WIDTH         (DATA_WIDTH),
        .IMAGE_WIDTH        (L2_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L2_IMAGE_WIDTH),
        .NUM_IFM            (L2_NUM_IFM),
        .APPROX_MODE        (0),
        .CONV_KERNEL_DIM    (L2_CONV_DIM)
    )
    u_line_buff_delayed_l2
    (
        .clk                (clk),
        .reset              (reset),

        //write port
        .pix_bus_in         (l1_out_delayed),
        .pix_bus_valid_in   (l1_out_delayed_valid),

        //read port
        .line_out           (),
        .line_valid_out     (),

        .window_out         (l2_window),
        .window_valid_out   (l2_window_valid),
        .window_valid_prev_out
                            (l2_window_prev_valid),
        .window_buff_valid_out
                            ()
    );

    // integer i;
    // always@(posedge clk) begin
    //     if(reset) begin
    //         l2_no_op            <= {L2_NUM_OFM{1'b0}};
    //         l2_no_op_reg        <= {L2_NUM_OFM{1'b0}};

    //         l2_no_op_valid      <= 1'b0;
    //         l2_no_op_valid_reg  <= 1'b0;
    //     end
    //     else begin
    //         if(l2_approx_relu_valid) begin
    //             for(i=0; i<L2_NUM_OFM; i=i+1) begin
    //                 l2_no_op[i] <= ~l2_approx_relu_result[i] & l2_approx_relu_valid;
    //                 // l2_no_op[i] <= 1'b0;
    //             end
    //         end 
    //         l2_no_op_reg        <= l2_no_op;

    //         l2_no_op_valid      <= l2_approx_relu_valid;
    //         l2_no_op_valid_reg  <= l2_no_op_valid;
    //     end 
    // end

    // wire                        l2_no_op_valid_delayed;
    // wire    [L2_NUM_OFM-1 : 0]  l2_no_op_delayed;

    // shift_reg
    // #(
    //     .CLOCK_CYCLES   (66),
    //     .DATA_WIDTH     (1 + L2_NUM_OFM)    //+2 for valid,last
    // )
    // u_no_op_shift_reg_l2
    // (
    //     .clk            (clk),

    //     .enable         (1'b1),
    //     .data_in        ({l2_no_op_valid_reg, l2_no_op_reg}),
    //     .data_out       ({l2_no_op_valid_delayed, l2_no_op_delayed})
    // );

    conv_layer
    #(
//        .DATA_WIDTH         (DATA_WIDTH),
        .WEIGHT_WIDTH       (WEIGHT_WIDTH),
        .BIT_SHIFT_MODE     (0),
        .CONV_KERNEL_DIM    (L2_CONV_DIM),
        .LAYER_IDX          (1),
        .OFM_COUNT          (L2_NUM_OFM),
        .IFM_COUNT          (L2_NUM_IFM),
        .USE_DSP            (0),
        .WINDOW_SEL_MODE    (0),
        .MAX_POOL_NEIGH_DIM_WIDTH
                            (L2_MAX_POOL_NEIGH_DIM_WIDTH),
        .PIPELINE_DELAY_STAGES
                            (3),
        .CONV_IM_DIM        (L2_CONV_IMAGE_WIDTH)
    )
    u_conv_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .pixel_bus_in       (l2_window),
        .neigh_idx_bus_in   ({(L2_MAX_POOL_NEIGH_DIM_BUS_WIDTH){1'b0}}),
        .no_op_in           (l2_no_op),
        // .no_op_in           ({L2_NUM_OFM{1'b0}}),
        // .no_op_in           (l2_no_op_reg),
        // .no_op_in           (l2_no_op_delayed),
        .pixel_valid_in     (l2_window_valid),

        .data_out           (l2_conv_result),
        .data_valid_out     (l2_conv_result_valid),
        .data_is_valid_out  (l2_conv_result_is_data_valid_bus),
        .data_odd_row_valid_out
                            (l2_conv_result_odd_row_valid)
    );

    relu_layer
    #(
        .APPROX_MODE        (0),
        .DATA_WIDTH         (SUM_WIDTH),
        .NUM_FM             (L2_NUM_OFM)
    )
    u_relu_layer_l2
    (
        .data_bus_in        (l2_conv_result),
        .data_bus_valid_in  (l2_conv_result_is_data_valid_bus),
        .data_bus_row_valid_in
                            (l2_conv_result_valid),

        .data_bus_out       (l2_relu_result),
        .data_bus_valid_out (l2_relu_result_is_data_valid),
        .data_bus_row_valid_out 
                            (l2_relu_result_valid)
    );

    /* eliminate actual max pool
    max_pool_layer
    #(
        .POOL_DATA_WIDTH    (SUM_WIDTH),
        .IMAGE_WIDTH        (L2_IMAGE_WIDTH),
        .IMAGE_HEIGHT       (L2_IMAGE_WIDTH),
        .MAX_POOL_DIM       (L2_MAX_POOL_DIM),
        .OFM_COUNT          (L2_NUM_OFM),
        .OFM_ITER_COUNT     (1),
        .DIM_WIDTH          (8),
        .DIM_PREDEFINED     (1),
        .APPROX_MODE        (0)
    )
    u_max_pool_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .data_in            (l2_relu_result),
        .data_valid_in      (l2_relu_result_valid),

        .data_out           (l2_pool_result),
        .data_valid_out     (l2_pool_result_valid)
    );
    */

    quant_dequant_activation_layer
    #(
        .IN_DATA_WIDTH      (SUM_WIDTH),
        .OFM_COUNT          (L2_NUM_OFM),
        .LAYER_IDX          (1),
        .MODE               ("int"),
        .SCALE_FACTOR_A     (L2_SCALE_FACTOR_A),
        .SCALE_FACTOR_B     (L2_SCALE_FACTOR_B)
    )
    u_quant_dequant_layer_l2
    (
        .clk                (clk),
        .reset              (reset),

        .scale_factor_a_in  (),
        .scale_factor_b_in  (),

        .pixel_in           (l2_relu_result),
        .pixel_valid_in     (l2_relu_result_is_data_valid),
        .pixel_row_valid_in (l2_relu_result_valid),

        .pixel_out          (l2_quant_result_out),
        .pixel_valid_out    (l2_quant_is_data_valid),
        .pixel_row_valid_out(l2_quant_out_row_valid)
    );

    pseudo_max_pool_layer
    #(
        .CONV_IM_DIM         (L2_CONV_IMAGE_WIDTH),
        .NUM_FM              (L2_NUM_OFM)
    )
    u_pseudo_max_pool_layer_l2
    (
        .clk                 (clk),
        .reset               (reset),

        //RX iface
        .rx_data_bus_in      (l2_quant_result_out),
        .rx_data_valid_bus_in(l2_quant_is_data_valid),
        .rx_row_valid_in     (l2_quant_out_row_valid),
        
        //TX interface
        .tx_data_bus_out     (l2_out),
        .tx_data_valid_out   (l2_out_valid)
    );

    assign data_out         = l2_out;
    assign data_valid_out   = l2_out_valid;
    
endmodule
