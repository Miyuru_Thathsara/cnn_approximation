`timescale 1ns / 1ps
module conv_elem
    (
        clk,
        reset,

        pixel_bus_in,
        weight_bus_in,
        pixel_valid_in,

        pixel_out,
        pixel_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   `include "common/util_funcs.v"
   `include "params/weight_params.sv"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                           BIT_SHIFT_MODE          = 0;
    parameter                                                           DATA_WIDTH              = 8;
    parameter                                                           WEIGHT_WIDTH            = (BIT_SHIFT_MODE) ? 3 : DATA_WIDTH;
    parameter                                                           CONV_KERNEL_DIM         = 3;
    parameter                                                           USE_DSP                 = 0;
    parameter                                                           LAYER_IDX               = 0;
    parameter                                                           WEIGHT_INTERNAL         = 0;
    parameter                                                           WEIGHT_KERNEL_IDX       = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                          CONV_KERNEL_NEIGH_DIM   = CONV_KERNEL_DIM * CONV_KERNEL_DIM;
    localparam                                                          CONV_KERNEL_DATA_WIDTH  = CONV_KERNEL_NEIGH_DIM * DATA_WIDTH;
    localparam                                                          MULT_RES_DATA_WIDTH     = DATA_WIDTH + WEIGHT_WIDTH;
    localparam                                                          CONV_OUT_DATA_WIDTH     = MULT_RES_DATA_WIDTH + count2width(CONV_KERNEL_NEIGH_DIM);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                               clk;
    input                                                               reset;

    input signed   [CONV_KERNEL_DATA_WIDTH-1 : 0]                       pixel_bus_in;
    input signed   [CONV_KERNEL_NEIGH_DIM * DATA_WIDTH-1:0]             weight_bus_in;
    input                                                               pixel_valid_in;

    output signed  [CONV_OUT_DATA_WIDTH-1 : 0]                          pixel_out;
    output signed                                                       pixel_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    // wire signed    [DATA_WIDTH-1:0]                                     pixel_bus_packed   [CONV_KERNEL_NEIGH_DIM -1 : 0];
    wire           [CONV_KERNEL_NEIGH_DIM * WEIGHT_WIDTH-1:0]           internal_weight_bus;
    wire signed    [CONV_KERNEL_NEIGH_DIM * WEIGHT_WIDTH-1:0]           weight_bus;
    // wire signed    [WEIGHT_WIDTH-1:0]                                   weight_bus_packed   [CONV_KERNEL_NEIGH_DIM -1 : 0];
    
    reg                                                                 pixel_valid_in_delayed;
    wire signed    [MULT_RES_DATA_WIDTH-1 : 0]                          mult_result_bus [CONV_KERNEL_NEIGH_DIM-1 : 0];
    wire signed    [CONV_KERNEL_NEIGH_DIM * MULT_RES_DATA_WIDTH-1 : 0]  mult_result_bus_expanded;
    wire signed    [CONV_OUT_DATA_WIDTH-1 : 0]                          conv_result;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    
    assign weight_bus = (WEIGHT_INTERNAL) ? internal_weight_bus : weight_bus_in;
    genvar i;
    generate
        for(i=0;i<CONV_KERNEL_NEIGH_DIM;i=i+1) begin
            assign internal_weight_bus[i*WEIGHT_WIDTH +: WEIGHT_WIDTH] =    (LAYER_IDX == 0) ? 
                                                                                (BIT_SHIFT_MODE == 1) ? weight_array_l1_approx[WEIGHT_KERNEL_IDX + i] : weight_array_l1[WEIGHT_KERNEL_IDX + i] : 
                                                                            (LAYER_IDX == 1) ? 
                                                                                (BIT_SHIFT_MODE == 1) ? weight_array_l2_approx[WEIGHT_KERNEL_IDX + i] : weight_array_l2[WEIGHT_KERNEL_IDX + i] :
                                                                            {(WEIGHT_WIDTH) {1'b0}};
            // assign weight_bus_packed[i] = weight_bus[i * WEIGHT_WIDTH +: WEIGHT_WIDTH];
            // assign pixel_bus_packed[i]  = pixel_bus_in[i * DATA_WIDTH +: DATA_WIDTH];
            if(BIT_SHIFT_MODE) begin
                shift_8
                #(
                    .DATA_WIDTH     (DATA_WIDTH),
                    .WEIGHT_WIDTH   (WEIGHT_WIDTH)
                )
                u_shift_8
                (
                    .pixel_in   (pixel_bus_in    [i*DATA_WIDTH +: DATA_WIDTH]),
                    .weight_in  (weight_bus      [i*WEIGHT_WIDTH +: WEIGHT_WIDTH]),
                    .valid_in   (pixel_valid_in),

                    .result_out (mult_result_bus [i]),
                    .result_valid_out
                                ()
                );
                always@(*) begin
                    pixel_valid_in_delayed  = pixel_valid_in;
                end
            end
            else begin
                if(USE_DSP) begin
                   mult8_basic 
                   u_mult8_basic 
                   (
                       .CLK    (clk),  
                       .CEM    (pixel_valid_in),  
                       .SCLR   (reset),  
                       .A      (pixel_bus_in    [i*DATA_WIDTH +: DATA_WIDTH]),
                       .B      (weight_bus      [i*WEIGHT_WIDTH +: WEIGHT_WIDTH]),
                       .P      (mult_result_bus [i])
                    );
                    always@(posedge clk) begin
                        if(reset) begin
                            pixel_valid_in_delayed  <= 1'b0;
                        end
                        else begin
                            pixel_valid_in_delayed  <= pixel_valid_in;
                        end
                    end
                end
                else begin
                    mult8_lut
                    u_mult8_lut
                     (
                         .clk               (clk),
                         .reset             (reset),
                 
                         .pixel_in          (pixel_bus_in    [i*DATA_WIDTH +: DATA_WIDTH]),
                         .weight_in         (weight_bus      [i*WEIGHT_WIDTH +: WEIGHT_WIDTH]),      
                         .valid_in          (pixel_valid_in),
                     
                         .result_out        (mult_result_bus [i]),     
                         .result_valid_out  ()
                     );
                     always@(posedge clk) begin
                        if(reset) begin
                            pixel_valid_in_delayed  <= 1'b0;
                        end
                        else begin
                            pixel_valid_in_delayed  <= pixel_valid_in;
                        end
                    end
                end
            end
            assign mult_result_bus_expanded[i * MULT_RES_DATA_WIDTH +: MULT_RES_DATA_WIDTH] = mult_result_bus[i];
        end            
    endgenerate
    
    

    generate
        if(CONV_KERNEL_DIM == 3) begin
            adder_tree_9
            #(
                .DATA_WIDTH     (MULT_RES_DATA_WIDTH)
            )
            u_adder_tree_9
            (
                .clk            (clk),
                .reset          (reset),

                .data_in        (mult_result_bus_expanded),
                .data_valid_in  (pixel_valid_in_delayed),

                .data_out       (conv_result),
                .data_valid_out (pixel_valid_out)
            );
        end
        else if(CONV_KERNEL_DIM == 5) begin
            adder_tree_25
            #(
                .DATA_WIDTH     (MULT_RES_DATA_WIDTH)
            )
            u_adder_tree_25
            (
                .clk            (clk),
                .reset          (reset),

                .data_in        (mult_result_bus_expanded),
                .data_valid_in  (pixel_valid_in_delayed),

                .data_out       (conv_result),
                .data_valid_out (pixel_valid_out)
            );
        end
    endgenerate
    assign pixel_out = conv_result;
    
endmodule
