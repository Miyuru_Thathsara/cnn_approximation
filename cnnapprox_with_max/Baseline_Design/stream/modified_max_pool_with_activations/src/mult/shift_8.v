`timescale 1ns / 1ps
module shift_8
    (
        // clk,
        // reset,

        pixel_in,
        weight_in,
        valid_in,
    
        result_out,
        result_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                           DATA_WIDTH        = 8;
    parameter                                                           WEIGHT_WIDTH      = 8;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam								                            RESULT_WIDTH      = DATA_WIDTH + WEIGHT_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input signed  [DATA_WIDTH-1 : 0]                        		    pixel_in;
    input signed  [WEIGHT_WIDTH-1:0]              			            weight_in;
    input                                                               valid_in;

    output reg signed [RESULT_WIDTH-1 : 0]                              result_out;
    output                                                              result_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers

//---------------------------------------------------------------------------------------------------------------------
    wire signed  [WEIGHT_WIDTH-1:0]              			            weight_comp;     
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    // always@(posedge clk) begin
    //     if(reset) begin
    //        result_out	        <= {RESULT_WIDTH{1'b0}};
    //        result_valid_out 	<= 1'b0;
    //     end
    //     else begin
    //         result_valid_out	<= valid_in;
    //         if(valid_in) begin
    //             result_out	    <= pixel_in << weight_in;
    //         end
    //     end
    // end
    
    assign weight_comp = -weight_in;
    always@(*) begin
        if(valid_in) begin
            if(|weight_in) begin
                if(weight_in[WEIGHT_WIDTH-1]) begin
                    result_out = -(pixel_in << (weight_comp-1));
                end
                else begin
                    result_out = pixel_in << (weight_in-1);
                end
            end
            else begin
                result_out  = {RESULT_WIDTH{1'b0}};
            end
        end
        else begin
            result_out  = {RESULT_WIDTH{1'b0}};
        end
    end
    
    assign result_valid_out = valid_in;

endmodule

