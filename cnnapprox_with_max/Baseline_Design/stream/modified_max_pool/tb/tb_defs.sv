`define SNOOP_APPROX_STATS_L1(OFM_IDX) \
    conv_positive[OFM_IDX]   = (u_conv_top.u_conv_layer_l1.genblk1[OFM_IDX].u_stream_conv_out_ch.conv_ofm_with_bias_dout_se >= 0) ? 1'b1 : 1'b0; \
    approx_positive[OFM_IDX] = (u_conv_top.u_conv_layer_l1.genblk1[OFM_IDX].u_stream_conv_out_ch.is_result_zero_delayed) ? 1'b0 : 1'b1; \
    // $di  splay("L1 Conv Positive : %d Approx Positive : %d", conv_positive[OFM_IDX], approx_positive[OFM_IDX]); \
    if(conv_positive[OFM_IDX] == 1 && approx_positive[OFM_IDX] == 0) begin \
        false_negatives[OFM_IDX]++; \
    end \
    else if(conv_positive[OFM_IDX] == 0 && approx_positive[OFM_IDX] == 1) begin \
        false_positives[OFM_IDX]++; \
    end\
    if(conv_positive[OFM_IDX] == 0) begin\
        negatives[OFM_IDX]++;\
    end\
    if(approx_positive[OFM_IDX] == 0) begin\
        no_op[OFM_IDX]++;\
    end

`define SNOOP_APPROX_STATS_L2(OFM_IDX) \
    conv_positive[OFM_IDX]   = (u_conv_top.u_conv_layer_l2.genblk1[OFM_IDX].u_stream_conv_out_ch.conv_ofm_with_bias_dout_se >= 0) ? 1'b1 : 1'b0; \
    approx_positive[OFM_IDX] = (u_conv_top.u_conv_layer_l2.genblk1[OFM_IDX].u_stream_conv_out_ch.is_result_zero_delayed) ? 1'b0 : 1'b1; \
    // $display("L2 Conv Positive : %d Approx Positive : %d", conv_positive[OFM_IDX], approx_positive[OFM_IDX]); \
    if(conv_positive[OFM_IDX] == 1 && approx_positive[OFM_IDX] == 0) begin \
        false_negatives[OFM_IDX]++; \
    end \
    else if(conv_positive[OFM_IDX] == 0 && approx_positive[OFM_IDX] == 1) begin \
        false_positives[OFM_IDX]++; \
    end\
    if(conv_positive[OFM_IDX] == 0) begin\
        negatives[OFM_IDX]++;\
    end\
    if(approx_positive[OFM_IDX] == 0) begin\
        no_op[OFM_IDX]++;\
    end

// `define L1_SNOOP_CALLS() \
//     `SNOOP_APPROX_STATS_L1(0)      \
//     `SNOOP_APPROX_STATS_L1(1)      \
//     `SNOOP_APPROX_STATS_L1(2)      \       
//     `SNOOP_APPROX_STATS_L1(3)      \       
//     `SNOOP_APPROX_STATS_L1(4)      \       
//     `SNOOP_APPROX_STATS_L1(5)      \       
//     `SNOOP_APPROX_STATS_L1(6)      \       
//     `SNOOP_APPROX_STATS_L1(7)      \       
//     `SNOOP_APPROX_STATS_L1(8)      \       
//     `SNOOP_APPROX_STATS_L1(9)      \       
//     `SNOOP_APPROX_STATS_L1(10)     \        
//     `SNOOP_APPROX_STATS_L1(11)     \        
//     `SNOOP_APPROX_STATS_L1(12)     \        
//     `SNOOP_APPROX_STATS_L1(13)     \        
//     `SNOOP_APPROX_STATS_L1(14)     \        
//     `SNOOP_APPROX_STATS_L1(15)     \        
//     `SNOOP_APPROX_STATS_L1(16)     \        
//     `SNOOP_APPROX_STATS_L1(17)     \        
//     `SNOOP_APPROX_STATS_L1(18)     \       
//     `SNOOP_APPROX_STATS_L1(19)

// // `define L2_SNOOP_CALLS()\
//     `SNOOP_APPROX_STATS_L2(0)                                               
//     `SNOOP_APPROX_STATS_L2(1)                                       
//     `SNOOP_APPROX_STATS_L2(2)                                       
//     `SNOOP_APPROX_STATS_L2(3)                                       
//     `SNOOP_APPROX_STATS_L2(4)                                       
//     `SNOOP_APPROX_STATS_L2(5)                                       
//     `SNOOP_APPROX_STATS_L2(6)                                       
//     `SNOOP_APPROX_STATS_L2(7)                                       
//     `SNOOP_APPROX_STATS_L2(8)                                       
//     `SNOOP_APPROX_STATS_L2(9)                                       
//     `SNOOP_APPROX_STATS_L2(10)      
//     `SNOOP_APPROX_STATS_L2(11)                      
//     `SNOOP_APPROX_STATS_L2(12)                      
//     `SNOOP_APPROX_STATS_L2(13)                      
//     `SNOOP_APPROX_STATS_L2(14)                      
//     `SNOOP_APPROX_STATS_L2(15)                      
//     `SNOOP_APPROX_STATS_L2(16)                      
//     `SNOOP_APPROX_STATS_L2(17)                      
//     `SNOOP_APPROX_STATS_L2(18)                      
//     `SNOOP_APPROX_STATS_L2(19)                      
//     `SNOOP_APPROX_STATS_L2(20)                         
//     `SNOOP_APPROX_STATS_L2(21)                      
//     `SNOOP_APPROX_STATS_L2(22)                      
//     `SNOOP_APPROX_STATS_L2(23)                      
//     `SNOOP_APPROX_STATS_L2(24)                      
//     `SNOOP_APPROX_STATS_L2(25)                      
//     `SNOOP_APPROX_STATS_L2(26)                      
//     `SNOOP_APPROX_STATS_L2(27)                      
//     `SNOOP_APPROX_STATS_L2(28)                      
//     `SNOOP_APPROX_STATS_L2(29)                      
//     `SNOOP_APPROX_STATS_L2(30)                      
//     `SNOOP_APPROX_STATS_L2(31)                      
//     `SNOOP_APPROX_STATS_L2(32)                      
//     `SNOOP_APPROX_STATS_L2(33)                      
//     `SNOOP_APPROX_STATS_L2(34)                      
//     `SNOOP_APPROX_STATS_L2(35)                      
//     `SNOOP_APPROX_STATS_L2(36)                      
//     `SNOOP_APPROX_STATS_L2(37)                      
//     `SNOOP_APPROX_STATS_L2(38)                      
//     `SNOOP_APPROX_STATS_L2(39)                      
//     `SNOOP_APPROX_STATS_L2(40)                         
//     `SNOOP_APPROX_STATS_L2(41)                      
//     `SNOOP_APPROX_STATS_L2(42)                      
//     `SNOOP_APPROX_STATS_L2(43)                      
//     `SNOOP_APPROX_STATS_L2(44)                      
//     `SNOOP_APPROX_STATS_L2(45)                      
//     `SNOOP_APPROX_STATS_L2(46)                      
//     `SNOOP_APPROX_STATS_L2(47)                      
//     `SNOOP_APPROX_STATS_L2(48)                      
//     `SNOOP_APPROX_STATS_L2(49)                      
//     `SNOOP_APPROX_STATS_L2(50)                     