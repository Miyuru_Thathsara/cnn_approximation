module bram_1w_2r(
    clk,
    reset,

    bram_port_a_wrt_data_in,
    bram_port_a_wrt_addr_in,
    bram_port_a_wrt_en_in,

    bram_port_a_rd_addr_in,
    bram_port_a_rd_sel_in,
    bram_port_a_rd_data_out,

    bram_port_b_rd_addr_in,
    bram_port_b_rd_data_out
);

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   BRAM_DATA_WIDTH = 8;
    parameter                                                   BRAM_ADDR_WIDTH = 8;
    parameter                                                   BRAM_RD_DELAY   = 2;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [BRAM_DATA_WIDTH-1 : 0]                         bram_port_a_wrt_data_in;
    input       [BRAM_ADDR_WIDTH-1 : 0]                         bram_port_a_wrt_addr_in;
    input                                                       bram_port_a_wrt_en_in;

    input       [BRAM_ADDR_WIDTH-1 : 0]                         bram_port_a_rd_addr_in;
    input                                                       bram_port_a_rd_sel_in;
    output      [BRAM_DATA_WIDTH-1 : 0]                         bram_port_a_rd_data_out;

    input       [BRAM_ADDR_WIDTH-1 : 0]                         bram_port_b_rd_addr_in;
    output      [BRAM_DATA_WIDTH-1 : 0]                         bram_port_b_rd_data_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire                                                        bram_port_a_rd_sel_delayed;

    wire        [BRAM_ADDR_WIDTH-1 : 0]                         bram_rd_addr;
    wire        [BRAM_DATA_WIDTH-1 : 0]                         bram_rd_data;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    assign bram_rd_addr = (bram_port_a_rd_sel_in == 1'b1) ? bram_port_a_rd_addr_in : bram_port_b_rd_addr_in;
    assign {bram_port_a_rd_data_out, bram_port_b_rd_data_out} = (bram_port_a_rd_sel_delayed) ? { bram_rd_data, {BRAM_DATA_WIDTH{1'b0}} } : 
                                                                                               { {BRAM_DATA_WIDTH{1'b0}}, bram_rd_data }; 
    
    bram_1w_2r_ip
    u_bram_1w_2r_ip
    (
        .clka               (clk),
        .ena                (bram_port_a_wrt_en_in),
        .wea                (bram_port_a_wrt_en_in),
        .addra              (bram_port_a_wrt_addr_in),
        .dina               (bram_port_a_wrt_data_in),
      
        .clkb               (clk),
        // .web                (1'b0),
        .addrb              (bram_rd_addr),
        .doutb              (bram_rd_data)
        // .dinb               ({BRAM_DATA_WIDTH{1'b0}})
    );

    
    //  shift_reg #(
    //     .CLOCK_CYCLES   (BRAM_RD_DELAY),
    //     .DATA_WIDTH     (1'b1)    //data+wr_en
    // )
    // u_pixel_shift_reg_b (
    //     .clk            (clk),
    //     .enable         (1'b1),
    //     .data_in        (bram_port_a_rd_sel_in),
    //     .data_out       (bram_port_a_rd_sel_delayed)
    // );
    assign bram_port_a_rd_sel_delayed = bram_port_a_rd_sel_in;

endmodule