`timescale 1ns / 1ps

module neigh_creator_bus
    (
        clk,
        reset,

        line_in,
        line_valid_in,
        // neigh_idx_in,
        neigh_idx_valid_in,
        

        neigh_out,
        neigh_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
   parameter                                                    CHANNEL_COUNT            = 20;
   parameter                                                    DATA_WIDTH               = 8;
   parameter                                                    CONV_KERNEL_DIM          = 5;
   parameter                                                    MAX_POOL_NEIGH_DIM_WIDTH = 3;
   parameter                                                    IM_COLS                  = 28;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
   localparam                                                   APPROX_KERNEL_DIM = CONV_KERNEL_DIM + 1; 
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [DATA_WIDTH * APPROX_KERNEL_DIM * CHANNEL_COUNT-1 : 0]   
                                                                line_in;
    input                                                       line_valid_in;
    // input       [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                neigh_idx_in;
    input                                                       neigh_idx_valid_in;

    output      [DATA_WIDTH * APPROX_KERNEL_DIM * APPROX_KERNEL_DIM * CHANNEL_COUNT -1 : 0]
                                                                neigh_out;
    output                                                      neigh_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [CHANNEL_COUNT-1 : 0]                           valid_bus;
    wire        [APPROX_KERNEL_DIM * APPROX_KERNEL_DIM * DATA_WIDTH * CHANNEL_COUNT -1 : 0]
                                                                stiched_neigh;
    wire        [CHANNEL_COUNT-1 : 0]                           stiched_neigh_valid;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    genvar i;
    generate
        for(i=0;i<CHANNEL_COUNT;i=i+1) begin
            neigh_creator
            #(
                .DATA_WIDTH         (DATA_WIDTH),
                .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
                .IM_COLS            (IM_COLS)
            )
            u_neigh_creator
            (
                .clk                (clk),
                .reset              (reset),

                .line_in            (line_in                [i*(DATA_WIDTH * APPROX_KERNEL_DIM) +: (DATA_WIDTH * APPROX_KERNEL_DIM)]),
                .line_valid_in      (line_valid_in),

                .neigh_out          (stiched_neigh          [i * (APPROX_KERNEL_DIM * APPROX_KERNEL_DIM * DATA_WIDTH) +: (APPROX_KERNEL_DIM * APPROX_KERNEL_DIM * DATA_WIDTH)]),
                .neigh_valid_out    (stiched_neigh_valid    [i])
            );

            // neigh_mux
            // #(
            //     .DATA_WIDTH         (DATA_WIDTH),
            //     .CONV_KERNEL_DIM    (CONV_KERNEL_DIM),
            //     .MAX_POOL_NEIGH_DIM_WIDTH   
            //                         (MAX_POOL_NEIGH_DIM_WIDTH)
            // )
            // u_neigh_mux
            // (
            //     .clk                (clk),
            //     .reset              (reset),

            //     .neigh_in           (stiched_neigh          [i * (APPROX_KERNEL_DIM * APPROX_KERNEL_DIM * DATA_WIDTH) +: (APPROX_KERNEL_DIM * APPROX_KERNEL_DIM * DATA_WIDTH)]),
            //     .neigh_idx_in       (neigh_idx_in),
            //     .neigh_idx_valid_in (neigh_idx_valid_in),
                

            //     .neigh_out          (neigh_out      [i * (DATA_WIDTH * CONV_KERNEL_DIM * CONV_KERNEL_DIM) +: (DATA_WIDTH * CONV_KERNEL_DIM * CONV_KERNEL_DIM)]),
            //     .neigh_valid_out    (valid_bus      [i])
            // );
        end
    endgenerate

    assign neigh_out       = stiched_neigh;
    assign neigh_valid_out = neigh_idx_valid_in;

endmodule