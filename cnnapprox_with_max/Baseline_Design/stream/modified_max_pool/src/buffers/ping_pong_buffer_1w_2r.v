module ping_ping_buff_1w_2r(
    clk,
    reset,

    bram_port_a_wrt_data_in,
    bram_port_a_wrt_addr_in,
    bram_port_a_wrt_en_in,

    bram_port_a_rd_addr_in,
    bram_port_a_rd_sel_in,
    bram_port_a_rd_data_out,

    bram_port_b_rd_addr_in,
    bram_port_b_rd_sel_in,
    bram_port_b_rd_data_out
);

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   BRAM_DATA_WIDTH = 8;
    parameter                                                   BRAM_ADDR_WIDTH = 8;
    parameter                                                   BRAM_RD_DELAY   = 2;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [BRAM_DATA_WIDTH-1 : 0]                         bram_port_a_wrt_data_in;
    input       [BRAM_ADDR_WIDTH-1 : 0]                         bram_port_a_wrt_addr_in;
    input                                                       bram_port_a_wrt_en_in;

    input       [BRAM_ADDR_WIDTH-1 : 0]                         bram_port_a_rd_addr_in;
    input                                                       bram_port_a_rd_sel_in;
    output      [BRAM_DATA_WIDTH-1 : 0]                         bram_port_a_rd_data_out;

    input       [BRAM_ADDR_WIDTH-1 : 0]                         bram_port_b_rd_addr_in;
    input                                                       bram_port_b_rd_sel_in;
    output      [BRAM_DATA_WIDTH-1 : 0]                         bram_port_b_rd_data_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    wire        [1 : 0]                                         bram_wrt_en_bus;
    wire        [1 : 0]                                         bram_port_a_rd_sel_bus;
    wire        [1 : 0]                                         bram_port_a_rd_sel_bus_delayed;

    wire        [1 : 0]                                         bram_port_b_rd_sel_bus;
    wire        [1 : 0]                                         bram_port_b_rd_sel_bus_delayed;

    wire        [2*BRAM_DATA_WIDTH-1 : 0]                       bram_port_a_rd_data_bus;
    wire        [2*BRAM_DATA_WIDTH-1 : 0]                       bram_port_b_rd_data_bus;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    assign bram_wrt_en_bus          = (bram_port_a_wrt_addr_in[0] == 1'b1 && bram_port_a_wrt_en_in == 1'b1) ? 2'b10 : 
                                      (bram_port_a_wrt_addr_in[0] == 1'b0 && bram_port_a_wrt_en_in == 1'b1) ? 2'b01 : 2'b00;

    assign bram_port_a_rd_sel_bus   = (bram_port_a_rd_addr_in[0]  == 1'b1 && bram_port_a_rd_sel_in == 1'b1) ? 2'b10 : 
                                      (bram_port_a_rd_addr_in[0]  == 1'b0 && bram_port_a_rd_sel_in == 1'b1) ? 2'b01 :2'b00;

    assign bram_port_b_rd_sel_bus   = (bram_port_b_rd_addr_in[0]  == 1'b1 && bram_port_b_rd_sel_in == 1'b1) ? 2'b10 : 
                                      (bram_port_b_rd_addr_in[0]  == 1'b0 && bram_port_b_rd_sel_in == 1'b1) ? 2'b01 :2'b00;

    assign bram_port_a_rd_data_out  = (bram_port_a_rd_sel_bus_delayed == 2'b01) ? bram_port_a_rd_data_bus[0 +: BRAM_DATA_WIDTH] :
                                      (bram_port_a_rd_sel_bus_delayed == 2'b10) ? bram_port_a_rd_data_bus[BRAM_DATA_WIDTH +: BRAM_DATA_WIDTH] :
                                      {BRAM_DATA_WIDTH{1'b0}};

    assign bram_port_b_rd_data_out  = (bram_port_b_rd_sel_bus_delayed == 2'b01) ? bram_port_b_rd_data_bus[0 +: BRAM_DATA_WIDTH] :
                                      (bram_port_b_rd_sel_bus_delayed == 2'b10) ? bram_port_b_rd_data_bus[BRAM_DATA_WIDTH +: BRAM_DATA_WIDTH] :
                                      {BRAM_DATA_WIDTH{1'b0}};

    bram_1w_2r
    #(
        .BRAM_DATA_WIDTH   (BRAM_DATA_WIDTH),
        .BRAM_ADDR_WIDTH   (BRAM_ADDR_WIDTH-1)
    )
    u_bram_1w_2r_a
    (
        .clk                        (clk),   
        .reset                      (reset),

        .bram_port_a_wrt_data_in    (bram_port_a_wrt_data_in),
        .bram_port_a_wrt_addr_in    (bram_port_a_wrt_addr_in [1 +: (BRAM_ADDR_WIDTH-1)]),
        .bram_port_a_wrt_en_in      (bram_wrt_en_bus         [0]),

        .bram_port_a_rd_addr_in     (bram_port_a_rd_addr_in  [1 +: (BRAM_ADDR_WIDTH-1)]),
        .bram_port_a_rd_sel_in      (bram_port_a_rd_sel_bus  [0]),
        .bram_port_a_rd_data_out    (bram_port_a_rd_data_bus [0 * BRAM_DATA_WIDTH +: BRAM_DATA_WIDTH]),

        .bram_port_b_rd_addr_in     (bram_port_b_rd_addr_in  [1 +: (BRAM_ADDR_WIDTH-1)]),
        .bram_port_b_rd_data_out    (bram_port_b_rd_data_bus [0 * BRAM_DATA_WIDTH +: BRAM_DATA_WIDTH])
    );

    bram_1w_2r
    #(
        .BRAM_DATA_WIDTH   (BRAM_DATA_WIDTH),
        .BRAM_ADDR_WIDTH   (BRAM_ADDR_WIDTH-1)
    )
    u_bram_1w_2r_b
    (
        .clk                        (clk),   
        .reset                      (reset),

        .bram_port_a_wrt_data_in    (bram_port_a_wrt_data_in),
        .bram_port_a_wrt_addr_in    (bram_port_a_wrt_addr_in [1 +: (BRAM_ADDR_WIDTH-1)]),
        .bram_port_a_wrt_en_in      (bram_wrt_en_bus         [1]),

        .bram_port_a_rd_addr_in     (bram_port_a_rd_addr_in  [1 +: (BRAM_ADDR_WIDTH-1)]),
        .bram_port_a_rd_sel_in      (bram_port_a_rd_sel_bus  [1]),
        .bram_port_a_rd_data_out    (bram_port_a_rd_data_bus [1 * BRAM_DATA_WIDTH +: BRAM_DATA_WIDTH]),

        .bram_port_b_rd_addr_in     (bram_port_b_rd_addr_in  [1 +: (BRAM_ADDR_WIDTH-1)]),
        .bram_port_b_rd_data_out    (bram_port_b_rd_data_bus [1 * BRAM_DATA_WIDTH +: BRAM_DATA_WIDTH])
    );

    
    // shift_reg #(
    //     .CLOCK_CYCLES   (BRAM_RD_DELAY),
    //     .DATA_WIDTH     (4)    //data+wr_en
    // )
    // u_pixel_shift_reg_b (
    //     .clk            (clk),
    //     .enable         (1'b1),
    //     .data_in        ({bram_port_a_rd_sel_bus, bram_port_b_rd_sel_bus}),
    //     .data_out       ({bram_port_a_rd_sel_bus_delayed, bram_port_b_rd_sel_bus_delayed})
    // );
    assign {bram_port_a_rd_sel_bus_delayed, bram_port_b_rd_sel_bus_delayed} = {bram_port_a_rd_sel_bus, bram_port_b_rd_sel_bus};

endmodule