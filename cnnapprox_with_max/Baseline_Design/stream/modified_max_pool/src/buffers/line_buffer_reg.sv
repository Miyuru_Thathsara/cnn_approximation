`timescale 1ns / 1ps

module line_buffer_reg
    (
        clk,
        reset,

        //write port
        pix_bus_in,
        pix_bus_valid_in,

        //read port
        line_out,
        line_valid_out,

        window_out,
        window_valid_out,
        window_valid_prev_out,
        window_buff_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
  `include "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   DATA_WIDTH      = 8;
//    parameter                                                   IMAGE_WIDTH     = 28;
//    parameter                                                   IMAGE_HEIGHT    = 28;
    parameter                                                   IMAGE_WIDTH     = 30;
    parameter                                                   IMAGE_HEIGHT    = 30;
    parameter                                                   NUM_IFM         = 64;
    parameter                                                   APPROX_MODE     = 1;
    parameter                                                   CONV_KERNEL_DIM = 3;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  DIM_WIDTH                = count2width(IMAGE_HEIGHT);
    localparam                                                  BLOCK_RAM_DELAY          = 2;
    localparam                                                  WINDOW_DIM               = CONV_KERNEL_DIM * CONV_KERNEL_DIM;
    localparam                                                  LINE_BUFF_IN_DATA_WIDTH  = NUM_IFM * DATA_WIDTH;
    localparam                                                  LINE_BUFF_OUT_DATA_WIDTH = WINDOW_DIM * DATA_WIDTH * NUM_IFM;
    localparam                                                  LINE_OUT_DATA_WIDTH      = CONV_KERNEL_DIM * DATA_WIDTH * NUM_IFM;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    //input port
    input       [LINE_BUFF_IN_DATA_WIDTH-1 : 0]                 pix_bus_in;
    input                                                       pix_bus_valid_in;
    
    //output port
    output      [LINE_OUT_DATA_WIDTH - 1 : 0]                   line_out;
    output                                                      line_valid_out;

    output      [LINE_BUFF_OUT_DATA_WIDTH-1 : 0]                window_out;
    output                                                      window_valid_out; 
    output                                                      window_valid_prev_out;     
    output                                                      window_buff_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg         [DIM_WIDTH-1  : 0]                              col_count;
    reg         [DIM_WIDTH-1  : 0]                              row_count;
    wire        [DIM_WIDTH-1  : 0]                              row_count_delayed;

    reg         [DATA_WIDTH * NUM_IFM-1 : 0]                    line_buff_mem [IMAGE_WIDTH-1 : 0][CONV_KERNEL_DIM-2 : 0];

    wire        [DIM_WIDTH-1  : 0]                              line_buffer_addr;
    wire        [DIM_WIDTH-1  : 0]                              line_buffer_addr_delayed;
    wire        [DIM_WIDTH-1  : 0]                              line_buffer_rd_addr;
    wire        [DIM_WIDTH-1  : 0]                              line_buffer_wrt_addr;


    wire        [LINE_BUFF_IN_DATA_WIDTH-1 : 0]                 pix_bus_reg;
    wire                                                        pix_bus_valid_reg;
    
    wire        [LINE_BUFF_IN_DATA_WIDTH * CONV_KERNEL_DIM  -1 : 0]
                                                                line_buff_din;
    wire        [LINE_BUFF_IN_DATA_WIDTH * CONV_KERNEL_DIM  -1 : 0]
                                                                line_buff_din_rearranged;
    wire        [LINE_BUFF_IN_DATA_WIDTH * (CONV_KERNEL_DIM-1) -1 : 0]
                                                                line_buff_dout;
    wire                                                        line_buff_wrt_en;
    
    // wire        [NUM_IFM-1 : 0]                                 line_out_valid;
    // wire        [NUM_IFM-1 : 0]                                 window_valid; 
    // wire        [NUM_IFM-1 : 0]                                 window_buff_valid;

    wire        [LINE_BUFF_OUT_DATA_WIDTH-1 : 0]                window_extractor_out;
    wire                                                        window_extractor_out_valid;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    always@(posedge clk) begin
        if(reset) begin
            col_count                 <= 8'h0;
            row_count                 <= 8'h0;
        end
        else begin
            if(pix_bus_valid_in) begin
                if(col_count == IMAGE_WIDTH-1) begin
                    col_count         <= 8'h0;
                    if(row_count == IMAGE_HEIGHT-1)begin
                        row_count     <= 8'h0;
                    end
                    else begin
                        row_count     <= row_count + 1'b1;
                    end
                end
                else begin
                    col_count         <= col_count + 1'b1;
                end
            end
        end
    end
    
    //pipeline stage 1 start
    //assign {pad_top, pad_left, pad_bottom, pad_right} = padding_mask_in;
    assign line_buffer_addr = col_count;
    shift_reg
    #(
        .CLOCK_CYCLES   (BLOCK_RAM_DELAY),
        .DATA_WIDTH     (1 + 2*DIM_WIDTH + LINE_BUFF_IN_DATA_WIDTH)    //+2 for valid,last
    )
    u_shift_reg
    (
        .clk            (clk),

        .enable         (1'b1),
        .data_in        ({pix_bus_valid_in, row_count, line_buffer_addr, pix_bus_in}),
        .data_out       ({pix_bus_valid_reg, row_count_delayed, line_buffer_addr_delayed, pix_bus_reg})
    );

    assign line_buffer_rd_addr  = line_buffer_addr;
    // assign line_buffer_wrt_addr = line_buffer_addr_delayed;
    // assign line_buff_wrt_en     = pix_bus_valid_reg;
    // assign line_buff_din        = {pix_bus_reg, line_buff_dout};    //shift op
    assign line_buff_din        = {pix_bus_in, line_buff_dout};    //shift op

    integer i;
    genvar j;
    generate
        for(j=0; j<CONV_KERNEL_DIM-1; j=j+1) begin
            // if(NUM_IFM == 20) begin
            //     line_buffer_mem_20
            //     u_conv_line_buff_20
            //     (
            //         .clka                       (clk),
            //         .wea                        (line_buff_wrt_en),
            //         .ena                        (line_buff_wrt_en),
            //         .addra                      (line_buffer_wrt_addr),
            //         .dina                       (line_buff_din  [(j+1)*LINE_BUFF_IN_DATA_WIDTH +: LINE_BUFF_IN_DATA_WIDTH]),
                    
            //         .clkb                       (clk),
            //         .addrb                      (line_buffer_rd_addr),
            //         .doutb                      (line_buff_dout [j*LINE_BUFF_IN_DATA_WIDTH +: LINE_BUFF_IN_DATA_WIDTH])
            //     );
            // end
            // else begin
            //     line_buffer_mem
            //     u_conv_line_buff
            //     (
            //         .clka                       (clk),
            //         .wea                        (line_buff_wrt_en),
            //         .ena                        (line_buff_wrt_en),
            //         .addra                      (line_buffer_wrt_addr),
            //         .dina                       (line_buff_din  [(j+1)*LINE_BUFF_IN_DATA_WIDTH +: LINE_BUFF_IN_DATA_WIDTH]),
                    
            //         .clkb                       (clk),
            //         .addrb                      (line_buffer_rd_addr),
            //         .doutb                      (line_buff_dout [j*LINE_BUFF_IN_DATA_WIDTH +: LINE_BUFF_IN_DATA_WIDTH])
            //     );
            // end
            assign line_buff_dout [j*LINE_BUFF_IN_DATA_WIDTH +: LINE_BUFF_IN_DATA_WIDTH] = line_buff_mem[line_buffer_rd_addr][j];
            always@(posedge clk) begin
                if(reset)begin
                    for(i=0; i<IMAGE_WIDTH; i++) begin
                        line_buff_mem[i][j]                      <= {(NUM_IFM * DATA_WIDTH){1'b0}};
                    end
                end
                else begin
                    if(pix_bus_valid_in) begin
                        line_buff_mem[line_buffer_rd_addr][j]   <= line_buff_din[(j+1)*LINE_BUFF_IN_DATA_WIDTH +: LINE_BUFF_IN_DATA_WIDTH];
                    end
                end
            end
        end

        // for(i=0; i<NUM_IFM; i=i+1) begin
//        generate
        //if(APPROX_MODE) begin
            window_extractor_approx
            #(
                .DATA_WIDTH                 (DATA_WIDTH),
                .NUM_IFM                    (NUM_IFM),
                .CONV_KERNEL_DIM            (CONV_KERNEL_DIM),
                .DIM_WIDTH                  (DIM_WIDTH)
            )
            u_window_extractor_approx
            (
                .clk                        (clk),
                .reset                      (reset),

                //write port
                // .line_buffer_addr_in        (line_buffer_wrt_addr),
                // .line_buff_data_in          (line_buff_din),
                // .row_count_in               (row_count_delayed),
                // .pix_bus_valid_in           (pix_bus_valid_reg),
                .line_buffer_addr_in        (line_buffer_rd_addr),
                .line_buff_data_in          (line_buff_din),
                .row_count_in               (row_count),
                .pix_bus_valid_in           (pix_bus_valid_in),


                //read port
                .line_out                   (line_out),
                .line_valid_out             (line_out_valid),

                .window_out                 (window_extractor_out),
                .window_valid_out           (window_extractor_out_valid),
                .window_valid_prev_out      (window_valid_prev_out),
                .window_buff_valid_out      ()
            );  
        //end
//        else begin
        //    window_extractor
        //    #(
        //        .DATA_WIDTH                 (DATA_WIDTH),
        //        .NUM_IFM                    (NUM_IFM),
        //        .CONV_KERNEL_DIM            (CONV_KERNEL_DIM),
        //        .DIM_WIDTH                  (DIM_WIDTH)
        
        //    )
        //    u_window_extractor
        //    (
        //        .clk                        (clk),
        //        .reset                      (reset),

        //        //write port
        //        .line_buffer_addr_in        (line_buffer_rd_addr),
        //        // .line_buff_data_in          (line_buff_din      [(i * (DATA_WIDTH * (CONV_KERNEL_DIM-1)) ) +: (DATA_WIDTH * (CONV_KERNEL_DIM-1) )]),
        //        // .line_buff_data_in          (line_buff_din_rearranged      
        //        //                                                 [(i * (DATA_WIDTH * (CONV_KERNEL_DIM)) ) +: (DATA_WIDTH * (CONV_KERNEL_DIM) )]),
        //        .line_buff_data_in          (line_buff_din),
        //        .row_count_in               (row_count),
        //        .pix_bus_valid_in           (pix_bus_valid_reg),

        //        //read port
        //        .line_out                   (line_out),
        //        .line_valid_out             (line_out_valid),

        //        .window_out                 (window_extractor_out),
        //        .window_valid_out           (window_extractor_out_valid),
        //        .window_buff_valid_out      ()
        //    );  
//        end
//        endgenerate
        // end
            line_rearrange
            #(
                .DATA_WIDTH                 (DATA_WIDTH),
                .DIM_SIZE                   (WINDOW_DIM),
                .NUM_IFM                    (NUM_IFM)
            )
            u_line_rearrange
            (
                // clk,
                // reset,

                //write port
                .line_in                    (window_extractor_out),
                .line_valid_in              (window_extractor_out_valid),

                .line_out                   (window_out),
                .line_valid_out             (window_valid_out)
            );
    endgenerate
    
    // assign window_valid_out = window_valid[0];
    // assign line_valid_out   = line_out_valid[0];
    // assign window_buff_valid_out = window_buff_valid[0];
endmodule  
