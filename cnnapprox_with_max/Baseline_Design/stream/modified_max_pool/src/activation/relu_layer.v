`timescale 1ns / 1ps
module relu_layer
    (
        data_bus_in,
        data_bus_valid_in,

        data_bus_out,
        data_bus_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
   
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                           APPROX_MODE       = 1;
    parameter                                                           DATA_WIDTH        = 8;
    parameter                                                           NUM_FM            = 20;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                          OUT_DATA_WIDTH    = (APPROX_MODE) ? 1 : DATA_WIDTH;
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input signed  [DATA_WIDTH * NUM_FM -1 : 0]                         data_bus_in;
    input                                                               data_bus_valid_in;

    output signed [OUT_DATA_WIDTH * NUM_FM -1:0]                        data_bus_out;
    output                                                              data_bus_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers

//---------------------------------------------------------------------------------------------------------------------
    // wire                                                                is_data_positive;                                  
    wire          [NUM_FM-1 : 0]                                        data_bus_valid;
    // wire                                                                is_data_negative;                                  
    // wire                                                                is_data_zero;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    genvar i;
    generate
        for(i=0; i<NUM_FM; i=i+1) begin
            relu
            #(
                .APPROX_MODE    (APPROX_MODE),
                .DATA_WIDTH     (DATA_WIDTH)
            )
            u_relu
            (
                .data_in        (data_bus_in        [i*DATA_WIDTH +: DATA_WIDTH]),
                .data_valid_in  (data_bus_valid_in),

                .data_out       (data_bus_out       [i*OUT_DATA_WIDTH +: OUT_DATA_WIDTH]),
                .data_valid_out (data_bus_valid     [i] )
            );
        end
    endgenerate

    assign data_bus_valid_out = data_bus_valid[0];

endmodule
