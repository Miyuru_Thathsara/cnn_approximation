`timescale 1ns / 1ps

module max_pool
    (
        clk,
        reset,

        cfg_row_count_in,
        cfg_col_count_in,
        cfg_valid_in,

        ofm_iter_count_in,
        col_count_in,
        row_count_in,

        ofm_iter_count_reg_in,
        col_count_reg_in,
        row_count_reg_in,

        vpool_cache_data_in,
        
        data_in,
        data_valid_in,

        data_out,
        data_idx_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                     "params/global_params.v"
    `include                                                     "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                   POOL_DATA_WIDTH         = 8;
    parameter                                                   IMAGE_WIDTH             = 24;
    parameter                                                   IMAGE_HEIGHT            = 24;
    parameter                                                   MAX_POOL_DIM            = 2;
    parameter                                                   MAX_POOL_NEIGH_DIM      = MAX_POOL_DIM * MAX_POOL_DIM;
    parameter                                                   OFM_ITER_COUNT          = 1;
    parameter                                                   DIM_WIDTH               = 8;   
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                  MAX_POOL_NEIGH_DIM_WIDTH= count2width(MAX_POOL_NEIGH_DIM);
    localparam                                                  OFM_ITER_COUNT_WIDTH    = count2width(OFM_ITER_COUNT);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                       clk;
    input                                                       reset;

    input       [DIM_WIDTH-1 : 0]                               cfg_row_count_in;
    input       [DIM_WIDTH-1 : 0]                               cfg_col_count_in;
    input                                                       cfg_valid_in;

    input       [OFM_ITER_COUNT_WIDTH-1 : 0]                    ofm_iter_count_in;
    input       [DIM_WIDTH-1 : 0]                               col_count_in;
    input       [DIM_WIDTH-1 : 0]                               row_count_in;

    input       [OFM_ITER_COUNT_WIDTH-1 : 0]                    ofm_iter_count_reg_in;
    input       [DIM_WIDTH-1 : 0]                               col_count_reg_in;
    input       [DIM_WIDTH-1 : 0]                               row_count_reg_in;

    input signed [POOL_DATA_WIDTH-1 : 0]                        vpool_cache_data_in; 
    input signed [POOL_DATA_WIDTH-1 : 0]                        data_in;
    input                                                       data_valid_in;

    output      [POOL_DATA_WIDTH-1 : 0]                         data_out;
    output      [MAX_POOL_NEIGH_DIM_WIDTH-1:0]                  data_idx_out;
    output                                                      data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    // reg         [DIM_WIDTH-1 : 0]                               cfg_col_count;
    // reg         [DIM_WIDTH-1 : 0]                               cfg_row_count;

    wire        [POOL_DATA_WIDTH-1 : 0]                         poolv_dout;
    wire        [MAX_POOL_NEIGH_DIM_WIDTH-1 :0]                 poolv_dout_idx;
    wire                                                        poolv_dout_valid;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    
    poolV
    #(
        .POOL_DATA_WIDTH    (POOL_DATA_WIDTH),
        .IMAGE_WIDTH        (IMAGE_WIDTH)
    )
    u_pool_v
    (
        .clk                (clk),
        .reset              (reset),

        .row_count_in       (row_count_in),
        .pix_buff_rd_data_in(vpool_cache_data_in),    
        .data_in            (data_in),
        .data_valid_in      (data_valid_in),

        .data_out           (poolv_dout),
        .data_idx_out       (poolv_dout_idx),
        .data_valid_out     (poolv_dout_valid)
    );

    poolH
    #(
        .POOL_DATA_WIDTH    (POOL_DATA_WIDTH)
    )
    u_pool_h
    (
        .clk                (clk),
        .reset              (reset),

        .col_count_in       (col_count_reg_in),
        .row_count_in       (row_count_reg_in),
        .ofm_iter_count_in  (ofm_iter_count_reg_in),

        .data_in            (poolv_dout),
        .data_idx_in        (poolv_dout_idx),
        .data_valid_in      (poolv_dout_valid),

        .data_out           (data_out),
        .data_idx_out       (data_idx_out),
        .data_valid_out     (data_valid_out)
    );

endmodule