`timescale 1ns / 1ps

module max_pool_layer
    (
        clk,
        reset,

        cfg_row_count_in,
        cfg_col_count_in,
        cfg_valid_in,

        data_in,
        data_valid_in,

        data_out,
        data_idx_out,
        col_count_out,
        row_count_out,
        data_valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                         "params/global_params.v"
    `include                                                        "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       POOL_DATA_WIDTH          = 27;
    parameter                                                       CONV_KERNEL_DIM          = 3; 
    parameter                                                       IMAGE_WIDTH              = 24;
    parameter                                                       IMAGE_HEIGHT             = 24;
    parameter                                                       MAX_POOL_DIM             = 2;
    parameter                                                       OFM_COUNT                = 20;
    parameter                                                       OFM_ITER_COUNT           = 1;     
    parameter                                                       DIM_WIDTH                = 8; 
    parameter                                                       DIM_PREDEFINED           = 1;
    parameter                                                       APPROX_MODE              = 0;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                      MAX_POOL_NEIGH_DIM       = MAX_POOL_DIM * MAX_POOL_DIM;
    localparam                                                      VPOOL_CACHE_DATA_WIDTH   = POOL_DATA_WIDTH * 4;
//    localparam                                                      PIX_BUFF_ADDR_WIDTH      = 10; //count2width(MAX_OFM_ITER_COUNT * IMAGE_WIDTH);
    localparam                                                      VPOOL_CACHE_DEPTH        = 28;
    localparam                                                      PIX_BUFF_ADDR_WIDTH      = count2width(VPOOL_CACHE_DEPTH) ;//count2width(OFM_ITER_COUNT * IMAGE_WIDTH);
    localparam                                                      MAX_POOL_NEIGH_DIM_WIDTH = count2width(MAX_POOL_NEIGH_DIM);
    localparam                                                      OFM_ITER_COUNT_WIDTH     = count2width(OFM_ITER_COUNT);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input                                                           clk;
    input                                                           reset;

    input        [DIM_WIDTH-1 : 0]                                  cfg_row_count_in;
    input        [DIM_WIDTH-1 : 0]                                  cfg_col_count_in;
    input                                                           cfg_valid_in;

    input signed [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  data_in;
    input                                                           data_valid_in;

    output       [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  data_out;
    output       [MAX_POOL_NEIGH_DIM_WIDTH*OFM_COUNT-1:0]           data_idx_out;
    output reg   [DIM_WIDTH-1 : 0]                                  col_count_out;
    output reg   [DIM_WIDTH-1 : 0]                                  row_count_out;
    output                                                          data_valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    reg   signed [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  data_in_reg;
    reg   signed [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  data_in_reg_1;
    reg   signed [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  data_in_reg_2;
    reg   signed [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  data_in_reg_3;
    reg                                                             data_valid_in_reg;
    reg                                                             data_valid_in_reg_1;


    wire         [PIX_BUFF_ADDR_WIDTH-1 : 0]                        pix_buff_addr;
    wire signed  [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  pix_buff_rd_data;
    wire signed  [POOL_DATA_WIDTH*OFM_COUNT-1 : 0]                  pix_buff_wrt_data;
    wire                                                            pix_buff_wrt_en;
    
    reg          [DIM_WIDTH-1 : 0]                                  cfg_col_count;
    reg          [DIM_WIDTH-1 : 0]                                  cfg_row_count;

    reg          [OFM_ITER_COUNT_WIDTH-1 : 0]                       ofm_iter_count;
    reg          [DIM_WIDTH-1 : 0]                                  col_count;
    reg          [DIM_WIDTH-1 : 0]                                  row_count;

    reg          [OFM_ITER_COUNT_WIDTH-1 : 0]                       ofm_iter_count_reg;
    reg          [DIM_WIDTH-1 : 0]                                  col_count_reg;
    reg          [DIM_WIDTH-1 : 0]                                  row_count_reg;
    
    reg          [DIM_WIDTH-1 : 0]                                  col_count_reg_b;
    reg          [DIM_WIDTH-1 : 0]                                  row_count_reg_b;
    
    wire         [OFM_COUNT-1 : 0]                                  valid_bus;
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------
    always@(posedge clk) begin : cfg_blk
        if(reset) begin
            data_in_reg         <= {(POOL_DATA_WIDTH*OFM_COUNT){1'b0}};             
            data_valid_in_reg   <= 1'b0;
            cfg_col_count       <= {DIM_WIDTH{1'b0}};
            cfg_row_count       <= {DIM_WIDTH{1'b0}};
        end
        else begin
            data_in_reg_1        <= data_in;
            data_in_reg_2        <= data_in_reg_1;
            data_in_reg_3        <= data_in_reg_2;
            data_in_reg         <= data_in;
            data_valid_in_reg   <= data_valid_in;
            data_valid_in_reg_1 <= data_valid_in_reg;
            if(DIM_PREDEFINED) begin
                cfg_col_count   <= IMAGE_WIDTH - (CONV_KERNEL_DIM - 1);
                cfg_row_count   <= IMAGE_HEIGHT - (CONV_KERNEL_DIM - 1);
            end
            else begin
                if(cfg_valid_in) begin
                    cfg_col_count   <= cfg_col_count_in - (CONV_KERNEL_DIM - 1);
                    cfg_row_count   <= cfg_row_count_in - (CONV_KERNEL_DIM - 1);
                end 
            end
        end 
    end

    always@(posedge clk) begin : pix_count_blk
        if(reset) begin
            ofm_iter_count          <= {OFM_ITER_COUNT_WIDTH{1'b0}};
            col_count               <= {DIM_WIDTH{1'b0}};
            row_count               <= {DIM_WIDTH{1'b0}};

            ofm_iter_count_reg      <= {OFM_ITER_COUNT_WIDTH{1'b0}};
            col_count_reg           <= {DIM_WIDTH{1'b0}};
            row_count_reg           <= {DIM_WIDTH{1'b0}};
            
            col_count_reg_b         <= {DIM_WIDTH{1'b0}};
            row_count_reg_b         <= {DIM_WIDTH{1'b0}};

            col_count_out           <= {DIM_WIDTH{1'b0}};
            row_count_out           <= {DIM_WIDTH{1'b0}};
        end
        else begin
            ofm_iter_count_reg      <= ofm_iter_count;
            col_count_reg           <= col_count;
            row_count_reg           <= row_count;  
            
            col_count_reg_b         <= col_count_reg;
            row_count_reg_b         <= row_count_reg;

            col_count_out           <= col_count_reg_b;
            row_count_out           <= row_count_reg_b;


            
            if(data_valid_in) begin
                if(ofm_iter_count == OFM_ITER_COUNT - 1) begin //todo : make_cfg
                    ofm_iter_count      <= {OFM_ITER_COUNT_WIDTH{1'b0}};
                    if(col_count == cfg_col_count-1) begin
                        col_count       <= {DIM_WIDTH{1'b0}};
                        if(row_count == cfg_row_count-1)begin
                            row_count   <= {DIM_WIDTH{1'b0}};
                        end
                        else begin
                            row_count   <= row_count + 1'b1;
                        end
                    end
                    else begin
                        col_count       <= col_count + 1'b1;
                    end
                end
                else begin
                    ofm_iter_count      <= ofm_iter_count + 1'b1;
                end 
            end
        end
    end
    
    
    assign pix_buff_addr        = OFM_ITER_COUNT * col_count + ofm_iter_count;
    assign pix_buff_wrt_en      = (data_valid_in == 1'b1 && row_count%2 == 0) ? 1'b1 : 1'b0;
    assign pix_buff_wrt_data    = data_in;


    genvar i;
    genvar j;
    generate
        for(j=0; j<(POOL_DATA_WIDTH * OFM_COUNT)/VPOOL_CACHE_DATA_WIDTH; j=j+1) begin
            if(APPROX_MODE) begin
                vpool_cache_105_24 
                u_vpool_cache_approx
                (
                    .clka   (clk),      
                    .wea    (pix_buff_wrt_en), 
                    .addra  (pix_buff_addr),     
                    .dina   (pix_buff_wrt_data  [j * VPOOL_CACHE_DATA_WIDTH +: VPOOL_CACHE_DATA_WIDTH]),  
                    .douta  (pix_buff_rd_data   [j * VPOOL_CACHE_DATA_WIDTH +: VPOOL_CACHE_DATA_WIDTH])
                );
            end
            else begin
                vpool_cache_108_28
                u_vpool_cache
                (
                    .clka(clk), 
                    .wea(pix_buff_wrt_en), 
                    .addra(pix_buff_addr), 
                    .dina(pix_buff_wrt_data  [j * VPOOL_CACHE_DATA_WIDTH +: VPOOL_CACHE_DATA_WIDTH]), 
                    .douta(pix_buff_rd_data   [j * VPOOL_CACHE_DATA_WIDTH +: VPOOL_CACHE_DATA_WIDTH])
                );
                //     input clka;
                //     input ena;
                //     input [0:0]wea;
                //     input [3:0]addra;
                //     input [107:0]dina;
                //     output [107:0]douta;

                // vpool_cache_135_24 
                // u_vpool_cache
                // (
                //     .clka   (clk),      
                //     .wea    (pix_buff_wrt_en), 
                //     .addra  (pix_buff_addr),     
                //     .dina   (pix_buff_wrt_data  [j * VPOOL_CACHE_DATA_WIDTH +: VPOOL_CACHE_DATA_WIDTH]),  
                //     .douta  (pix_buff_rd_data   [j * VPOOL_CACHE_DATA_WIDTH +: VPOOL_CACHE_DATA_WIDTH])
                // );
            end
        end

        for(i=0; i<OFM_COUNT; i=i+1) begin
            max_pool
            #(
                .POOL_DATA_WIDTH    (POOL_DATA_WIDTH),
                .IMAGE_HEIGHT       (IMAGE_HEIGHT),
                .IMAGE_WIDTH        (IMAGE_WIDTH)
            )
            u_max_pool
            (
                .clk                (clk),
                .reset              (reset),

                .cfg_row_count_in   (cfg_row_count_in),
                .cfg_col_count_in   (cfg_col_count_in),
                .cfg_valid_in       (cfg_valid_in),

                .ofm_iter_count_in  (ofm_iter_count),
                .col_count_in       (col_count_reg),
                .row_count_in       (row_count_reg),

                .ofm_iter_count_reg_in  (ofm_iter_count_reg),
                .col_count_reg_in   (col_count_reg_b),
                .row_count_reg_in   (row_count_reg_b),

                .vpool_cache_data_in(pix_buff_rd_data   [i*POOL_DATA_WIDTH +: POOL_DATA_WIDTH]),

                .data_in            (data_in_reg        [i*POOL_DATA_WIDTH +: POOL_DATA_WIDTH]),
                .data_valid_in      (data_valid_in_reg),

                .data_out           (data_out           [i*POOL_DATA_WIDTH +: POOL_DATA_WIDTH]),
                .data_idx_out       (data_idx_out       [i*MAX_POOL_NEIGH_DIM_WIDTH +: MAX_POOL_NEIGH_DIM_WIDTH]),
                .data_valid_out     (valid_bus          [i])
            ); 
        end 
        assign data_valid_out = valid_bus[0];
    endgenerate

endmodule
