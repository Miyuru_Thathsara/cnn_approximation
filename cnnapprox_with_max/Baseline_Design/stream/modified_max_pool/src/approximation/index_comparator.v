`timescale 1ns / 1ps

module index_comparator
    (
        //data receive port
        current_idx_in,
        max_idx_in,
        valid_in,

        //data transmit port
        no_op_out,
        valid_out
    );

//---------------------------------------------------------------------------------------------------------------------
// Global constant headers
//---------------------------------------------------------------------------------------------------------------------
//    `include                                                         "params/global_params.v"
    `include                                                        "common/util_funcs.v"
//---------------------------------------------------------------------------------------------------------------------
// parameter definitions
//---------------------------------------------------------------------------------------------------------------------
    parameter                                                       MAX_POOL_DIM             = 2;
//---------------------------------------------------------------------------------------------------------------------
// localparam definitions
//---------------------------------------------------------------------------------------------------------------------
    localparam                                                      MAX_POOL_NEIGH_DIM       = MAX_POOL_DIM * MAX_POOL_DIM;
    localparam                                                      MAX_POOL_NEIGH_DIM_WIDTH = count2width(MAX_POOL_NEIGH_DIM);
//---------------------------------------------------------------------------------------------------------------------
// I/O signals
//---------------------------------------------------------------------------------------------------------------------
    input        [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                   current_idx_in;
    input        [MAX_POOL_NEIGH_DIM_WIDTH-1 : 0]                   max_idx_in;
    input                                                           valid_in;

    output reg                                                      no_op_out;
    output reg                                                      valid_out;
//---------------------------------------------------------------------------------------------------------------------
// Internal wires and registers
//---------------------------------------------------------------------------------------------------------------------
    
//---------------------------------------------------------------------------------------------------------------------
// Implementation
//---------------------------------------------------------------------------------------------------------------------

    // always@(*) begin
    //     if(valid_in) begin
    //         valid_out           = 1'b1;
    //         if(current_idx_in == max_idx_in) begin
    //             no_op_out   = 1'b1;
    //         end
    //         else begin
    //             no_op_out   = 1'b0;
    //         end
    //     end
    //     else begin
    //         no_op_out       = 1'b0;
    //         valid_out               = 1'b0;
    //     end
    // end

    always@(*) begin
        valid_out       = valid_in;
        if(current_idx_in != max_idx_in) begin
            no_op_out   = 1'b1;
        end
        else begin
            no_op_out   = 1'b0;
        end
        // no_op_out   = 1'b0;
    end

endmodule
