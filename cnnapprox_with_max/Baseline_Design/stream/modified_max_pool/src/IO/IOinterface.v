`timescale 1ns / 1ps
module IOinterface(
    clk,
    reset,
    pixel_in,
    pixel_valid_in,
    pixel_out,
    pixel_valid_out
    );
    
    parameter                       DATA_WIDTH  = 8;
    parameter                       IFM_COUNT   = 64;
    parameter                       REG_SIZE    = 8;
    
    input                                         clk;
    input                                         reset;
    input [REG_SIZE * DATA_WIDTH-1:0]             pixel_in;
    input                                         pixel_valid_in;
    
    output reg [DATA_WIDTH*IFM_COUNT-1:0]         pixel_out;
    output reg                                    pixel_valid_out;
    
    
    
    reg [IFM_COUNT-1:0]                           counter;
    
    initial begin
        counter <= 0;
        pixel_valid_out <= 1'b0;
    end
    
    always@(posedge clk)begin
        if(reset)begin
            pixel_out <= {(DATA_WIDTH*IFM_COUNT){1'b0}};
            pixel_valid_out <= 1'b0;
        end
        else begin
            if(pixel_valid_in)begin
                pixel_out[((IFM_COUNT/REG_SIZE)-1-counter) * REG_SIZE * DATA_WIDTH +: REG_SIZE * DATA_WIDTH] <= pixel_in;
                if(counter < (IFM_COUNT/REG_SIZE)-1)begin
                    counter <= counter + 1'b1;
                    pixel_valid_out <= 1'b0;
                end
                else begin
                    counter <= {(IFM_COUNT){1'b0}};
                    pixel_valid_out <= 1'b1;
                end 
            end   
        end
    end
endmodule
