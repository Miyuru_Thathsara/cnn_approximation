// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Tue Apr  9 10:27:40 2019
// Host        : HPZ840MQ running 64-bit Ubuntu 16.04.5 LTS
// Command     : write_verilog -force -mode synth_stub
//               /media/data3/duvindu/projects/modified_max_pool_50_all_layers/lenet_maxpool_mod.runs/max_pool_idx_mem_l2_synth_1/max_pool_idx_mem_l2_stub.v
// Design      : max_pool_idx_mem_l2
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu9p-fsgd2104-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_2,Vivado 2018.3" *)
module max_pool_idx_mem_l2(clka, ena, wea, addra, dina, douta)
/* synthesis syn_black_box black_box_pad_pin="clka,ena,wea[0:0],addra[1:0],dina[99:0],douta[99:0]" */;
  input clka;
  input ena;
  input [0:0]wea;
  input [1:0]addra;
  input [99:0]dina;
  output [99:0]douta;
endmodule
