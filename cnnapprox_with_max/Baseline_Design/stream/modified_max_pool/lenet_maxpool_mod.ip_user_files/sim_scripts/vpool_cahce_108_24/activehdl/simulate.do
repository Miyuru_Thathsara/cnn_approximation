onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+vpool_cahce_108_24 -L xil_defaultlib -L xpm -L blk_mem_gen_v8_4_2 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.vpool_cahce_108_24 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {vpool_cahce_108_24.udo}

run -all

endsim

quit -force
