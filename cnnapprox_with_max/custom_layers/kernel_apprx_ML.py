#####################################################################################
#																				    #
# Project Title : Lowering Dynamic Power of a Stream-based CNN Hardware Accelerator #
# Author        : Rukshan Wickramasinghe											#
# Supervisor    : Prof. Lam Siew Kei   											    #
# Lab           : Hardware and Embedded Systems Lab (HESL), NTU, Singapore		    #
# Script name   : kernel_apprx_ML.py												#
# Description   : This function replaces the weights of the "cus_conv" layer of the #
#                 "net" with the quantized values of the weights of "conv" layer 	#
#                 using the number of levels given.									#
# Last Modified : 6th Dec 2018 													    #
#																				    #
#####################################################################################

import caffe
import numpy as np
import math


def kernel_apprx_ML(net,kernel_name_original,kernel_name_cus,num_levels=5):
	k=net.params[kernel_name_original][0].data
	shp=k.shape
	siz=k.size
	unrolled=k.reshape((1,siz))
        #k_abs = np.abs(k)
	percentile_val = np.percentile(k,99)
        next_pow_2 = pow(2, math.ceil(math.log(percentile_val)/math.log(2)))
	#bins=[0]+[1/2.0**i for i in range(num_levels)][::-1]	#determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]
	bins=[0]+[next_pow_2/2.0**i for i in range(num_levels)][::-1]	#determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]
	#bins=[0]+[0.0078125 * 2**i for i in range(num_levels)][::-1]	#determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]
	#bins=[0]+[0.03125 * 2**i for i in range(1)][::-1]	#determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]
        	
	levels=np.array(bins).reshape((len(bins),1))
	abs_distance=np.abs(np.abs(unrolled)-levels)
	locations=np.argmin(abs_distance,axis=0).reshape((1,siz))
	neg_pos=np.zeros((1,siz))
	neg_pos[unrolled>0]=1.0
	neg_pos[unrolled<0]=-1.0
	temp=np.zeros((1,siz))
	for i in range(levels.size):
		temp[locations==i]=levels[i]
	temp_final=temp*neg_pos
	temp=temp_final.reshape(shp)
	#net.params[kernel_name_cus][0].data[...]=temp
	#net.params[kernel_name_cus][1].data[...]=0
	num_levels = math.log( (np.max(np.abs(temp[np.nonzero(temp)]))/np.min(np.abs(temp[np.nonzero(temp)])) ), 2) + 1
	print "\n\nApproximated kernels in layer "+kernel_name_original +" with "+str(num_levels)+" Levels\n"+str(bins)+"\n\n"
	print("Actual Min level : %f Max level : %f 99th percentile : %f nearest pow 2 : %f Num Levels : %d" %(np.min(k), np.max(k), percentile_val, next_pow_2, num_levels) )
	print("Approx Min level : %f Max level : %f Num Levels : %d" %(np.min(np.abs(temp[np.nonzero(temp)])), np.max(np.abs(temp[np.nonzero(temp)])), num_levels) )
	#print(temp.shape)
        orig_weight_txt_name = kernel_name_original + '.txt'
        cus_weight_txt_name = kernel_name_cus + '.txt'
        print(k.shape)
        print(temp.shape)
	np.savetxt(orig_weight_txt_name, k.flatten(), delimiter=',', fmt='%f')
	np.savetxt(cus_weight_txt_name, temp.flatten(), delimiter=',', fmt='%f')
        print(k[0][0])
	print(temp[0][0])
        return temp

def kernel_sign_connect_ML(net,kernel_name_original):
        k=net.params[kernel_name_original][0].data
        k_sign_connect = k.copy()
        k_sign_connect[k<0] = -1
        k_sign_connect[k>0] = 1
	print("Original : ", k[0][0])
	print("Modified : ", k_sign_connect[0][0])
	return k_sign_connect


def kernel_apprx_residual_ML(net,kernel_name_original,kernel_name_cus,num_levels=5):
        k=net.params[kernel_name_original][0].data
        shp=k.shape
        siz=k.size
        unrolled=k.reshape((1,siz))
        bins=[0]+[0.015625/2.0**i for i in range(num_levels)][::-1]    #determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]

        levels=np.array(bins).reshape((len(bins),1))
        abs_distance=np.abs(np.abs(unrolled)-levels)
        locations=np.argmin(abs_distance,axis=0).reshape((1,siz))
        neg_pos=np.zeros((1,siz))
        neg_pos[unrolled>0]=1.0
        neg_pos[unrolled<0]=-1.0
        temp=np.zeros((1,siz))
        for i in range(levels.size):
                temp[locations==i]=levels[i]
        temp_final=temp*neg_pos
        temp=temp_final.reshape(shp)
        #net.params[kernel_name_cus][0].data[...]=temp
        #net.params[kernel_name_cus][1].data[...]=0
        num_levels = math.log( (np.max(np.abs(temp[np.nonzero(temp)]))/np.min(np.abs(temp[np.nonzero(temp)])) ), 2) + 1
        print "\n\nApproximated kernels in layer "+kernel_name_original +" with "+str(num_levels)+" Levels\n"+str(bins)+"\n\n"
        print("Actual Min level : %f Max level : %f Num Levels : %d" %(np.min(k), np.max(k), num_levels) )
        print("Approx Min level : %f Max level : %f Num Levels : %d" %(np.min(np.abs(temp[np.nonzero(temp)])), np.max(np.abs(temp[np.nonzero(temp)])), num_levels) )
        #print(temp.shape)
	residual = k-temp
	print(k[0][0])
	print(temp[0][0])
	#print(residual[0][0])

        return [residual,temp]

def update_params(net,kernel_name_original,kernel_name_cus,temp):
    print("update params")
#     print("Net params shape : ", net.params[kernel_name_cus][0].data.shape)
#     print("Temp params shape : ", temp.shape)
    net.params[kernel_name_cus][0].data[...]=temp
       #if(kernel_name_original == "conv2") : 
 	#    net.params[kernel_name_cus][1].data[...]=0
	#else :
	#    net.params[kernel_name_cus][1].data[...]=net.params[kernel_name_original][1].data
    #net.params[kernel_name_cus][1].data[...]=0
	#print(net.params[kernel_name_original][1].data)
    #net.params[kernel_name_cus][1].data[...]=net.params[kernel_name_original][1].data

def update_params_residual(net,kernel_name_original,residual_weights):
    print("update params residual")
    net.params[kernel_name_original][0].data[...]=residual_weights
    net.params[kernel_name_original][1].data[...]=0.0

def copy_layer_params(net,kernel_name_original,kernel_name_cus) :
#     print("Layer params , ", net.params[kernel_name_original][2].data[...])
    net.params[kernel_name_cus][0].data[...] = net.params[kernel_name_original][0].data[...]
    net.params[kernel_name_cus][1].data[...] = net.params[kernel_name_original][1].data[...]
    
#     print("copy layer params ", kernel_name_original)
#     print(net.params[kernel_name_cus][0].data)
#     print(net.params[kernel_name_original][0].data)
#     net.params[kernel_name_cus][0].data[...] = 0.0
