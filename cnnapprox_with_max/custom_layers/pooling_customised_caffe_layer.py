import caffe
import numpy as np
import math
from im2col import *


class pooling_customised_caffe_layer(caffe.Layer):
    def setup(self, bottom, top):
        #if len(top) != 2:
        #    raise Exception("2 TOP BLOBS IS REQUIRED FOR CUSTOMISED CONVOLUTION LAYER")
        if len(bottom) != 2:
            raise Exception("2 BOTTOM BLOBS ARE REQUIRED FOR CUSTOMISED CONVOLUTION LAYER")
        #params = eval(self.param_str)
	#TODO: retrieve from prototxt
	params={"pool_kernel_size":2,
		"pool_stride":2,
		}
        self.pool_kernel_size = params["pool_kernel_size"]
        self.pool_stride = params["pool_stride"]

    #def get_probabilistic_saving(self, hist):
#	saving = hist[0] * 3 + hist[1] * 2 + hist[1] 
 #       #saving_percent = float(saving)/
  #      print("Saving : ", saving)

    def forward(self, bottom, top):
        image_ori=bottom[0].data
	image_mod=bottom[1].data
	#print(image_mod[0][0])
        in_batch, in_channel, in_row, in_col = image_ori.shape
	total_activations = in_batch * in_channel * in_row * in_col
	h_out = int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride)))
	w_out = int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))
        top[0].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
	top[0].data[...]=np.zeros((in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))))
	top[1].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
        top[1].data[...]=np.zeros((in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))))
	image_ori_reshaped= image_ori.reshape(in_batch * in_channel, 1, in_row, in_col)
	image_mod_reshaped= image_mod.reshape(in_batch * in_channel, 1, in_row, in_col)
	im_ori_col = im2col_indices(image_ori_reshaped, self.pool_kernel_size, self.pool_kernel_size, padding=0, stride=self.pool_stride)
	im_mod_col = im2col_indices(image_mod_reshaped, self.pool_kernel_size, self.pool_kernel_size, padding=0, stride=self.pool_stride)
	print("Total Activations ", total_activations)
	max_idx = np.argmax(im_mod_col, axis=0)
	#print("im2col test ", im_mod_col[0][0], im_mod_col[1][0], im_mod_col[2][0], im_mod_col[3][0], max_idx[0])
        hist=np.histogram(max_idx, bins=[0,1,2,3,4])
	print("Histogram MAX idx : ", hist)
	#print(hist[0])
	saving = hist[0][0] * 3 + hist[0][1] * 2 + hist[0][1]
        print("Saving : %d, Total Activations : %d, Percent : %f" %(saving, total_activations, float(saving)/total_activations))
	#print("Max idx shape : ", max_idx.shape)
	out_orig = im_ori_col[max_idx, range(max_idx.size)]
	out_orig = out_orig.reshape(h_out, w_out, in_batch, in_channel)
	out_orig = out_orig.transpose(2, 3, 0, 1)
	out_approx = im_mod_col[max_idx, range(max_idx.size)]
        out_approx = out_approx.reshape(h_out, w_out, in_batch, in_channel)
        out_approx = out_approx.transpose(2, 3, 0, 1)
	print(out_orig.shape)
	print(out_approx.shape)
	print(top[0].data.shape)
	print(top[1].data.shape)
	top[0].data[...] = out_orig
	top[1].data[...] = out_approx
        print "***** POOL1 for BATCH " + str(in_batch) + " completed ***** "


    def __getitem__(self):
        pass

    def reshape(self, bottom, top):
	image_ori=bottom[0].data
	in_batch, in_channel, in_row, in_col = image_ori.shape
	top[0].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
	top[1].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
        pass

    def backward(self, bottom, top):
        pass


