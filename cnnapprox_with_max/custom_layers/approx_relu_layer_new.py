import caffe
import numpy as np
import math
from im2col import *

TRAIN = 0
TEST = 1

class approx_relu_layer_new(caffe.Layer):

    def setup(self, bottom, top): 
        if len(bottom) != 2:
            raise Exception("ERROR: Two Bottom Blobs Required (Orig Conv (0) and Approx Conv(1) )")
        if self.phase == TEST and len(top) != 2:
            raise Exception("ERROR : Two Top Blobs")

    def forward(self, bottom, top):
        orig_conv_activations=bottom[0].data
        approx_conv_activations=bottom[1].data
        #print("Approx RELU forward")
	#top[0].reshape(orig_conv_activations.shape[0], orig_conv_activations.shape[1], orig_conv_activations.shape[2], orig_conv_activations.shape[3])
        top[0].data[...]=np.zeros((orig_conv_activations.shape[0], orig_conv_activations.shape[1], orig_conv_activations.shape[2], orig_conv_activations.shape[3]))
        top[1].data[...]=np.zeros((orig_conv_activations.shape[0], orig_conv_activations.shape[1], orig_conv_activations.shape[2], orig_conv_activations.shape[3]))
	#print(orig_conv_activations.shape)
	#print(top[0].data.shape)
	
	top[0].data[...]=orig_conv_activations
	top[0].data[approx_conv_activations<=0] = 0.0
	top[1].data[...]=approx_conv_activations
        top[1].data[approx_conv_activations<=0] = 0.0
	#print("*********************************")
	#print(orig_conv_activations.shape)
	#print(orig_conv_activations[0][0][0][1:10])
	#print(approx_conv_activations[0][0][0][1:10])
	#print(top[0].data[0][0][0][1:10])
	#print("*********************************")

    def reshape(self, bottom, top):
        top[0].reshape(*bottom[0].shape)
        top[1].reshape(*bottom[1].shape)

    def backward(self, bottom, top):
        pass
