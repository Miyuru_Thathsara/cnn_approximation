import caffe
import numpy as np

def tohex(val, nbits):
  return hex((val + (1 << nbits)) % (1 << nbits))

def init_network(config_name, weights_model_name):
    caffe.set_mode_gpu()
    net=caffe.Net(config_name,weights_model_name,caffe.TEST)
    return net

def quantize_weights(weights, scale_factor, clipping_point):
    quant_weights = np.round(weights * scale_factor)
    quant_weights = quant_weights.astype(int)
    quant_weights[quant_weights>clipping_point] = clipping_point
    quant_weights[quant_weights<(-1*clipping_point)] = -1*clipping_point
    return quant_weights

def quantize_weights_layer(net, name, scale_factor):
    weights = net.params[name][0].data[...]
    quant_weights = quantize_weights(weights, scale_factor, 127)
    #print(weights[1][0])
    #print(quant_weights[1][0])
    print("Quant Weights Name : %s Min : %d Min : %d" %(name, np.min(quant_weights), np.max(quant_weights)))
    return quant_weights

def quantize_bias_layer(net, name, scale_factor, clipping_point):
    bias = net.params[name][1].data[...]
    print(bias)
    quant_bias = quantize_weights(bias, scale_factor, clipping_point)
    #print(quant_bias.shape)
    print(quant_bias)
    return quant_bias

def convert_weights(fp, layer_name, weights) :
    dimensions = weights.shape
    #print(dimensions)
    weights_count = dimensions[0] * dimensions[1] * dimensions[2] * dimensions[3]
    identifier = 'localparam signed [7:0] weight_array_' +layer_name+'[' + str(weights_count)+ '-1:0] =  '
    #print(identifier)
    #print(weights[0][0])
    weights_str = ' '
    for i in range(dimensions[0]-1,-1,-1): #OFM dim
	kernel_hex = ''
    	for j in range(dimensions[1]-1,-1,-1): #IFM dim
    		for m in range(dimensions[2]-1,-1,-1): #filt row
    			for n in range(dimensions[3]-1,-1,-1): #filt col
    				weights_hex = tohex(weights[i][j][n][m], 8)
    				weights_hex = '8\'h' + weights_hex[2:-1]
    				kernel_hex = weights_hex + ', ' + kernel_hex
				#if(i==1 and j==0) : 
				#    print("weight : %d, weights hex : %s" %(weights[i][j][m][n], weights_hex))	
	print("Hello")
	#kernel_hex = '{' + kernel_hex[:-2] + '}'	
	kernel_hex = kernel_hex[:-2]	
	#print(kernel_hex)
	weights_str = kernel_hex+ ', \n' + weights_str
	#if(i==1) : 
		#print(kernel_hex)
      
    weights_str = identifier + '\n{' + weights_str[:-4] + '};\n\n'
    fp.write(weights_str)	
    #print(weights_str)

def convert_bias(fp, layer_name, bias, nbits) :
    bias_count = bias.shape[0]
    bias_hex_str = ''
    identifier = 'localparam signed ['+ str(nbits-1) + ':0] bias_' +layer_name+'[' + str(bias_count)+ '-1:0] =  '
    for i in range(0, bias_count) :
	bias_hex     = tohex(bias[i], nbits)
	bias_hex_str = str(nbits) + '\'h' + bias_hex[2:-1] + ' ,' + bias_hex_str
    bias_hex_str = identifier + '\n' + '{'  +bias_hex_str[:-2] + '};\n\n'
    fp.write(bias_hex_str)
    #print(bias_hex_str)

def approximate_weights(weights) :
    non_zero_min = np.min(abs(weights[np.nonzero(weights)]))
    approx_shift_factors = weights/non_zero_min
    approx_shift_factors = approx_shift_factors.astype(int)
    #print(weights[0])
    #print(approx_shift_factors[0])
    return approx_shift_factors

#def convert_approx_weights(fp, layer_name, weight) : 
def convert_activations(fp, net, layer_name, scale_factor):
    input_data = net.blobs[layer_name].data  #first batch first image
    #print("net : %s max : %f min : %f" %(layer_name, np.max(input_data), np.min(input_data)))
    #print(input_data.shape)
    im_count = input_data.shape[0]
    fm_count = input_data.shape[1]
    im_dim = input_data.shape[2]
    pixel_count = im_dim * im_dim
    #print(im_dim)
    #print(input_data[0][0])
    quant_input_data = quantize_weights(input_data, scale_factor, (2**25))   
    #print(quant_input_data.shape) 
    #print("im count : %d fm count : %d im dim : %d" %(im_count, fm_count, im_dim))
    identifier = 'localparam   signed [7:0] pixel_array_' + layer_name + '[' + str(pixel_count-1) + '-1 : 0] = \n'
    inputs_str = ''
    input_str = ''
    for m in range(0, fm_count) :
	for i in range(0, im_dim) :
    	    for j in range(0, im_dim) :
    	        inputs_str = inputs_str + ', ' + str(quant_input_data[27][m][i][j])
            inputs_str = inputs_str + '\n'
        inputs_str = inputs_str + '\n'
    inputs_str = identifier + '{\n' + inputs_str[1:] + '};'
    fp.write(inputs_str)
    #return quant_input_data
