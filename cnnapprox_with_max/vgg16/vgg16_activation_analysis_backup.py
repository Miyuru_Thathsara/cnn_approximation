import caffe
import numpy as np
import time
from kernel_apprx_multi_level_per_channel_closest_level import *
#import matplotlib.pyplot as plt
caffe.set_mode_gpu()

#function definitions
def init_network():
    net2=caffe.Net('vgg_conventional.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
    return net2

def init_mod_network():
    net3=caffe.Net('vgg_customised.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
    kernel_apprx_multi_level_per_channel_closest_level(net3,"conv1_2","conv1_2_cus",6)
    kernel_apprx_multi_level_per_channel_closest_level(net3,"conv2_2","conv2_2_cus",6)
    kernel_apprx_multi_level_per_channel_closest_level(net3,"conv3_3","conv3_3_cus",7)
    kernel_apprx_multi_level_per_channel_closest_level(net3,"conv4_3","conv4_3_cus",8)
    kernel_apprx_multi_level_per_channel_closest_level(net3,"conv5_3","conv5_3_cus",8)
    return net3

def run_activation_sample(net, layer_name="pool1"):
    acc=[]
    acc5=[]
    #activations = []
    tic=time.time()

    batch_size = 10
    num_images = 10000
    iterations = num_images/batch_size

    neg_activations_count = 0
    activation_count      = 0

    print("Analysing Pre-Activations for layer : %s" %(layer_name))
    for i in range(iterations):
        net.forward()
        activations            = []
	activations.append(net.blobs[layer_name].data)
	activations            = net.blobs[layer_name].data
	activations            = np.array(activations)
        activations            = activations.flatten()
	neg_activations        = (activations<0)
	neg_activations_count  += np.count_nonzero(neg_activations)
	activation_count       += len(activations)
        acc.append(net.blobs["accuracy"].data*100)
        acc5.append(net.blobs["accuracy_5"].data*100)
        print "--------------------------------------------Processed %s images-----------------------------------------"%((i+1)*batch_size)
    
    neg_activations_percent = 100 * float(neg_activations_count)/activation_count
    accr=np.mean(acc)
    accr5=np.mean(acc5)
    toc=time.time()
    t=toc-tic
#     print "\nTime elapsed: %s Minutes"%(t/60.0)
    print "Accuracy (Top 1): %.17f%%, Accuracy (Top 5): %.17f%%"%(accr, accr5)
    return [neg_activations_count, activation_count, neg_activations_percent]

def run_activation_sample_mod_network(net, layer_name="pool1", num_images=10000):
    acc=[]
    acc5=[]
    #activations = []
    tic=time.time()

    batch_size = 10
    #num_images = 10000
    iterations = num_images/batch_size

    neg_activations_count = 0
    activation_count      = 0
    approx_equal_sum      = 0
    layer_name_approx        = layer_name + '_cus'

    print("Analyzing the approximation confidence for layer : %s cus layer : %s" %(layer_name, layer_name_approx))
    for i in range(iterations):
        net.forward()
        activations            = []
        activations_approx     = []
        approx_equal_array     = []

        activations.append(net.blobs[layer_name].data)
        activations_approx.append(net.blobs[layer_name_approx].data)

        #negative activations        
        activations            = np.array(activations)
        activations            = activations.flatten()
        neg_activations        = (activations<0)
        #negative approximate activations
        activations_approx     = np.array(activations_approx)
        activations_approx     = activations_approx.flatten()
        neg_activations_approx = (activations_approx<0)
        approx_equal_array     = np.equal(neg_activations, neg_activations_approx);
        #check equality
        approx_equal_sum       += np.count_nonzero(approx_equal_array)
        activation_count       += len(activations)
        acc.append(net.blobs["accuracy"].data*100)
        acc5.append(net.blobs["accuracy_5"].data*100)
        print "--------------------------------------------Processed %s images-----------------------------------------"%((i+1)*batch_size)

    neg_equality_percent    = 100 * float(approx_equal_sum)/activation_count
    neg_activations_percent = 100 * float(neg_activations_count)/activation_count
    accr=np.mean(acc)
    accr5=np.mean(acc5)
    toc=time.time()
    t=toc-tic
#     print "\nTime elapsed: %s Minutes"%(t/60.0)
    print "Accuracy (Top 1): %.17f%%, Accuracy (Top 5): %.17f%%"%(accr, accr5)
    return [neg_activations_count, activation_count, neg_activations_percent, approx_equal_sum, neg_equality_percent]

def analyse_activations(net, layer_name="pool1"):
    #net = init_network();
    #Activations statistics
    #activations = run_activation_sample(net, layer_name)
    #activations = np.array(activations)
    #activations = activations.flatten()

    #neg_activations = (activations<0)
    #neg_activations_count = np.count_nonzero(neg_activations)
    #neg_activations_percent = 100 * float(neg_activations_count)/np.count_nonzero(activations)
    [neg_activations_count, activation_count, neg_activations_percent] = run_activation_sample(net, layer_name)
    print("Activation Analysis, Total : %d, Negative : %d, Percent : %f%%" %(activation_count, neg_activations_count, neg_activations_percent) )
    print ("****************************************************\n")
    return neg_activations_percent

def analyze_approx_confidence(net, layer_name="pool1"):
    num_images = 100
    [neg_activations_count, activation_count, neg_activations_percent, approx_equal_sum, neg_equality_percent] = run_activation_sample_mod_network(net, layer_name, num_images)
    print("Activation Analysis, Total : %d, Negative : %d, Percent : %f%% approx_equal_sum : %d neg_equality_percent : %f%%" %(activation_count, neg_activations_count, neg_activations_percent, approx_equal_sum, neg_equality_percent) )
    print ("****************************************************\n")
#run

#run

def analyze_activations():
    net = init_network();
    activation_redundant_percent = []
    activation_redundant_percent.append(["conv_1_1", analyse_activations(net, "conv_1_1")])
    activation_redundant_percent.append(["pool1", analyse_activations(net, "pool1")])
    activation_redundant_percent.append(["conv2_1", analyse_activations(net, "conv2_1")])
    activation_redundant_percent.append(["pool2", analyse_activations(net, "pool2")])
    activation_redundant_percent.append(["conv3_1", analyse_activations(net, "conv3_1")])
    activation_redundant_percent.append(["conv3_2", analyse_activations(net, "conv3_2")])
    activation_redundant_percent.append(["pool3", analyse_activations(net, "pool3")])
    activation_redundant_percent.append(["conv4_1", analyse_activations(net, "conv4_1")])
    activation_redundant_percent.append(["conv4_2", analyse_activations(net, "conv4_2")])
    activation_redundant_percent.append(["pool4", analyse_activations(net, "pool4")])
    activation_redundant_percent.append(["conv5_1", analyse_activations(net, "conv5_1")])
    activation_redundant_percent.append(["conv5_1", analyse_activations(net, "conv5_1")])
    activation_redundant_percent.append(["pool5", analyse_activations(net, "pool5")])

def analyze_approximation():
    net = init_mod_network();
    analyze_approx_confidence(net, "conv1_2")
    analyze_approx_confidence(net, "conv2_2")
    analyze_approx_confidence(net, "conv3_3")
    analyze_approx_confidence(net, "conv4_3")
    analyze_approx_confidence(net, "conv5_3")

analyze_approximation();
#print(activation_redundant_percent)
# activation_redundant_percent.append(analyse_activations("conv2_1"))
# activation_redundant_percent.append(analyse_activations("pool2"))
# activation_redundant_percent.append(analyse_activations("conv3_1"))
# activation_redundant_percent.append(analyse_activations("conv3_2"))
# activation_redundant_percent.append(analyse_activations("pool3"))
## activation_redundant_percent.append(analyse_activations("conv4_1"))
# activation_redundant_percent.append(analyse_activations("conv4_2"))
# activation_redundant_percent.append(analyse_activations("pool4"))
# activation_redundant_percent.append(analyse_activations("conv5_1"))
# activation_redundant_percent.append(analyse_activations("conv5_2"))
# activation_redundant_percent.append(analyse_activations("pool5"))
