import numpy as np
import caffe
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

net_name='vgg_conventional.prototxt'
net=caffe.Net(net_name,'VGG_ILSVRC_16_layers.caffemodel',caffe.TEST) 
file_name='variance_report.txt'
para=['conv1_2','conv2_2','conv3_3','conv4_3','conv5_3']#include the layers you need to extract data
parameters={}
variance={}
thresholds={}
percentages={}

for i in para:
	kernels=net.params[i][0].data
	parameters[i]=kernels
	abs_mean_layer=np.mean(np.abs(kernels),keepdims=1)
	thresholds[i]=abs_mean_layer
	abs_mean_kernel=np.mean(np.abs(kernels),axis=(1,2,3),keepdims=1)
	zero_mean=abs_mean_kernel-abs_mean_layer
	zero_mean_2=zero_mean*zero_mean
	var=np.sum(zero_mean_2)/float(zero_mean_2.size)
	variance[i]=var
	tot_weights=kernels.size
	plus=np.sum(kernels>abs_mean_layer)
	minus=np.sum(kernels<-abs_mean_layer)
	zro=tot_weights-(plus+minus)
	percentages[i]=[100.0*plus/tot_weights,100.0*zro/tot_weights,100.0*minus/tot_weights]

with open(file_name,'w') as f:
	f.write("######### Variance of "+net_name+" ############\n\n\n")
	for i in para:
		f.write(i+" : %.16f\n"%(variance[i]))

print "\n\nVariance.txt created!"

num_levels=7
bins=[1/2.0**i for i in range(2,num_levels)]
clrs=['grey','red','green','navy','darkgreen','m','firebrick']
###########plotting the weights################

for i in para:
	wei=parameters[i].reshape((parameters[i].size))
	plt.hist(wei,bins=100)
	plt.title(i+' weight distribution')
	plt.xlabel('weight')
	plt.ylabel('frequency')
	plt.axvline(x=0,color='white',linestyle='dashed',linewidth=0.5)
	for k in range(len(bins)):
		plt.axvline(x=bins[k],color=clrs[k],linestyle='dashed',linewidth=0.5)
		plt.axvline(x=-bins[k],color=clrs[k],linestyle='dashed',linewidth=0.5)
	#red_patch=mpatches.Patch(color='red',label='Thresholds: \n'+str(thresholds[i][0,0,0,0])+'\n'+str(-thresholds[i][0,0,0,0]))
	green_patch=mpatches.Patch(color='green',label='\nVariance: \n%.10f'%(variance[i]))
	plt.legend(handles=[green_patch])
	#plt.show()
	plt.savefig('Weight_distribution_'+i+'.png',dpi=400)
	plt.clf()

