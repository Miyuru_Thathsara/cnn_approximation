import numpy as np
import caffe

caffe.set_mode_gpu()

model_file='VGG_ILSVRC_16_layers.caffemodel'
deploy_prototxt='vgg_conventional.prototxt'

net=caffe.Net(deploy_prototxt,model_file,caffe.TEST)   #init_net

#Params_list=[k for k,v in net.params.items()]  #list with layers which have biases
#print([k for k, v in net.params.items()])      #print above list

Acc_layer_1="accuracy"
Acc_layer_5="accuracy_5"
Accuracy_list_1=[]
Accuracy_list_5=[]
iterations=10000/50

#for j in range(len(Params_list)):
#        layer=Params_list[j]
#        #print(net.params[layer][1].data)
#        inter=[x*(1/255) for x in net.params[layer][1].data]
#        net.params[layer][1].data[...]=np.array(inter)


output=net.forward()
Acc_1=net.blobs[Acc_layer_1].data * 100

print(Acc_1.shape)
print(type(Acc_1))


for i in range(iterations):
        output=net.forward()
#	print(net.blobs[Acc_layer_1].data)
        Acc_1=net.blobs[Acc_layer_1].data
        Acc_5=net.blobs[Acc_layer_5].data
        Accuracy_list_1.append(Acc_1)
	#print(Accuracy_list_1[-1])
        Accuracy_list_5.append(Acc_5)


print(Accuracy_list_1)

list_sum_1=sum(Accuracy_list_1)
length_list_1=len(Accuracy_list_1)
Avg_Acc_1=list_sum_1/length_list_1

list_sum_5=sum(Accuracy_list_5)
length_list_5=len(Accuracy_list_5)
Avg_Acc_5=list_sum_5/length_list_5

print("Average Accuracy of VGG (TOP1):"+str(Avg_Acc_1))
print("Average Accuracy of VGG (TOP5):"+str(Avg_Acc_5))

print("Mean Accuracy : ", np.mean(Accuracy_list_1))
