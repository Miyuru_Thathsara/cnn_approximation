import caffe
import numpy as np

def k_aprox(net,kernel_name_original,kernel_name_cus,threshold):
	k=net.params[kernel_name_original][0].data
	siz=k.shape
	temp=np.zeros(siz)
	temp[k>=threshold]=1
	temp[k<=-threshold]=-1
	net.params[kernel_name_cus][0].data[...]=temp
	net.params[kernel_name_cus][1].data[...]=0
	print "Approximated kernels in layer "+kernel_name_original
