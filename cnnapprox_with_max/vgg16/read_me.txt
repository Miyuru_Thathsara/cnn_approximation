This is a guide for creating/running the custom model

kernel_apprx.py -  contains the function k_aprox(net,kernel_name_original,kernel_name_cus,threshold)
				approximates the corresponding set of kernels to contain 1,0,-1

run_net_custom.py  - contains the function calling required to run the modified algorithm

pooling_customised_caffe_layer.py  - The customised pooling layer. It takes in 2 blobs (original convolution maps and customised convolution maps).Outputs pooled maps of original conv values according to 						the indexing of the customised conv map.(edit this file according to the pool stride and pool kernel size)
