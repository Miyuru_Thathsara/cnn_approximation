import caffe
import numpy as np
import time
#from kernel_apprx_multi_level_per_channel_closest_level import *
from kernel_apprx_ML import *
import matplotlib.pyplot as plt

GPU_ID = 0 # Switch between 0 and 1 depending on the GPU you want to use.
caffe.set_mode_gpu()
caffe.set_device(GPU_ID)

def plot_hist(distr):
    distr_flattened = distr.flatten()
    plt.hist(distr_flattened)
    plt.save_fig('mapped_levels.eps', format='eps', dpi=1000)

#baseline model
net=caffe.Net('vgg_conventional.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
#modified model
net2=caffe.Net('vgg_customised_temp.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
approx_params=kernel_apprx_ML(net2,"conv1_1","conv1_1_cus",8)
update_params(net2, "conv1_1", "conv1_1_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv1_2","conv1_2_cus",4)
print(approx_params.shape)
update_params(net2, "conv1_2", "conv1_2_cus", approx_params)

approx_params=kernel_apprx_ML(net2,"conv2_1","conv2_1_cus",8)
update_params(net2, "conv2_1", "conv2_1_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv2_2","conv2_2_cus",8)
update_params(net2, "conv2_2", "conv2_2_cus", approx_params)

approx_params=kernel_apprx_ML(net2,"conv3_1","conv3_1_cus",8)
update_params(net2, "conv3_1", "conv3_1_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv3_2","conv3_2_cus",8)
update_params(net2, "conv3_2", "conv3_2_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv3_3","conv3_3_cus",8)
update_params(net2, "conv3_3", "conv3_3_cus", approx_params)

approx_params=kernel_apprx_ML(net2,"conv4_1","conv4_1_cus",8)
update_params(net2, "conv4_1", "conv4_1_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv4_2","conv4_2_cus",8)
update_params(net2, "conv4_2", "conv4_2_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv4_3","conv4_3_cus",8)
update_params(net2, "conv4_3", "conv4_3_cus", approx_params)

approx_params=kernel_apprx_ML(net2,"conv5_1","conv5_1_cus",8)
update_params(net2, "conv5_1", "conv5_1_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv5_2","conv5_2_cus",8)
update_params(net2, "conv5_2", "conv5_2_cus", approx_params)
approx_params=kernel_apprx_ML(net2,"conv5_3","conv5_3_cus",8)
update_params(net2, "conv5_3", "conv5_3_cus", approx_params)

############################################
# run the baseline model
############################################
print("############################################")
print("run the baseline model...")
print("############################################")

acc=[]
acc5=[]
#conv1_2 = []
#conv1_2_cus = []

batch_size = 5
num_images = 200
iterations = num_images/batch_size

tic=time.time()
for i in range(iterations):
	#net.forward()
	acc.append(net.blobs["accuracy"].data*100)
	acc5.append(net.blobs["accuracy_5"].data*100)
	#conv1_2.append(net.blobs["conv1_2"].data*100)
    	#conv1_2_cus.append(net.blobs["conv1_2_cus"].data*100)
	if(i%200 == 0) :
	    print "--------------------------------------------Processed %s images-----------------------------------------"%((i+1)*batch_size)
baseline_accr=np.mean(acc)
baseline_accr5=np.mean(acc5)
toc=time.time()
baseline_t=toc-tic
print "\nAccuracy (Top 1): %.17f%%"%(baseline_accr)
print "Accuracy (Top 5): %.17f%%"%(baseline_accr5)

############################################
# run the modified model
############################################

#caffe.set_mode_cpu()
print("############################################")
print("run the modified model...")
print("############################################")

acc=[]
acc5=[]
#conv1_2 = []
#conv1_2_cus = []

batch_size = 5
num_images = 10000
iterations = num_images/batch_size

tic=time.time()
for i in range(iterations):
        net2.forward()
        acc.append(net2.blobs["accuracy"].data*100)
        acc5.append(net2.blobs["accuracy_5"].data*100)
        #conv1_2.append(net.blobs["conv1_2"].data*100)
        #conv1_2_cus.append(net.blobs["conv1_2_cus"].data*100)  
	if(i%200 == 0) :
            print "--------------------------------------------Processed %s images-----------------------------------------"%((i+1)*batch_size)
accr=np.mean(acc)
accr5=np.mean(acc5)
toc=time.time()
t=toc-tic
#print "\nTime elapsed: %s Minutes"%(baseline_t/60.0)
#print "\nTime elapsed: %s Minutes"%(t/60.0)
#print "\nBaseline Accuracy (Top 1): %.17f%%"%(baseline_accr)
#print "Baseline Accuracy (Top 5): %.17f%%"%(baseline_accr5)

print "\nAccuracy (Top 1): %.17f%%"%(accr)
print "Accuracy (Top 5): %.17f%%"%(accr5)

