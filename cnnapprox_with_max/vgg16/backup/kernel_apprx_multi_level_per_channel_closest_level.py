import caffe
import numpy as np


"""
def ret(distro,percentage):
	if len(distro)==0:
		return 1
	n=len(distro)
	indx=int(n*percentage/100)
	return distro[indx]

def mn(distro,nth):
	distro=np.array(distro)
	current_mean=1
	for i in range(nth):
		if len(distro)==0:
			return 1
		current_mean=np.mean(distro)
		distro=distro[distro<current_mean]
	return current_mean	
"""

def kernel_apprx_multi_level_per_channel_closest_level(net,kernel_name_original,kernel_name_cus,num_levels=5):
	k=net.params[kernel_name_original][0].data
	shp=k.shape
	siz=k.size
	unrolled=k.reshape((1,siz))
	bins=[0]+[1/2.0**i for i in range(num_levels)][::-1]	#determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]
	
	levels=np.array(bins).reshape((len(bins),1))
	abs_distance=np.abs(np.abs(unrolled)-levels)
	locations=np.argmin(abs_distance,axis=0).reshape((1,siz))
	neg_pos=np.zeros((1,siz))
	neg_pos[unrolled>0]=1.0
	neg_pos[unrolled<0]=-1.0
	temp=np.zeros((1,siz))
	for i in range(levels.size):
		temp[locations==i]=levels[i]
	temp_final=temp*neg_pos
	temp=temp_final.reshape(shp)
	net.params[kernel_name_cus][0].data[...]=temp
	#Replace original weights too
	net.params[kernel_name_original][0].data[...]=temp
	net.params[kernel_name_cus][1].data[...]=0
	print "\n\nApproximated kernels in layer "+kernel_name_original +" with "+str(num_levels)+" Levels\n"+str(bins)+"\n\n"
