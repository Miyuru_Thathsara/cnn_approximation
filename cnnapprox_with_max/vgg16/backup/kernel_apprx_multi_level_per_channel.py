import caffe
import numpy as np

def ret(distro,percentage):
	if len(distro)==0:
		return 1
	n=len(distro)
	indx=int(n*percentage/100)
	return distro[indx]

def mn(distro,nth):
	distro=np.array(distro)
	current_mean=1
	for i in range(nth):
		if len(distro)==0:
			return 1
		current_mean=np.mean(distro)
		distro=distro[distro<current_mean]
	return current_mean	

def kernel_apprx_multi_level_per_channel(net,kernel_name_original,kernel_name_cus):
	k=net.params[kernel_name_original][0].data
	siz=k.shape
	kk=k.reshape((siz[0]*siz[1],siz[2],siz[3]))
	temp_reshaped=np.zeros(kk.shape)
	for i in range(kk.shape[0]):
		pos=sorted(kk[i][kk[i]>0])
		neg=sorted(-1*kk[i][kk[i]<0])#to make the vaules +ve
		"""
		temp_reshaped[i][kk[i]>ret(pos,100*1/8)]=0.25
		temp_reshaped[i][kk[i]>ret(pos,100*3/8)]=0.5
		temp_reshaped[i][kk[i]>ret(pos,100*4/8)]=1
		temp_reshaped[i][kk[i]<-1*ret(neg,100*1/8)]=-0.25
		temp_reshaped[i][kk[i]<-1*ret(neg,100*3/8)]=-0.5
		temp_reshaped[i][kk[i]<-1*ret(neg,100*4/8)]=-1
		"""
		
		temp_reshaped[i][kk[i]>mn(pos,3)]=0.25
		temp_reshaped[i][kk[i]>mn(pos,2)]=0.5
		temp_reshaped[i][kk[i]>mn(pos,1)]=1
		temp_reshaped[i][kk[i]<-1*mn(neg,3)]=-0.25
		temp_reshaped[i][kk[i]<-1*mn(neg,2)]=-0.5
		temp_reshaped[i][kk[i]<-1*mn(neg,1)]=-1
		
	temp=temp_reshaped.reshape(siz)
	net.params[kernel_name_cus][0].data[...]=temp
	net.params[kernel_name_cus][1].data[...]=0
	print "Approximated kernels in layer "+kernel_name_original
