import caffe
import numpy as np
import time
from kernel_apprx_multi_level_per_channel import *
GPU_ID = 0 # Switch between 0 and 1 depending on the GPU you want to use.
caffe.set_mode_gpu()
caffe.set_device(GPU_ID)

net3=caffe.Net('vgg_customised.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
kernel_apprx_multi_level_per_channel(net3,"conv1_2","conv1_2_cus")
kernel_apprx_multi_level_per_channel(net3,"conv2_2","conv2_2_cus")
kernel_apprx_multi_level_per_channel(net3,"conv3_3","conv3_3_cus")
kernel_apprx_multi_level_per_channel(net3,"conv4_3","conv4_3_cus")
kernel_apprx_multi_level_per_channel(net3,"conv5_3","conv5_3_cus")

acc=[]
acc5=[]
tic=time.time()

for i in range(2500):
	net3.forward()
	acc.append(net3.blobs["accuracy"].data*100)
	acc5.append(net3.blobs["accuracy_5"].data*100)
	print "--------------------------------------------Processed %s images-----------------------------------------"%((i+1)*4)
accr=np.mean(acc)
accr5=np.mean(acc5)
toc=time.time()
t=toc-tic
print "\nTime elapsed: %s Minutes"%(t/60.0)
print "\nAccuracy (Top 1): %.17f%%"%(accr)
print "Accuracy (Top 5): %.17f%%"%(accr5)
