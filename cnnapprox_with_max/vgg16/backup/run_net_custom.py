import caffe
import numpy as np
import time
from kernel_apprx import *

#net2=caffe.Net('cifar10_conventional.prototxt','cifar10_quick_1_iter_4000.caffemodel',caffe.TEST)
#net1=caffe.Net('cifar10_conventional_original.prototxt','cifar10_quick_1_iter_4000.caffemodel',caffe.TEST)
net3=caffe.Net('vgg_customised.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
k_aprox(net3,"conv1_2","conv1_2_cus",0.02)
k_aprox(net3,"conv2_2","conv2_2_cus",0.02)
k_aprox(net3,"conv3_3","conv3_3_cus",0.02)
k_aprox(net3,"conv4_3","conv4_3_cus",0.02)
k_aprox(net3,"conv5_3","conv5_3_cus",0.02)

acc=[]
acc5=[]
tic=time.time()
#net1.forward()
#net2.forward()
for i in range(100):
	net3.forward()
	acc.append(net3.blobs["accuracy"].data*100)
	acc5.append(net3.blobs["accuracy_5"].data*100)
	print "--------------------------------------------Processed %s images-----------------------------------------"%((i+1)*100)
accr=np.mean(acc)
accr5=np.mean(acc5)
toc=time.time()
t=toc-tic
print "\nTime elapsed: %s Minutes"%(t/60.0)
print "\nAccuracy (Top 1): "+str(accr) +"%"
print "Accuracy (Top 5): "+str(accr5) +"%"
