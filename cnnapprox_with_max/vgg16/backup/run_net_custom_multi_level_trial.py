import caffe
import numpy as np
import time
from kernel_apprx_multi_level_per_channel_closest_level import *
GPU_ID = 0 # Switch between 0 and 1 depending on the GPU you want to use.
caffe.set_mode_gpu()
caffe.set_device(GPU_ID)
tic=time.time()
current=0
logg=np.zeros((16,2+5))
for p in range(6,8):
	for q in range(6,8):
		for r in range(6,8):
			for s in range(7,9,):
				for t in range(8,9):
					net3=caffe.Net('vgg_customised.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
					kernel_apprx_multi_level_per_channel_closest_level(net3,"conv1_2","conv1_2_cus",p)
					kernel_apprx_multi_level_per_channel_closest_level(net3,"conv2_2","conv2_2_cus",q)
					kernel_apprx_multi_level_per_channel_closest_level(net3,"conv3_3","conv3_3_cus",r)
					kernel_apprx_multi_level_per_channel_closest_level(net3,"conv4_3","conv4_3_cus",s)
					kernel_apprx_multi_level_per_channel_closest_level(net3,"conv5_3","conv5_3_cus",t)

					acc=[]
					acc5=[]


					for i in range(2500):
						net3.forward()
						acc.append(net3.blobs["accuracy"].data*100)
						acc5.append(net3.blobs["accuracy_5"].data*100)
						print "--------------------------------------------Processed %s images-----------------------------------------"%((i+1)*4)
					accr=np.mean(acc)
					accr5=np.mean(acc5)
					logg[current,0]=p
					logg[current,1]=q
					logg[current,2]=r
					logg[current,3]=s
					logg[current,4]=t
					logg[current,5]=accr
					logg[current,6]=accr5
					current+=1
					del net3
					print "\n\nTested for %s th combination\n\n"%(current)
					np.savetxt('logg_quantized_kernel_convolution.csv',logg,delimiter=",")

toc=time.time()	
max_acc=np.argmax(logg[:,5])
print "best combination : %s"%(logg[max_acc,:])

t=toc-tic
print "\nTime elapsed: %s Minutes"%(t/60.0)
print "\nAccuracy (Top 1): %.17f%%"%(accr)
print "Accuracy (Top 5): %.17f%%"%(accr5)
