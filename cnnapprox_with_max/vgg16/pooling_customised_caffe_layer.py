import caffe
import numpy as np
import math
from im2col import *




class pooling_customised_caffe_layer(caffe.Layer):
    def setup(self, bottom, top):
        if len(top) != 1:
            raise Exception("1 TOP BLOB IS REQUIRED FOR CUSTOMISED CONVOLUTION LAYER")
        if len(bottom) != 2:
            raise Exception("2 BOTTOM BLOBS ARE REQUIRED FOR CUSTOMISED CONVOLUTION LAYER")
        #params = eval(self.param_str)
	params={"pool_kernel_size":2,
		"pool_stride":2,
		}
        self.pool_kernel_size = params["pool_kernel_size"]
        self.pool_stride = params["pool_stride"]



    def forward(self, bottom, top):
        image_ori=bottom[0].data
	image_mod=bottom[1].data
        in_batch, in_channel, in_row, in_col = image_ori.shape
	h_out = int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride)))
	w_out = int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))
        top[0].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
	top[0].data[...]=np.zeros((in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride)))))
	image_ori_reshaped= image_ori.reshape(in_batch * in_channel, 1, in_row, in_col)
	image_mod_reshaped= image_mod.reshape(in_batch * in_channel, 1, in_row, in_col)
	im_ori_col = im2col_indices(image_ori_reshaped, self.pool_kernel_size, self.pool_kernel_size, padding=0, stride=self.pool_stride)
	im_mod_col = im2col_indices(image_mod_reshaped, self.pool_kernel_size, self.pool_kernel_size, padding=0, stride=self.pool_stride)
	max_idx = np.argmax(im_mod_col, axis=0)
	out = im_ori_col[max_idx, range(max_idx.size)]
	out = out.reshape(h_out, w_out, in_batch, in_channel)
	out = out.transpose(2, 3, 0, 1)
	top[0].data[...] = out
        #print "***** POOL1 for BATCH " + str(in_batch) + " completed ***** "


    def __getitem__(self):
        pass

    def reshape(self, bottom, top):
	image_ori=bottom[0].data
	in_batch, in_channel, in_row, in_col = image_ori.shape
	top[0].reshape(in_batch,in_channel,int(math.ceil(1+(in_row-self.pool_kernel_size)/float(self.pool_stride))),int(math.ceil(1+(in_col-self.pool_kernel_size)/float(self.pool_stride))))
        pass

    def backward(self, bottom, top):
        pass


