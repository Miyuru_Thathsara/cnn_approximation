#####################################################################################
#																				    #
# Project Title : Lowering Dynamic Power of a Stream-based CNN Hardware Accelerator #
# Author        : Rukshan Wickramasinghe											#
# Supervisor    : Prof. Lam Siew Kei   											    #
# Lab           : Hardware and Embedded Systems Lab (HESL), NTU, Singapore		    #
# Script name   : ML_weight_distr_plot.py											#
# Description   : This will plot the histograms of the weight distributions of the 	#
#                 given layers and highlight the quantization level closest to the 	#
#                 99th percentile value of the absolute weights.                 	#
# Last Modified : 6th Dec 2018 													    #
#																				    #
#####################################################################################

import matplotlib
matplotlib.use('Agg')
import numpy as np
import caffe
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

#################### Parameters (Adjust if necessary) #################### 	
	
#net_name    = 'lenet_conventional.prototxt'  # name of the prototxt file
net_name    = 'vgg_conventional.prototxt'  # name of the prototxt file
#caffe_model = 'lenet_iter_10000.caffemodel'	 # name of the weight file 
caffe_model = 'VGG_ILSVRC_16_layers.caffemodel'	 # name of the weight file 
file_name   = 'variance_report.txt'			 # name of the file to write the variances	
para        = ['conv1_2']              # include the layers you need to extract data
num_levels  = 4								 # number of qunatization levels.		

##########################################################################

levels = [1/(2**i) for i in range(0,10)][::-1]
#levels = [-0.125, -0.0625, -0.03125, 0, 0.03125, 0.0625, 0.125]
levels = [0]+levels
levels = np.array(levels).reshape((1,len(levels)))

def closest(number):
	dist=np.abs(levels-number)
	indx=np.argmin(dist)
	#print "\n\nClosest level to %f is %f,level number %s"%(number,levels[0,indx],abs(indx-levels.size))
	#print "rest of the levels : "+str(levels[0,1:indx+1].T)+"\n"
	return levels[0,indx]

net         = caffe.Net(net_name,caffe_model,caffe.TEST)
parameters  = {}
variance    = {}
thresholds  = {}
percentages = {}

for i in para:
	kernels         = net.params[i][0].data
	parameters[i]   = kernels
	abs_mean_layer  = np.mean(np.abs(kernels),keepdims=1)
	thresholds[i]   = abs_mean_layer
	abs_mean_kernel = np.mean(np.abs(kernels),axis=(1,2,3),keepdims=1)
	zero_mean       = abs_mean_kernel-abs_mean_layer
	zero_mean_2     = zero_mean*zero_mean
	var             = np.sum(zero_mean_2)/float(zero_mean_2.size)
	variance[i]     = var
	tot_weights     = kernels.size
	plus            = np.sum(kernels>abs_mean_layer)
	minus           = np.sum(kernels<-abs_mean_layer)
	zro             = tot_weights-(plus+minus)
	percentages[i]  = [100.0*plus/tot_weights,100.0*zro/tot_weights,100.0*minus/tot_weights]

with open(file_name,'w') as f:
	f.write("######### Variance of "+net_name+" ############\n\n\n")
	for i in para:
		f.write(i+" : %.16f\n"%(variance[i]))

print "\n\nVariance.txt created!"


bins = [1/2.0**i for i in range(2,num_levels)]
clrs = ['grey','red','green','navy','darkgreen','m','firebrick']
clrs = ['black','red','green','m','grey','firebrick']

###########plotting the weights################

for i in para:
	wei     = parameters[i].reshape((parameters[i].size))
	arr     = sorted(list(np.abs(wei)))
	indx    = int(len(arr)*99/100.0)
	max_val_old = arr[indx]
        max_val = np.percentile(np.array(wei) ,99)
        print("max val old : ", max_val_old, " max val : ", max_val)
	plt.hist(wei,bins = 500)
	plt.title(i+' weight distribution')
	plt.xlabel('weight value')
	plt.ylabel('frequency')
	plt.axvline(x = 0,color = 'm',linestyle = 'dashed',linewidth = 1.5)
	for k in range(len(bins)):
		if(bins[k] == closest(max_val)):
			plt.axvline(x = bins[k],color = 'red',linestyle = '-.',linewidth = 1.5)
			plt.axvline(x = -bins[k],color = 'red',linestyle = '-.',linewidth = 1.5)
		else:
			plt.axvline(x = bins[k],color = 'black',linestyle = 'dashed',linewidth = 1)#clrs[k]
			plt.axvline(x = -bins[k],color = 'black',linestyle = 'dashed',linewidth = 1)
		
	#red_patch=mpatches.Patch(color='red',label='Thresholds: \n'+str(thresholds[i][0,0,0,0])+'\n'+str(-thresholds[i][0,0,0,0]))
	green_patch = mpatches.Patch(color = 'white',label = '\n99th Percentile value : \n%.5f'%(max_val))
	plt.legend(handles = [green_patch])
	#plt.show() #uncomment to show the plot at run time
	plt.savefig('Weight_distribution_'+i+'.png',dpi=800)
	plt.clf()
