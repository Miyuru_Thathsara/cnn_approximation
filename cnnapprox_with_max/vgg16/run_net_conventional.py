import caffe
import numpy as np
import time
#import matplotlib.pyplot as plt

caffe.set_mode_gpu()
#caffe.set_device(1)
net2=caffe.Net('vgg_conventional.prototxt','VGG_ILSVRC_16_layers.caffemodel',caffe.TEST)
#net1=caffe.Net('cifar10_conventional_original.prototxt','cifar10_quick_1_iter_4000.caffemodel',caffe.TEST)
#net3=caffe.Net('cifar10_customised.prototxt','cifar10_quick_1_iter_4000.caffemodel',caffe.TEST)

#l1_activations = []
acc=[]
acc5=[]
acc_sum = 0.0
acc5_sum = 0.0
tic=time.time()
batch_size = 25
num_images = 10000
iterations = num_images/batch_size

print "iterations %d" %(iterations)
#net1.forward()
for i in range(iterations):
	net2.forward()
	#l1_activations.append(net2.blobs["conv1_1"].data)
	acc.append(net2.blobs["accuracy"].data*100)
        acc5.append(net2.blobs["accuracy_5"].data*100)
	#print(net2.blobs["accuracy"].data*100)
	#acc_sum = net2.blobs["accuracy"].data*100
	#acc5_sum = net2.blobs["accuracy_5"].data*100
	#acc.append(net2.blobs["accuracy"].data*100)
	#acc5.append(net2.blobs["accuracy"].data*100)
	print "---------------------------------------------Processed %s images--------------------------------------------------------------------------------"%(batch_size*(i+1))
accr=np.mean(acc)
accr5=np.mean(acc5)
#net3.forward()
toc=time.time()
t=toc-tic

#l1_activations = np.array(l1_activations)
#l1_activations_flat = l1_activations.flatten()

#plt.hist(l1_activations_flat, bins=50)
#plt.show()
print "\nTime elapsed: %s Minutes"%(t/60.0)
print "\nAccuracy (Top 1): "+str(accr) +"%"
print "Accuracy (Top 5): "+str(accr5) +"%"
