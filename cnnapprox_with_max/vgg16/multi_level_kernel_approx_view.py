import caffe
import numpy as np
from numpy import genfromtxt
from termcolor import colored




def get_kernels(net,layer_name,feature_map):
	kernel=net.params[layer_name][0].data[feature_map]
	shpe=kernel.shape
	return kernel.reshape((shpe[0]*shpe[1],shpe[2]))

def modify_kernels(kernels,method,threshold_high,threshold_low):
	i,j=kernels.shape
	k=kernels.reshape((i/j,j,j))
	shpe=k.shape
	mod_k=np.zeros(shpe)
	if method=='per_kernel' or method=='per_kernel_2_thresholds' or method=='per_layer':
		mod_k[k>=threshold_high]=1
		mod_k[k<=threshold_low]=-1 #give the threshold_low param -ve
	elif method=='per_channel' or method=='per_channel_2_thresholds':
		for ch in range(shpe[0]):
			mod_k[ch][k[ch]>=threshold_high[ch]]=1
			mod_k[ch][k[ch]<=threshold_low[ch]]=-1
	return mod_k.reshape((i,j))
		
def get_threshold(kernel,mtd2):
	"""
	Methods:
		per_kernel
		per_channel
		per_kernel_2_thresholds
		per_channel_2_thresholds
	returns 2 thresholds :(threshold high,threshol low)
	"""
	i,j=kernel.shape
	k=kernel.reshape((i/j,j,j))
	if mtd2=='per_kernel':
		thh=np.mean(np.abs(k))
		thl=-np.mean(np.abs(k))
	elif mtd2=='per_channel':
		thh=list(np.mean(np.abs(k),axis=(1,2)))
		thl=list(-np.mean(np.abs(k),axis=(1,2)))
	elif mtd2=='per_kernel_2_thresholds':
		thh=1
		thl=-1
		high=k[k>0]
		low=k[k<0]
		if len(high)!=0:
			thh=np.mean(high)
		if len(low)!=0:
			thl=np.mean(low)
	elif mtd2=='per_channel_2_thresholds':
		thh=[1]*(i/j)
		thl=[-1]*(i/j)
		num=0
		for ch in k:
			high=ch[ch>0]
			low =ch[ch<0]
			if len(high)!=0:
				thh[num]=np.mean(high)
			if len(low)!=0:
				thl[num]=np.mean(low)
			num+=1
	return [thh,thl]
	


def print_kernels(ker,mod_ker,feature_map):
	clrs=['grey','red','green','yellow','blue','magenta','cyan','white']
	bins=[1.0,0.5,0.25,0.125,0.0625,0.03125,0.015625,0.0]
	clrmp1='[  '
	clrmp2='[  '
	for b in range(len(bins))[::-1]:
		clrmp1+=colored(bins[b],clrs[b],attrs=['bold'])+"  "
		clrmp2+=colored(-1*bins[b],clrs[b],attrs=['dark'])+"  "
	clrmp1+=']'
	clrmp2+=']'
	clrmp=clrmp1+"  "+clrmp2
	i,j=ker.shape
	for ii in range(i):
		if ii%j==0:
			print '\n\n\nchannel %s\t%s\n'%(ii/j,clrmp)
		"""
		strng=''
		strng+=' '.join([" %.8f"%(k) if k>=0 else "%.8f"%(k) for k in ker[ii]])
		strng+='\t'
		strng+='   '.join([" %s"%(int(k)) if k>=0 else "%s"%(int(k)) for k in mod_ker[ii]])
		"""	
		strng1=''
		strng2=''
		for jj in range(j):
			if mod_ker[ii,jj]>0:
				strng1+=colored(" %.7f "%ker[ii,jj],clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['bold'])
				strng2+=colored(" %.6f  "%mod_ker[ii,jj],clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['bold'])
			elif mod_ker[ii,jj]==0:
				if ker[ii,jj]>=0:
					strng1+=colored(" %.7f "%ker[ii,jj],clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['bold'])
					strng2+=colored(" %.6f  "%mod_ker[ii,jj],clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['bold'])
					#strng1+=colored(" %.8f "%ker[ii,jj],'white')
					#strng2+=colored(" %.6f  "%(int(mod_ker[ii,jj])),'white')
				else:
					strng1+=colored("%.7f "%ker[ii,jj],clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['dark'])
					strng2+=colored(" %.6f  "%np.abs(mod_ker[ii,jj]),clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['bold'])
					#strng1+=colored("%.8f "%ker[ii,jj],'white')
					#strng2+=colored(" %s  "%(int(mod_ker[ii,jj])),'white')
			else:
				strng1+=colored("%.7f "%ker[ii,jj],clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['dark'])
				strng2+=colored("%.6f  "%mod_ker[ii,jj],clrs[bins.index(np.abs(mod_ker[ii,jj]))],attrs=['dark'])
				#strng1+=colored("%.8f "%ker[ii,jj],'red')
				#strng2+=colored("%s  "%(int(mod_ker[ii,jj])),'red')
		print strng1+'\t\t'+strng2
		


def kernel_approx_multi_level(ori_weights,num_levels=5):
	k=ori_weights
	shp=k.shape
	siz=k.size
	unrolled=k.reshape((1,siz))
	bins=[0]+[1/2.0**i for i in range(num_levels)][::-1]	#determining the levels of approximation [0.0 ..... 0.03125,0.0625,0.125,0.25,0.5,1.0]
	
	levels=np.array(bins).reshape((len(bins),1))
	abs_distance=np.abs(np.abs(unrolled)-levels)
	locations=np.argmin(abs_distance,axis=0).reshape((1,siz))
	neg_pos=np.zeros((1,siz))
	neg_pos[unrolled>0]=1.0
	neg_pos[unrolled<0]=-1.0
	temp=np.zeros((1,siz))
	for i in range(levels.size):
		temp[locations==i]=levels[i]
	temp_final=temp*neg_pos
	temp=temp_final.reshape(shp)
	return temp

######################################################################################################
net_name='alexnet_bn_conventional.prototxt'
caffe_model_name='alexnet_cvgj_iter_320000.caffemodel'
layer_name='conv1'
number_of_levels=7

net=caffe.Net(net_name,caffe_model_name,caffe.TEST)
shpe=net.params[layer_name][0].data.shape
feature_maps=range(shpe[0])


weights=net.params[layer_name][0].data
print weights.shape
approximated=kernel_approx_multi_level(weights,number_of_levels)
weights_unrolled=weights.reshape((shpe[0],shpe[1]*shpe[2],shpe[3]))
approximated_unrolled=approximated.reshape((shpe[0],shpe[1]*shpe[2],shpe[3]))



for feature_map in feature_maps:
	print "\n\n################################################################################################\n\n"
	print colored("Filter %i"%(feature_map),'yellow',attrs=['underline','bold'])+"\n\n"
	
	#print "Kernel %s\n\nThreshold High: %s\nThreshold Low :%s\nMethod : %s"%(feature_map,threshold_high,threshold_low,method)	
	#modified_kernels=modify_kernels(kernels,method,threshold_high,threshold_low)
	print_kernels(weights_unrolled[feature_map],approximated_unrolled[feature_map],feature_map)

