import caffe
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


def extract_dat(prototxt,caffe_model,layer_name,kernel_number=-1):
	net=caffe.Net(prototxt+'.prototxt',caffe_model+'.caffemodel',caffe.TEST)
	if kernel_number==-1:
		return net.params[layer_name][0].data
	else: return net.params[layer_name][0].data[kernel_number]

def straighten(matrx):
	return matrx.reshape([matrx.size,1])

def variance(data):
	return np.var(data)

def plotter(data,titl,y_lab,x_lab,num_bins,lgnd_labs,lgnd_vals,save_name,save=False,den=False):
	plt.hist(data,bins=num_bins,density=den)
	plt.title(titl)
	plt.xlabel(x_lab)
	plt.ylabel(y_lab)
	#plt.axvline(x=thresholds[i],color='red',linestyle='dashed')
	#plt.axvline(x=-thresholds[i],color='red',linestyle='dashed')
	if len(lgnd_labs)!=0:	
		strng=''
		for i in range(len(lgnd_labs)):
			strng+=lgnd_labs[i]+' : '+str(lgnd_vals[i])+'\n'
		red_patch=mpatches.Patch(color='red',label=strng)
		plt.legend(handles=[red_patch])
	#green_patch=mpatches.Patch(color='green',label='\nVariance: \n%.10f'%(variance[i]))
	#yellow_patch=mpatches.Patch(color='yellow',label='\nApprx.: \n+1 : %.4f%%\n 0 : %.4f%%\n-1 : %.4f%%'%(tuple(percentages[i])))
	plt.grid(1)
	if save:
		plt.savefig(save_name+'.png')
	plt.show()
	plt.clf()
	


###############################################################################################################
prototxt_file_name = 'vgg_conventional'
caffe_model_name = 'VGG_ILSVRC_16_layers'
layer_name = ['conv1_2']
kernel_num=-1

title = 'Weight distribution\n'+prototxt_file_name
x_axis = 'weight value'
y_axis = 'Frequency %'
number_of_bins =1000
save=False
save_name='test'
density=True
###############################################################################################################





for lyr in layer_name:
	dat=extract_dat(prototxt_file_name,caffe_model_name,lyr,kernel_num)
	num_dat=dat.size

	legend_labels=['variance','mean','abs. mean']
	legend_values=[variance(dat),np.mean(dat),np.mean(np.abs(dat))]
	title_update=title+',\nLayer : '+lyr+'     '+('Kernel : '+str(kernel_num))*(-1<kernel_num)
	save_name_update=save_name+'_'+lyr
	plotter(straighten(dat),title_update,y_axis,x_axis,number_of_bins,legend_labels,legend_values,save_name_update,save,density)



